import 'package:flutter/material.dart';
import 'package:most/src/common/events.dart';
import 'package:most/src/models/post_type.dart';
import 'package:most/src/views/circles_page.dart';
import 'package:most/src/views/questions_and_answers_page.dart';
import 'package:most/src/views/shopping_page.dart';
import 'package:most/src/views/user_profile_editing_page.dart';

import 'src/common/repository.dart';

// Post FAB bottom segues to
import 'src/views/post.dart';
import 'src/views/user_profile.dart';
import 'src/views/login.dart';
import 'src/views/settings.dart';

import 'package:flutter_platform_widgets/flutter_platform_widgets.dart';

/* TODO: See https://github.com/GeekyAnts/flutter-login-home-animation */

void main() {
  // Enter home page if logged in. Otherwise show login page.
  Repository.get()
      .loginStatus
      .then((isLoggedIn) => runApp(new MostApp(isLoggedIn)));
  // runApp(new MostApp(true));
}

/// This widget is the root of your application.
class MostApp extends StatelessWidget {
  MostApp(this.isLoggedIn);
  final bool isLoggedIn;

  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      title: 'MOST',
      theme: new ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or press Run > Flutter Hot Reload in IntelliJ). Notice that the
        // counter didn't reset back to zero; the application is not restarted.
        primaryColor: Colors.red[300],
        primaryColorDark: Color.fromRGBO(196, 0, 29, 1.0),
        primaryColorLight: Color.fromRGBO(255, 97, 111, 1.0),
        accentColor: Colors.black87,
        buttonColor: Colors.black87,
        selectedRowColor: Colors.black,
        textTheme: TextTheme(
            button: TextStyle(fontWeight: FontWeight.bold),
            title: TextStyle(fontWeight: FontWeight.bold, fontSize: 21.0)),
        accentTextTheme:
            TextTheme(button: TextStyle(fontWeight: FontWeight.bold)),
        buttonTheme: ButtonThemeData(textTheme: ButtonTextTheme.accent),
      ),

      home: isLoggedIn
          ? new MyHomePage()
          : new Login(), // TODO: add login determination
      // home: MyHomePage(),
      routes: <String, WidgetBuilder>{
        '/home': (BuildContext context) => new MyHomePage(),
        '/login': (BuildContext context) => new Login(),
        '/profile/edit': (BuildContext context) => new UserProfileEditing(),
        // '/post': (BuildContext context) => new Post(title: '发布'),
        '/settings': (BuildContext context) => new Settings(),
        '/post/ask': (BuildContext context) => new Post(
              type: PostType.ask,
            ),
        '/post/used_book': (BuildContext context) => new Post(
              type: PostType.used_book,
            ),
        '/post/second_hand': (BuildContext context) => new Post(
              type: PostType.second_hand,
            ),
        '/post/daigou': (BuildContext context) => new Post(
              type: PostType.daigou,
            ),
        '/post/choose_tag': (BuildContext context) => new ChooseTag(title: ''),
      },
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title, this.icon}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;
  final Widget icon;

  @override
  _MyHomePageState createState() => new _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  // void onSubmitted(String value) {
  //   setState(() => _scaffoldKey.currentState
  //       .showSnackBar(new SnackBar(content: new Text('You wrote $value!'))));
  // }

  // Home Pages
  /* Implementation notes
  -----------------------
  Use a list and index to implement page transition.
  Note to call setState() when changing the page index.Axis
  */
  final Key keyOne = PageStorageKey('pageOne');
  final Key keyTwo = PageStorageKey('pageTwo');
  final Key keyThree = PageStorageKey('pageThree');
  final Key keyMyProfile = PageStorageKey('myProfile');

  int _currentIndex = 0;

  CirclesPage page1;
  QuestionsAndAnswersPage page2;
  ShoppingPage page3;
  List<Widget> homePages;
  UserProfile userProfilePage;

  Widget _currentPage;

  final PageStorageBucket bucket = PageStorageBucket();

  // App bar theme is the inverse of the primary theme.
  ThemeData get appbarTheme {
    final primaryColor = Theme.of(context).primaryTextTheme.title.color;
    final titleTheme = Theme.of(context)
        .textTheme
        .title
        .copyWith(color: Theme.of(context).primaryColor);
    final textTheme = Theme.of(context).textTheme.copyWith(title: titleTheme);
    return Theme.of(context)
        .copyWith(primaryColor: primaryColor, primaryTextTheme: textTheme);
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    _initUserObjectId();

    page1 = CirclesPage(
      key: keyOne,
    );
    page2 = QuestionsAndAnswersPage(
      key: keyTwo,
    );
    page3 = ShoppingPage(
      key: keyThree,
    );
    userProfilePage = UserProfile(
      key: keyMyProfile,
      id: thisUserObjectId,
    );
    homePages = [
      // page1,
      page2,
      page3,
      userProfilePage,
    ];

    _currentPage = homePages[0];

    Repository.get().eventBus.on<InActionEvent>().listen((event) {
      // final snackBar = SnackBar(
      //   content: Text(event.message),
      // );
      // Scaffold.of(context).showSnackBar(snackBar); // TODO: remove one
      Navigator.of(context)
          .pushNamedAndRemoveUntil('/', (Route<dynamic> r) => false);
      // .then((_) => Scaffold.of(context).showSnackBar(snackBar));
    });
  }

  String thisUserObjectId;
  _initUserObjectId() {
    Repository.get().thisUserObjectId.then((objectId) {
      print(objectId);
      if (this.mounted) {
        setState(() {
          userProfilePage = UserProfile(
            key: keyMyProfile,
            id: objectId,
            isThisUser: true,
          );
          homePages = [
            // page1,
            page2,
            page3,
            userProfilePage,
          ];
        });
      }
    });
  }

  @override
  void dispose() {
    // TODO: implement dispose

    homePages.clear();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return new PlatformScaffold(
      // appBar: buildAppBar(context),
      // key: _scaffoldKey,
      body: PageStorage(
        bucket: bucket,
        child: _currentPage,
      ),

      // floatingActionButton: new FloatingActionButton(
      //   onPressed: _openPostDialog,
      //   tooltip: 'Increment',
      //   child: new Icon(Icons.add),
      // ),
      // floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,

      bottomNavBar: new PlatformNavBar(
        android: (_) => MaterialNavBarData(
              type: BottomNavigationBarType.fixed,
            ),
        ios: (_) => CupertinoTabBarData(),
        currentIndex: _currentIndex,
        itemChanged: (int index) {
          if (this.mounted) {
            // ensure the object is still in the tree.
            setState(() {
              this._currentIndex = index;
              this._currentPage = homePages[this._currentIndex];
            });
          }
        },
        items: [
          // new BottomNavigationBarItem(
          //   icon: new Icon(Icons.donut_large),
          //   title: new Text(
          //     '圈子',
          //   ),
          // ),
          new BottomNavigationBarItem(
            icon: new Icon(Icons.library_books),
            title: new Text('知识'),
          ),
          new BottomNavigationBarItem(
            icon: new Icon(Icons.shopping_cart),
            title: new Text('购物'),
          ),
          new BottomNavigationBarItem(
            icon: new Icon(Icons.account_circle),
            title: new Text('我'),
          ),
        ],
      ),
    );
  }
}
