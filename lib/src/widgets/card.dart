import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:date_format/date_format.dart';
import 'package:most/src/views/goods_details.dart';
import 'package:most/src/views/question_details.dart';
import 'package:notus/notus.dart';
import '../views/user_page/other_user_page.dart';

class MyCard extends StatefulWidget {
  final List cardData;
  final bool isFirstCard;
  final int numberOfLines;
  final String cardTitle;
  MyCard(
      {Key key,
      this.cardData,
      this.isFirstCard,
      this.numberOfLines,
      this.cardTitle})
      : super(key: key);
  @override
  State<StatefulWidget> createState() {
    return new MyCardState(
        cardData: cardData,
        isFirstCard: isFirstCard,
        numberOfLines: numberOfLines,
        cardTitle: cardTitle);
  }
}

class MyCardState extends State<MyCard> {
  List cardData;
  bool isFirstCard;
  int numberOfLines;
  String cardTitle;
  List<dynamic> listData;
  //RegExp regExp1 = new RegExp("</.*>");
  //RegExp regExp2 = new RegExp("<.*>");
  TextStyle subtitleStyle =
      new TextStyle(fontSize: 12.0, color: const Color(0xFFB5BDC0));
  TextStyle contentStyle = new TextStyle(fontSize: 15.0, color: Colors.black);

  MyCardState(
      {Key key,
      this.cardData,
      this.isFirstCard,
      this.numberOfLines,
      this.cardTitle});

  //设置字体
  textStyle({int size, Color color, FontWeight fontWeight}) {
    return new TextStyle(
      fontSize: size.toDouble(),
      decoration: TextDecoration.none,
      color: color,
      fontWeight: fontWeight,
    );
  }

  setListData() {
    listData = [];
    if (cardData.length == numberOfLines) {
      for (var j = 0; j < cardData.length; j++) {
        listData.add(cardData[j]);
        if (j != numberOfLines - 1) {
          listData.add('LINE');
        }
      }
    } else {
      if (cardData.isEmpty) {
        listData = [numberOfLines];
      } else {
        for (var j = 0; j < cardData.length; j++) {
          listData.add(cardData[j]);
          if (j != numberOfLines - 1) {
            listData.add('LINE');
          }
        }
        listData.add(numberOfLines - cardData.length);
      }
    }
  }

  @override
  void initState() {
    super.initState();
    setListData();
    //getReply();
  }

  @override
  Widget build(BuildContext context) {
    Widget _singleItemDisplay(dynamic item) {
      if (item is String) {
        return new Divider(
          height: 0.66,
        );
      } else if (item is num) {
        return new Container(
          height: 0.66 * (item - 1) + 66.0 * item,
        );
      } else {
        return new Container(
          height: 66.0,
          padding: EdgeInsets.fromLTRB(0.0, 10.0, 0.0, 10.0),
          child: new GestureDetector(
            onTap: () {
              if (item['tag'] == 1) {
                Navigator.of(context)
                    .push(new MaterialPageRoute(builder: (ctx) {
                  return new QuestionDetailPage(
                      userId: item['user_id'],
                      questionId: item['objectId'],
                      postTime: item['createdAt']['iso'],
                      questionBody: item['question_detail'],
                      questionTitle: item['question_title'],
                      userAvatarUrl: (item['user_image'] != null)
                          ? item['user_image']['url']
                          : null,
                      userName: item['username']);
                }));
              } else if (item['tag'] == 2 || item['tag'] == 3) {
                Navigator.of(context)
                    .push(new MaterialPageRoute(builder: (ctx) {
                  return new QuestionDetailPage(
                      userId: item['user_id'],
                      questionId: item['question_id'],
                      postTime: item['question_createdAt']['iso'],
                      questionBody: item['question_detail'],
                      questionTitle: item['question_title'],
                      userAvatarUrl: item['question_user_image']['url'],
                      userName: item['question_username']);
                }));
              } else if (item['tag'] == 4 || item['tag'] == 5) {
                Navigator.of(context)
                    .push(new MaterialPageRoute(builder: (ctx) {
                  return new GoodsDetailPage(
                      tag: item['tag'], tradeId: item['objectId']);
                }));
              }
            },
            child: new Column(
              children: <Widget>[
                new Row(
                  children: <Widget>[
                    new GestureDetector(
                      onTap: () {
                        Navigator.of(context)
                            .push(new MaterialPageRoute(builder: (ctx) {
                          return new UserDetailsPage(userId: item['user_id']);
                        }));
                      },
                      child: new Container(
                        width: 20.0,
                        height: 20.0,
                        decoration: new BoxDecoration(
                          image: new DecorationImage(
                              image: new NetworkImage(
                                  item['user_image']['url'].toString())),
                          borderRadius: new BorderRadius.all(
                            const Radius.circular(10.0),
                          ),
                        ),
                      ),
                    ),
                    new Container(
                      height: 20.0,
                      margin: const EdgeInsets.fromLTRB(6.0, 0.0, 0.0, 0.0),
                      child: new Text(item['username'].toString(),
                          style: textStyle(
                              size: 12,
                              color: Color.fromRGBO(74, 74, 74, 1.0),
                              fontWeight: FontWeight.w500)),
                    ),
                    new Container(
                      height: 20.0,
                      margin: const EdgeInsets.fromLTRB(6.0, 0.0, 0.0, 0.0),
                      child: new Text(getStates(item),
                          style: textStyle(
                              size: 12,
                              color: Color.fromRGBO(74, 74, 74, 0.8),
                              fontWeight: FontWeight.w400)),
                    ),
                    new Material( 
                      color: Colors.transparent,
                      child: Container(
                        height: 20.0,
                        margin: const EdgeInsets.fromLTRB(6.0, 0.0, 0.0, 0.0),
                        child: new Text(
                          getDate(DateTime.parse(
                              item['createdAt']['iso'].toString())),
                          style: subtitleStyle,
                      ))),
                  ],
                ),
                new Container(
                    margin: const EdgeInsets.fromLTRB(0.0, 5.0, 0.0, 0.0),
                    padding: EdgeInsets.all(0.0),
                    child: new Row(
                      children: <Widget>[
                        new Expanded(
                          child: new Text(
                            getDetailString(item),
                            maxLines: 1,
                            overflow: TextOverflow.ellipsis,
                            style: textStyle(
                              size: 14,
                              color: Color.fromRGBO(0, 0, 0, 0.9),
                              fontWeight: FontWeight.w500),
                        ))
                      ],
                    ))
              ],
            ),
          ),
        );
      }
    }

    Widget itemsWidget = new Column(
        children: listData.map((dynamic item) {
      return _singleItemDisplay(item);
    }).toList());

    num topDistance() {
      if (isFirstCard == true) {
        return 20.0;
      } else {
        return 10.0;
      }
    }

    return new Container(
      padding: EdgeInsets.fromLTRB(10.0, topDistance(), 10.0, 10.0),
      child: new Container(
        decoration: new BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.all(Radius.circular(14.0)),
            boxShadow: [
              new BoxShadow(
                color: Color.fromRGBO(0, 0, 0, 0.1),
                blurRadius: 14.0,
              ),
            ]),
        child: new Column(
          children: <Widget>[
            new Container(
              margin: EdgeInsets.fromLTRB(20.0, 18.0, 20.0, 5.0),
              child: new Row(
                children: <Widget>[
                  //红点
                  new Container(
                    width: 8.0,
                    height: 20.0,
                    margin: new EdgeInsets.fromLTRB(0.0, 0.0, 12.0, 0.0),
                    decoration: new BoxDecoration(
                        color: Color(0XFFCD2E2D),
                        borderRadius:
                            new BorderRadius.all(const Radius.circular(3.5))),
                  ),
                  //问题标题
                  new Text(
                    cardTitle,
                    style: this.textStyle(
                        size: 18,
                        color: Colors.black,
                        fontWeight: FontWeight.w600),
                  ),
                ],
              ),
            ),
            new Container(
              margin: EdgeInsets.fromLTRB(20.0, 0.0, 0.0, 10.0),
              child: itemsWidget,
            )
          ],
        ),
      ),
    );
  }

  String getStates(Map<String, dynamic> item) {
    if (item['tag'] == 1) {
      return '发布了问题';
    } else if (item['tag'] == 2) {
      if (item['question_title'].toString().length > 6) {
        return '回答了"' +
            item['question_title'].toString().substring(0, 6) +
            '...' +
            '"';
      } else {
        return '回答了"' + item['question_title'].toString() + '"';
      }
    } else if (item['tag'] == 3) {
      if (item['answer_detail'].toString().length > 6) {
        return '评论了"' +
            item['answer_detail'].toString().substring(0, 6) +
            '...' +
            '"';
      } else {
        return '评论了"' + item['answer_detail'].toString() + '"';
      }
    } else if (item['tag'] == 4) {
      return '发布了二手书';
    } else if (item['tag'] == 5) {
      return '发布了闲置品';
    }
    return '';
  }

  String getDetailString(Map<String, dynamic> item) {
    if (item['tag'] == 1) {
      return item['question_title'];
    } else if (item['tag'] == 2) {
      return NotusDocument.fromJson(json.decode(item['answer_detail']))
          .toPlainText();
    } else if (item['tag'] == 3) {
      return item['comment_detail'];
    } else if (item['tag'] == 4 || item['tag'] == 5) {
      return item['trade_title'];
    }
    return '';
  }

  String getDate(DateTime date) {
    return formatDate(date, [mm, '月', dd, '日', hh, ':', nn]).toString();
  }

  String listToJsonString(List list) {
    String result = '[';
    for (var i = 0; i < list.length; i++) {
      result += '"' + list[i] + '"';
      if (i != list.length - 1) {
        result += ',';
      }
    }
    result += ']';
    return result;
  }
}
