import 'package:flutter/material.dart';
import 'package:most/src/api/leancloud.dart';

import 'dart:async';
import 'package:quill_delta/quill_delta.dart';
import 'package:date_format/date_format.dart';
import 'package:flutter_platform_widgets/flutter_platform_widgets.dart';
import 'package:groovin_material_icons/groovin_material_icons.dart';
import 'package:transparent_image/transparent_image.dart';
import '../utils/utils.dart';

class CouponsTab extends StatefulWidget {
  final String userId;

  CouponsTab({
    Key key,
    this.userId,
    // @required this.controller
  }) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return new CouponsTabState(
      userId: userId,
    );
  }
}

class CouponsTabState extends State<CouponsTab> {
  String userId;

  List takenTickets;
  List untakenTickets;
  List finalTicketList;

  CouponsTabState({Key key, this.userId});

  //设置字体
  textStyle({int size, Color color, FontWeight fontWeight}) {
    return new TextStyle(
      fontSize: size.toDouble(),
      decoration: TextDecoration.none,
      color: color,
      fontWeight: fontWeight,
    );
  }

  // Future initUserInfo() async {
  //   final info = LeanCloudUser.getUserInfo(userObjectId);
  // }

  //获取票子
  getTicket() async {
    //获取已经有的票子
    var newTicketData = await LeanCloudFunction.callFunction(
        myData: {'user_id': userId}, functionName: 'check_user_ticket');
    //获取暂时没有的票子
    var newUntakeTicketData = await LeanCloudFunction.callFunction(
        myData: {'user_id': userId}, functionName: 'check_new_ticket');
    if (this.mounted) {
      setState(() {
        takenTickets = newTicketData;
        untakenTickets = newUntakeTicketData;
        finalTicketList = takenTickets + untakenTickets;
      });
    }
  }

  @override
  void initState() {
    super.initState();
    getTicket();
  }

  @override
  Widget build(BuildContext context) {
    if (takenTickets == null || untakenTickets == null) {
      return new Center(
        child: new PlatformCircularProgressIndicator(
          android: (_) => MaterialProgressIndicatorData(
                backgroundColor: Color(0XFFFF2D55),
              ),
          ios: (_) => CupertinoProgressIndicatorData(),
        ),
      );
    } else if (takenTickets.isEmpty && untakenTickets.isEmpty) {
      return Center(
        child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text('😎',
                  style: TextStyle(
                    fontSize: 56.0,
                  )),
              Container(height: 16.0),
              Text('暂时还没发布卡券噢，敬请关注')
            ]),
      );
    } else {
      return Container(
        margin: new EdgeInsets.fromLTRB(8.0, 8.0, 8.0, 0.0),
        child: new ListView.builder(
          itemCount: finalTicketList.length,
          itemBuilder: _renderMyCardRow,
          // controller: _controller,
        ),
      );
    }

    /*return new PlatformScaffold(
        appBar: new PlatformAppBar(
          backgroundColor: Color(0xFFC62828),
          title: new Text("我的卡券", style: new TextStyle(color: Colors.white)),
          android: (BuildContext context) => MaterialAppBarData(
                iconTheme: new IconThemeData(color: Colors.white),
              ),
          ios: (_) => CupertinoNavigationBarData(),
        ),
        body: _body
    );*/
  }

  Widget _buildGetButton(var item) {
    if (takenTickets.contains(item)) {
      return Container();
    } else {
      return ButtonTheme.bar(
        padding: EdgeInsets.only(bottom: 8.0),
        child: new ButtonBar(
          alignment: MainAxisAlignment.start,
          children: <Widget>[
            new Container(
              margin: EdgeInsets.only(left: 16.0, bottom: 16.0)
            ),
            new FlatButton(
              color: Colors.pink,
              padding: EdgeInsets.all(0.0),
              child: new Text(
                "领取",
                style: Theme.of(context).textTheme.button,
              ),
              onPressed: () async {
                var result = await LeanCloudFunction.callFunction(myData: {
                  'user_id': userId,
                  'ticket_id': item['objectId'].toString(),
                }, functionName: 'add_ticket');
                if (result.toString() == 'OK') getTicket();
              },
              // shape: new RoundedRectangleBorder(
              //     borderRadius: new BorderRadius.circular(9.0))
            ),
          ],
        ),
      );
    }
  }

  //渲染我的卡片列表
  Widget _renderMyCardRow(BuildContext context, int i) {
    var listItem = finalTicketList[i];
    String ticketId = listItem['objectId'].toString();
    String ticketTitle = listItem['ticket_title'].toString();
    String shopLogoUrl = listItem['shop_logo']['url'].toString();
    String shopName = listItem['shop_name'].toString();
    int remainTimes = listItem['times'];
    DateTime dateTime =
        DateTime.parse(listItem['expires_on']['iso'].toString());

    var row = new Card(
      // elevation: 0.0,
      // shape: RoundedRectangleBorder(),
      //商铺图片
      child: Column(
        children: <Widget>[
          Padding(
            padding: EdgeInsets.fromLTRB(16.0, 16.0, 16.0, 16.0),
            child: new Row(
              children: <Widget>[
                Flexible(
                  flex: 1,
                  child: new Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        shopName,
                        style: Theme.of(context).textTheme.caption.copyWith(
                              color: Theme.of(context).primaryColorLight,
                            ),
                        maxLines: 1,
                      ),
                      Container(
                        height: 8.0,
                      ),
                      Text(
                        ticketTitle.toUpperCase(),
                        style: Theme.of(context).textTheme.title.copyWith(
                              fontWeight: FontWeight.bold,
                              fontSize: 21.0,
                            ),
                        softWrap: true,
                        overflow: TextOverflow.ellipsis,
                        maxLines: 2,
                      ),
                      Container(
                        height: 14.0,
                      ),
                      new Text(
                        '截止日期：${getDateWithYear(dateTime)}',
                        overflow: TextOverflow.ellipsis,
                        // style: textStyle(
                        //   size: 17,
                        //   color: Colors.black45,
                        // ),
                      ),
                      new Text(
                        (remainTimes == -1) ? '无次数限制' : ('限使用 1 次'),
                        overflow: TextOverflow.ellipsis,
                        // style: textStyle(
                        //   size: 17,
                        //   color: Colors.black45,
                        // ),
                      ),
                    ],
                  ),
                ),

                //放1. 优惠标题 2.商铺名称 3.优惠细节 4，使用次数与使用截止日期
                Padding(
                  child: FadeInImage.memoryNetwork(
                    fit: BoxFit.contain,
                    width: 80.0,
                    image: shopLogoUrl,
                    placeholder: kTransparentImage,
                  ),
                  padding: EdgeInsets.fromLTRB(16.0, 0.0, 0.0, 0.0),
                ),
              ],
            ),
          ),
          _buildGetButton(listItem),
        ],
      ),
    );
    return new Builder(
      builder: (ctx) {
        return new InkWell(
          // padding: const EdgeInsets.all(0.0),
          onTap: () {
            if (takenTickets.contains(listItem)) {
              showCommentBottomView(listItem, ctx);
            }
          },
          child: row,
        );
      },
    );
  }

  void showCommentBottomView(
      Map<String, dynamic> ticketItem, BuildContext ctx) {
    showModalBottomSheet(
        context: ctx,
        builder: (sheetCtx) {
          return new Container(
            height: 400.0,
            color: Colors.transparent,
            child: new Column(
              children: <Widget>[
                new Container(
                  height: 24.0,
                  margin: EdgeInsets.fromLTRB(16.0, 16.0, 16.0, 12.0),
                  child: Text(
                    ticketItem['ticket_title'],
                    style: textStyle(
                      size: 18,
                      fontWeight: FontWeight.w600
                    ),
                  ),
                ),
                new Flexible(
                  flex: 1,
                  fit: FlexFit.tight,
                  child: Container(
                    padding: EdgeInsets.fromLTRB(16.0, 6.0, 16.0, 6.0),
                    child: Text(ticketItem['ticket_detail']),
                  ),
                ),
                ticketItem['times'] == 1?
                new Container(
                  margin: EdgeInsets.only(bottom: 20.0),
                  child: new FlatButton(
                    color: Colors.red,
                    padding: new EdgeInsets.all(0.0),
                    child: new Text(
                      "使用卡券",
                      style: this.textStyle(
                          size: 14,
                          color: Colors.white,
                          fontWeight: FontWeight.w500),
                    ),
                    onPressed: () async {
                      var result =
                          await LeanCloudFunction.callFunction(myData: {
                        'user_id': userId,
                        'ticket_id': ticketItem['objectId'].toString(),
                      }, functionName: 'use_ticket');
                      if (result.toString() == 'OK') getTicket();
                    },
                    shape: new RoundedRectangleBorder(
                      borderRadius: new BorderRadius.circular(9.0))),
                ) : new Container(),
              ],
            ),
          );
        });
  }
}
