import 'dart:async';

import 'package:flutter/material.dart';
import 'package:groovin_material_icons/groovin_material_icons.dart';
import 'package:fab_menu/fab_menu.dart';

// Speed Dial

class AddPostFAB extends StatefulWidget {
  @override
  createState() => new _AddPostFABState();
}

class _AddPostFABState extends State<AddPostFAB> {
  // Open Post dialog
  // Future _openPostDialog(PostType type) async {
  //   // TODO: Await shold return some signal
  //   switch (type) {
  //     case PostType.ask:
  //       ;
  //       break;
  //       case PostType.used_book:
  //       Navigator.of(context).pushNamed('/post/used_book');
  //       break;
  //       case PostType.used_book:
  //       Navigator.of(context).pushNamed('/post/used_book');
  //       break;
  //       case PostType.used_book:
  //       Navigator.of(context).pushNamed('/post/used_book');
  //       break;

  //     default:
  //   }
  // await Navigator.of(context).push(
  //   new MaterialPageRoute(
  //     builder: (BuildContext context) {
  //       return new Ask();
  //     },
  //     fullscreenDialog: true,
  //   ),
  // );
  // }

  //   await Navigator.of(context).push(
  //     new MaterialPageRoute(
  //       builder: (BuildContext context) {
  //         return new PlatformScaffold();
  //       },
  //       // fullscreenDialog: true,
  //     ),
  //   );

  // return await Navigator.pushNamed(context, '/post').then(
  //   (success) => Scaffold.of(context).showSnackBar(new SnackBar(
  //         content: Text('Success!'),
  //       )),
  // );
  // if (save != null) {
  //   _addWeightSave(save);
  // }
  // }

  @override
  Widget build(BuildContext context) {
    List<MenuData> dialers = [
      new MenuData(GroovinMaterialIcons.comment_question,
          (context, menuData) => Navigator.of(context).pushNamed('/post/ask'),
          labelText: '提问'),
      new MenuData(
          GroovinMaterialIcons.book,
          (context, menuData) =>
              Navigator.of(context).pushNamed('/post/used_book'),
          labelText: '二手书'),
      new MenuData(
          GroovinMaterialIcons.atom,
          (context, menuData) =>
              Navigator.of(context).pushNamed('/post/second_hand'),
          labelText: '闲置品'),
      // new MenuData(
      //     GroovinMaterialIcons.shopping,
      //     (context, menuData) =>
      //         Navigator.of(context).pushNamed('/post/daigou'),
      //     labelText: '代购'),
    ];

    return new FabMenu(
      menus: dialers,
      mainButtonBackgroundColor: Theme.of(context).accentColor,
      // mainIcon: Icons.book,
      // menuButtonBackgroundColor: Theme.of(context).primaryColor,
    );
  }
}
