import 'dart:async';
import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_platform_widgets/flutter_platform_widgets.dart';
import 'package:most/src/models/search_info.dart';
import 'package:most/src/views/question_details.dart';
import 'package:zefyr/zefyr.dart';
import '../common/repository.dart';

import '../api/leancloud.dart';

import 'package:incrementally_loading_listview/incrementally_loading_listview.dart';
import 'package:notus/notus.dart';

const int suggestionLimit = 10;
const String suggestionQueryClasses = '';

Future<bool> _setSearchHistory(List<String> history) async {
  return Repository.get().setSearchHistory(history);
}

class _SearchDemoSearchDelegate extends SearchDelegate<String> {
  var _history = <String>[];

  /// Where search has scrolled to
  String sid;
  List<PostSearchResultEntry> results;

  _SearchDemoSearchDelegate() {
    _initSearchHistory(); // Init asynchronizely
    // _setSearchHistory(['你好']); // TODO: remove this
  }

  Future _initSearchHistory() async {
    _history = await Repository.get().searchHistory;
  }

  @override
  Widget buildLeading(BuildContext context) {
    return new IconButton(
      tooltip: '返回',
      icon: new AnimatedIcon(
        icon: AnimatedIcons.menu_arrow,
        progress: transitionAnimation,
      ),
      onPressed: () {
        close(context, null);
      },
    );
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    // Iterable<String> suggestions;
    // // If it is called by flutter (suggestionRecursionCounter == 0), call itself recursively;
    // // if not, don't call itself.
    // suggestionRecursionCounter = (suggestionRecursionCounter == 0) ? 1 : 0;
    // _getSuggestions(query).then((onValue)=>sugg)
    // final Iterable suggestions =
    //     query.isEmpty ? _history : _getSuggestions(query);

    return new _SuggestionList(
      query: query,
      // suggestions: suggestions,
      history: _history,
      onSelected: (String suggestion) {
        query = suggestion;
        showResults(context);
      },
    );
  }

  @override
  Widget buildResults(BuildContext context) {
    // Add query to history if it is not in history
    if (_history != null && !_history.contains(query)) {
      _history.add(query);
      _setSearchHistory(_history);
    }

    // Build reslut list
    if (query.isEmpty) return Container();
    return _ResultList(query: query);
  }

  @override
  List<Widget> buildActions(BuildContext context) {
    return <Widget>[
      query.isEmpty
          ? new IconButton(
              tooltip: 'Voice Search',
              icon: const Icon(Icons.mic),
              onPressed: () {
                query = 'TODO: implement voice input';
              },
            )
          : new IconButton(
              tooltip: 'Clear',
              icon: const Icon(Icons.clear),
              onPressed: () {
                query = '';
                showSuggestions(context);
              },
            )
    ];
  }
}

class _SuggestionList extends StatefulWidget {
  _SuggestionList({this.query, this.onSelected, this.history});

  final String query;
  final ValueChanged<String> onSelected;
  final List<String> history;

  @override
  _SuggestionListState createState() {
    return new _SuggestionListState();
  }
}

class _SuggestionListState extends State<_SuggestionList> {
  List<String> suggestions;

  // @override
  // void initState() {
  //   // TODO: implement initState
  //   super.initState();
  //   if (widget.query.isEmpty) {
  //     suggestions = widget.history;
  //   } else {
  //     _getSuggestions(widget.query);
  //   }
  // }

  Future<void> _setSuggestionsOf(String keywords) async {
    final SearchResponse response = await LeanCloudFunction.search(
      query: keywords,
      clazz: suggestionQueryClasses,
      limit: suggestionLimit,
      include: 'publisher',
    );
    // response.results.retainWhere((res) => res.title == null);
    if (this.mounted) {
      setState(() {
        suggestions = response.results
            ?.map((entry) => (entry as PostSearchResultEntry).title)
            ?.toList();
        print('suggestions: $suggestions');
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    final ThemeData theme = Theme.of(context);
    // If query is empty, show history.
    if (widget.query.isEmpty) {
      suggestions = widget.history;
    } else {
      _setSuggestionsOf(widget.query);
    }

    if (suggestions == null) {
      return Center(
        child: PlatformCircularProgressIndicator(),
      );
    } else if (suggestions.isEmpty) {
      return Container();
    } else {
      return new ListView.builder(
        itemCount: suggestions?.length ?? 0,
        itemBuilder: (BuildContext context, int i) {
          final String suggestion = suggestions[i];
          if (suggestion == null)
            return Container(); // if no title, return nothing
          final start = suggestion.indexOf(
              widget.query); // Start index of the query in this suggestion
          final suggestionText = (start == -1) // Test if it contains the query
              ? Text(suggestion) // if no, highlight nothing
              : RichText(
                  text: new TextSpan(
                    text: suggestion.substring(0, start), // Before keyword
                    style: theme.textTheme.title
                        .copyWith(fontWeight: FontWeight.bold),
                    children: <TextSpan>[
                      new TextSpan(
                        // Keyword
                        text: suggestion.substring(
                            start, start + widget.query.length),
                        style: theme.textTheme.subhead,
                      ),
                      new TextSpan(
                        // After keyword
                        text: suggestion.substring(start + widget.query.length),
                        style: theme.textTheme.subhead,
                      ),
                    ],
                  ),
                );
          return new ListTile(
            leading: widget.query.isEmpty
                ? const Icon(Icons.history)
                : const Icon(null),
            title: suggestionText,
            onTap: () {
              widget.onSelected(suggestion);
            },
          );
        },
      );
    }
  }
}

/////////////////////////////////////////////////////////////////////

class _ResultList extends StatefulWidget {
  _ResultList({this.query});

  final String query;

  @override
  _ResultListState createState() {
    return new _ResultListState();
  }
}

class _ResultListState extends State<_ResultList> {
  List results;
  bool _hasMoreResults; // Whether we can load more
  bool _loadingMore;
  String sid;

  Widget _buildResultsItem(BuildContext context, int i) {
    // Add a one-pixel-high divider widget before each row in theListView.
    if (i.isOdd) return Divider();
    final index = i ~/ 2;
    // If reach the end of the result list
    if ((_loadingMore ?? false) && index == results.length - 1) {
      return ListTile();
    }
    return _buildRow(results[index]);
  }

  Widget _buildRow(PostSearchResultEntry result) {
    // if (result.authorInfo == null) {
    //   return ListTile();
    // }
    return ListTile(
      isThreeLine: true,
      title: Text(
        result.title,
        overflow: TextOverflow.ellipsis,
        style: Theme.of(context)
            .textTheme
            .subhead
            .copyWith(fontWeight: FontWeight.bold),
      ),
      subtitle: Text(
        NotusDocument.fromJson(json.decode(result.body)).toPlainText(),
        overflow: TextOverflow.ellipsis,
      ),
      onTap: () {
        final questionDetailPage = QuestionDetailPage(
          questionId: result.objectId,
          questionTitle: result.title,
          questionBody: result.body,
          userName: result.publisher?.username,
          userAvatarUrl: result.publisher?.avatar?.uri?.toString(),
          postTime: result.updatedAt,
        );
        if (isCupertino) {
          Navigator.of(context)
              .push(CupertinoPageRoute(builder: (BuildContext context) {
            return questionDetailPage;
          }));
        } else {
          Navigator.of(context)
              .push(MaterialPageRoute(builder: (BuildContext context) {
            return questionDetailPage;
          }));
        }
      },
    );
  }

  /// Returns a [SearchResponse] with [sid] and [results] property.
  /// Use [sid] to achieve offset.
  Future<SearchResponse> _getSearchResults(String keywords,
      {String sid}) async {
    SearchResponse response = (sid == null)
        ? await LeanCloudFunction.search(
            type: SearchType.post,
            query: keywords, include: 'publisher',
            // clazz: suggestionQueryClasses,
            // limit: suggestionLimit,
          )
        : await LeanCloudFunction.search(
            type: SearchType.post,
            query: keywords,
            // clazz: suggestionQueryClasses,
            // limit: suggestionLimit,
            sid: sid,
            include: 'publisher',
          );
    response.results.removeWhere((result) =>
        result.body == null ||
        result.title == null); // Keep anything but without title or body
    // response.results.forEach((res) async {
    //   res.authorInfo = await LeanCloudUser.getUserInfo(res.authorObjectId);
    // });

    return response;
  }

  Future _initLoad() async {
    SearchResponse response = await _getSearchResults(widget.query);
    if (this.mounted) {
      setState(() {
        _hasMoreResults = response.results.isNotEmpty;
        // if (this.mounted) {
        //   setState(() {
        results = response.results;
        sid = response.sid;
      });
    }
    // });
    // }
    // if (widget.query.isEmpty || results.isEmpty) {
    //   return new Center(
    //     child: new Text(
    //       '很抱歉，\n没有找到与"$widget.query"相关的内容。',
    //       textAlign: TextAlign.center,
    //     ),
    //   );
    // }
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: _initLoad(),
        builder: (context, snapshot) {
          // switch (snapshot.connectionState) {
          // case ConnectionState.waiting:
          // return Center(child: PlatformCircularProgressIndicator());
          // case ConnectionState.done:
          if (results == null) {
            return Center(child: PlatformCircularProgressIndicator());
          } else if (results.isEmpty) // No result.
            return Center(
              child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text('😎',
                        style: TextStyle(
                          fontSize: 56.0,
                        )),
                    Container(height: 16.0),
                    Text('没有找到东西噢，换个关键词试试')
                  ]),
            );
          return IncrementallyLoadingListView(
            padding: const EdgeInsets.only(top: 16.0),
            hasMore: () => _hasMoreResults,
            itemCount: () =>
                (results == null) ? 0 : results.length, // Include dividers
            onLoadMore: () {
              setState(() {
                _loadingMore = true;
              });
            },
            onLoadMoreFinished: () {
              setState(() {
                _loadingMore = false;
              });
            },
            loadMoreOffsetFromBottom: 2,
            loadMore: () async {
              final response = await _getSearchResults(widget.query, sid: sid);

              if (this.mounted) {
                setState(() {
                  _hasMoreResults = response
                      .results.isNotEmpty; // Must have this after get results
                  sid = response.sid;
                  results.addAll(response.results);
                });
              }
            },
            itemBuilder: _buildResultsItem,
          );

          //   default:
          //     return Text('遇到了点麻烦...');
          // }
        });
  }
}

////////////////////////////////////////////////////////////////////

class SearchButton extends StatelessWidget {
  final _SearchDemoSearchDelegate _delegate = new _SearchDemoSearchDelegate();

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return new IconButton(
        tooltip: 'Search',
        icon: const Icon(Icons.search),
        onPressed: () {
          showSearch<String>(
            context: context,
            delegate: _delegate,
          );
        });
  }
}

// class SearchBar extends StatelessWidget implements PreferredSize {
//   @override
//   Widget build(BuildContext context) {
//     return Container(
//       child: SafeArea(child: MaterialSearchInput<String>()),
//     );
//   }

//   // TODO: implement child
//   @override
//   Widget get child => MaterialSearchInput<String>();

//   // TODO: implement preferredSize
//   @override
//   Size get preferredSize => Size.fromHeight(kToolbarHeight);
// }

// class MaterialSearchInput<T> extends StatefulWidget {
//   MaterialSearchInput({
//     Key key,
//     this.onSaved,
//     this.validator,
//     this.autovalidate,
//     this.placeholder,
//     this.onSelect,
//   });

//   final FormFieldSetter<T> onSaved;
//   final FormFieldValidator<T> validator;
//   final bool autovalidate;
//   final String placeholder;
//   final ValueChanged<T> onSelect;

//   @override
//   _MaterialSearchInputState<T> createState() =>
//       new _MaterialSearchInputState<T>();
// }

// class _MaterialSearchInputState<T> extends State<MaterialSearchInput<T>> {
//   GlobalKey<FormFieldState<T>> _formFieldKey =
//       new GlobalKey<FormFieldState<T>>();
//   final _SearchDemoSearchDelegate _delegate = new _SearchDemoSearchDelegate();
//   final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

//   // _buildMaterialSearchPage(BuildContext context) {
//   //   return new _MaterialSearchPageRoute<T>(
//   //       settings: new RouteSettings(
//   //         name: 'material_search',
//   //         isInitialRoute: false,
//   //       ),
//   //       builder: (BuildContext context) {
//   //         return new Material(
//   //           child: new MaterialSearch<T>(
//   //             placeholder: widget.placeholder,
//   //             results: widget.results,
//   //             getResults: widget.getResults,
//   //             filter: widget.filter,
//   //             sort: widget.sort,
//   //             onSelect: (dynamic value) => Navigator.of(context).pop(value),
//   //           ),
//   //         );
//   //       });
//   // }

//   // _showMaterialSearch(BuildContext context) {
//   //   Navigator.of(context)
//   //       .push(_buildMaterialSearchPage(context))
//   //       .then((dynamic value) {
//   //     if (value != null) {
//   //       _formFieldKey.currentState.didChange(value);
//   //       widget.onSelect(value);
//   //     }
//   //   });
//   // }

//   bool get autovalidate {
//     return widget.autovalidate ??
//         Form.of(context)?.widget?.autovalidate ??
//         false;
//   }

//   bool _isEmpty(field) {
//     return field.value == null;
//   }

//   Widget build(BuildContext context) {
//     final TextStyle valueStyle = Theme.of(context).textTheme.display3;

//     return InkWell(
//       onTap: () => showSearch<String>(
//             context: context,
//             delegate: _delegate,
//           ),
//       child: new FormField<T>(
//         key: _formFieldKey,
//         validator: widget.validator,
//         onSaved: widget.onSaved,
//         autovalidate: autovalidate,
//         builder: (FormFieldState<T> field) {
//           return new InputDecorator(
//             baseStyle: valueStyle,
//             isEmpty: _isEmpty(field),
//             decoration: new InputDecoration(
//               labelStyle: _isEmpty(field) ? null : valueStyle,
//               labelText: widget.placeholder,
//               errorText: field.errorText,
//             ),
//             child: _isEmpty(field)
//                 ? null
//                 : new Text(
//                     // widget.formatter != null
//                     // ? widget.formatter(field.value)
//                     field.value.toString(),
//                     style: valueStyle),
//           );
//         },
//       ),
//     );
//   }
// }
