import 'package:flutter/material.dart';
import 'package:date_format/date_format.dart';

class ImageTextCard extends StatefulWidget {
  final String imageStirng;
  final String title;
  final bool isNetImage;
  ImageTextCard({Key key, this.imageStirng, this.isNetImage, this.title}):super(key: key);
  @override
  State<StatefulWidget> createState() {
    return new ImageTextCardState(imageStirng: imageStirng, isNetImage: isNetImage, title: title);
  }
}

class ImageTextCardState extends State<ImageTextCard> {

  String imageStirng;
  String title;
  bool isNetImage;
  //RegExp regExp1 = new RegExp("</.*>");
  //RegExp regExp2 = new RegExp("<.*>");
  TextStyle subtitleStyle = new TextStyle(
      fontSize: 12.0,
      color: const Color(0xFFB5BDC0)
  );
  TextStyle contentStyle = new TextStyle(
      fontSize: 15.0,
      color: Colors.black
  );

  ImageTextCardState({Key key, this.imageStirng, this.title, this.isNetImage});

  //设置字体
  textStyle({int size, Color color, FontWeight fontWeight}) {
    return new TextStyle(
      fontSize: size.toDouble(),
      decoration: TextDecoration.none,
      color: color,
      fontWeight: fontWeight,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        new Container(
          width: 44.0,
          height: 44.0,
          decoration: new BoxDecoration(
            image: new DecorationImage(
                image: getImage(),
            ),
            borderRadius: new BorderRadius.all(
              const Radius.circular(22.0),
            ),
          ),
        ),
        new Container(
          margin: EdgeInsets.fromLTRB(0.0, 5.0, 0.0, 0.0),
          child: new Text(title, style: textStyle(size: 10, color: Colors.black, fontWeight: FontWeight.w600,)),
        )
        
      ],
    );
  }

  ImageProvider getImage() {
    if (isNetImage == true) {
      return new NetworkImage(imageStirng);
    } else {
      return new AssetImage(imageStirng);
    }
  }

}