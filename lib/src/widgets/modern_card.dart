import 'package:date_format/date_format.dart';
import 'package:flutter/material.dart';
import 'package:groovin_material_icons/groovin_material_icons.dart';
import 'package:most/src/models/search_info.dart';
import 'package:most/src/utils/utils.dart';
import 'package:most/src/views/goods_details.dart';
import 'package:most/src/views/question_details.dart';

class InfoCard extends StatefulWidget {
  final String title;
  final List<PostSearchResultEntry> data;
  const InfoCard({
    Key key,
    this.title,
    this.data,
  }) : super(key: key);

  @override
  InfoCardState createState() {
    return new InfoCardState();
  }
}

class InfoCardState extends State<InfoCard> {
  @override
  Widget build(BuildContext context) {
    return Card(
      color: Colors.white,
      elevation: 0.0,
      shape: RoundedRectangleBorder(),
      child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            new InfoTile(
              options: <Widget>[
                IconButton(
                  iconSize: 20.0,
                  padding: const EdgeInsets.all(0.0),
                  onPressed: () => {},
                  icon: Icon(GroovinMaterialIcons.star_outline),
                ),
                IconButton(
                  padding: const EdgeInsets.all(0.0),
                  onPressed: () => {},
                  iconSize: 20.0,
                  icon: Icon(GroovinMaterialIcons.dots_vertical),
                )
              ],
            ),
            Divider(),
            FlatButton(
              padding: EdgeInsets.all(16.0),
              onPressed: () => {},
              child: Stack(
                fit: StackFit.loose,
                children: <Widget>[
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: <Widget>[
                      Text(
                        'Ali Connor - 25 min ago',
                        style: Theme.of(context)
                            .textTheme
                            .body1
                            .copyWith(fontWeight: FontWeight.w300),
                      ),
                      Container(height: 4.0),
                      Text(
                        '你好',
                        style: Theme.of(context)
                            .textTheme
                            .title
                            .copyWith(fontWeight: FontWeight.w800),
                      ),
                      Container(height: 8.0),
                      Text("我会帮你干好活的！不必担心。"),
                    ],
                  ),
                  Positioned(
                    right: -8.0,
                    top: -16.0,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        IconButton(
                          iconSize: 20.0,
                          padding: const EdgeInsets.all(0.0),
                          onPressed: () => {},
                          icon: Icon(GroovinMaterialIcons.star_outline),
                        ),
                        IconButton(
                          padding: const EdgeInsets.all(0.0),
                          onPressed: () => {},
                          iconSize: 20.0,
                          icon: Icon(GroovinMaterialIcons.dots_vertical),
                        )
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ]),
    );
  }
}

class InfoTile extends StatefulWidget {
  const InfoTile({
    Key key,
    this.authorUsername,
    this.createdAt,
    this.title,
    this.body,
    this.options,
    this.authorObjectId,
    this.authorAvatarUrl,
    this.objectId,
    this.tag,
  }) : super(key: key);

  final String authorUsername;
  final String authorObjectId;
  final String authorAvatarUrl;
  final String objectId;
  final String createdAt;
  final String title;
  final String body;
  final List<Widget> options;
  final int tag;

  @override
  InfoTileState createState() {
    return new InfoTileState();
  }
}

class InfoTileState extends State<InfoTile> {
  String get actionName {
    switch (widget.tag) {
      case 1:
        return '发布于';
      case 2:
        return '回答于';
      case 3:
        return '评论于';
      default:
        return null;
    }
  }

  @override
  Widget build(BuildContext context) {
    return FlatButton(
      padding: EdgeInsets.all(16.0),
      onPressed: () {
        if (widget.tag == 1) {
          Navigator.of(context).push(new MaterialPageRoute(builder: (ctx) {
            return new QuestionDetailPage(
                userId: widget.authorObjectId,
                questionId: widget.objectId,
                postTime: widget.createdAt,
                questionBody: widget.body,
                questionTitle: widget.title,
                userAvatarUrl: widget.authorAvatarUrl,
                userName: widget.authorUsername);
          }));
        } else if (widget.tag == 2 || widget.tag == 3) {
          Navigator.of(context).push(new MaterialPageRoute(builder: (ctx) {
            return new QuestionDetailPage(
                userId: widget.authorObjectId,
                questionId: widget.objectId,
                postTime: widget.createdAt,
                questionBody: widget.body,
                questionTitle: widget.title,
                userAvatarUrl: widget.authorAvatarUrl,
                userName: widget.authorUsername);
          }));
        } else if (widget.tag == 4 || widget.tag == 5) {
          Navigator.of(context).push(new MaterialPageRoute(builder: (ctx) {
            return new GoodsDetailPage(
                tag: widget.tag, tradeId: widget.objectId);
          }));
        }
      },
      child: Stack(
        fit: StackFit.loose,
        overflow: Overflow.visible,
        children: <Widget>[
          Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              Row(
                children: <Widget>[
                  LimitedBox(
                    child: Text(
                      widget.authorUsername,
                      overflow: TextOverflow.ellipsis,
                    ),
                    maxWidth: 56.0,
                  ),
                  Text(
                    ' $actionName ${getDate(DateTime.parse(widget.createdAt))}',
                    style: Theme.of(context)
                        .textTheme
                        .body1
                        .copyWith(fontWeight: FontWeight.w300),
                  ),
                ],
              ),
              Container(height: 4.0),
              Text(
                widget.title,
                style: Theme.of(context)
                    .textTheme
                    .title
                    .copyWith(fontWeight: FontWeight.w800),
              ),
              Container(height: 8.0),
              Text(widget.body),
            ],
          ),
          Positioned(
            right: -8.0,
            top: -16.0,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: widget.options,
            ),
          ),
        ],
      ),
    );
  }
}
