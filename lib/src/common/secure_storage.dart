// import 'dart:async';

import 'dart:async';

import 'package:flutter_secure_storage/flutter_secure_storage.dart';
// import 'package:most/src/common/events.dart';

class SecureStorage extends FlutterSecureStorage {
  /// If no value corresponds to key, return null
  // Future<String> getUsername() async {
  //   return readAll().then((onValue) {
  //     if (onValue.containsKey('username')) {
  //       return onValue['username'];
  //     }
  //   });
  // }

  // setUsername(String username) async {
  //   await write(key: 'username', value: username);
  // }

  /// If no value corresponds to key, return null
  Future<String> getSessionToken() async {
    return readAll().then((onValue) {
      if (onValue.containsKey('sessionToken')) {
        return onValue['sessionToken'];
      }
    });
  }

  Future<void> setSessionToken(String token) async =>
      await write(key: 'sessionToken', value: token);

  // /// This is what determines different users
  // Future<String> getThisUserObjectId() async {
  //   return readAll().then((onValue) {
  //     print(onValue);
  //     if (onValue.containsKey('objectId')) {
  //       return onValue['objectId'];
  //     }
  //   });
  // }

  // setObjectId(String objectId) async {
  //   await write(key: 'objectId', value: objectId);
  // }

//   /// This is what determines different users
//   String get objectId async {
//     String objectId;
//     return await readAll().then((onValue) {
//       if (onValue.containsKey('objectId')) {
//         objectId = onValue['objectId'];
//         return objectId;
//       }
//     });
//     return Future.wait(objectId);
//   }

//   set objectId(String token) async => write(key: 'objectId', value: token);
// }

// void a() {
//   _secureStorage.readAll().then((storage) {
//     storage.keys.contains(element)
//   })
}

// // Read value
// String value = await storage.read(key: key);

// // Read all values
// Map<String, String> await storage.readAll();

// // Delete value
// await storage.delete(key: key);

// // Delete all
// await storage.deleteAll();

// // Write value
// await storage.write(key: key, value: value);
