import 'package:event_bus/event_bus.dart';
import 'package:meta/meta.dart';
import 'package:most/src/api/leancloud.dart';

/// The global [EventBus] object.
EventBus eventBus = EventBus();

/// Get user's ID token
class LoginUser {
  final String user;
  LoginUser(this.user);
}

/// Used for opening the microsoft login webview.
class OpenLoginWebViewEvent {
  final String url;
  OpenLoginWebViewEvent(this.url);
}

/// Validate user's id against the server
// https://leancloud.cn/docs/rest_api.html#hash1028771764
class ValidateUserId {
  final String openid;
  final String accessToken;
  final String expireIn;
  ValidateUserId(
      {@required this.accessToken,
      @required this.expireIn,
      @required this.openid});
}

/// Close login webview, enter home page, and update User data model.
class UserLoggedInEvent {
  final String username;
  final String sessionToken;
  final FileData avatar;
  final bool isNewUser;
  UserLoggedInEvent({
    @required this.username,
    @required this.sessionToken,
    @required this.isNewUser,
    @required this.avatar,
  });
}

/// Clear secure storage, enter login page.
class UserLoggedOutEvent {
  UserLoggedOutEvent();
}

class ErrorEvent {
  final String error;
  ErrorEvent(this.error);
}

/// Fired when the webview receives the id token
class UserIdTokenRetrieved {
  final String idToken;
  UserIdTokenRetrieved(this.idToken);
}

/// Prompt a message about what it is doing
class InActionEvent {
  final String message;
  InActionEvent(this.message);
}
