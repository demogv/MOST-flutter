import 'dart:async';
import 'dart:io';

import 'package:dio/dio.dart';
import 'package:most/src/models/post_type.dart';
import 'package:most/src/models/search_info.dart';
import 'package:most/src/models/user_info.dart';

import 'secure_storage.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../api/leancloud.dart';

import 'package:event_bus/event_bus.dart';

import '../models/user.dart';

/// The centralized database. Note that it is a singleton.
class Repository {
  // Databases
  SecureStorage secureStorage;
  Future<SharedPreferences> preferences;
  LeanCloud leancloud;
  EventBus eventBus;

  // // Data
  // String sessionToken;

  // Singleton pattern
  static final Repository _repo = Repository._internal();
  static Repository get() => _repo;
  Repository._internal() {
    // this is the initializer
    preferences = SharedPreferences.getInstance();
    secureStorage = SecureStorage();
    leancloud = LeanCloud();
    eventBus = EventBus();
  }

  // // Private methods
  // Future<void> _initSharedPreferences() async {
  //   preferences = await SharedPreferences.getInstance();
  // }

  // Public properties
  static final defaultfAvatarUri = 'asserts/icons/default_avatar.png';

  /// Stores anything requiring extra protection
  // static final SecureStorage secureStorage = SecureStorage();

  // /// Store anything may not be secure. Prefix [await] before invocation.
  // static final Future<SharedPreferences> preferences =
  //     SharedPreferences.getInstance();

  static final mostLogoUri = 'asserts/icons/g116.png';

  Future<Options> get leancloudOptions async => Options(
        baseUrl: baseUrl,
        connectTimeout: 100000,
        receiveTimeout: 100000,
        headers: {
          'X-LC-Id': appId,

          /// Use 'X-LC-Sign' for more security
          'X-LC-Sign': appSign,
          // 'X-LC-Key': appKey,
          'X-LC-Session': await sessionToken,
        },
        contentType: ContentType.json,
        responseType: ResponseType.JSON,
      );

  // Public methods
  Future<bool> get loginStatus async {
    // print(await secureStorage.readAll());
    return await sessionToken != null &&
        await thisUsername != null &&
        await thisUserObjectId != null;
  }

  /// Object ID of the current user
  Future<String> get thisUserObjectId async =>
      (await preferences).getString('thisUserObjectId');

  Future<bool> setThisUserObjectId(String id) async =>
      (await preferences).setString('thisUserObjectId', id);

  Future<String> get thisUsername async =>
      (await preferences).getString('username');

  //// User name of the current user
  Future<bool> setThisUsername(String username) async =>
      (await preferences).setString('username', username);

  Future<String> get sessionToken async {
    return await secureStorage.getSessionToken();
  }

  Future<void> setSessionToken(String token) async =>
      await secureStorage.setSessionToken(token);

  Future<List<String>> get searchHistory async {
    final result = (await preferences).getStringList('searchHistory');
    if (result == null) return <String>[]; // avoid returning null
    return result;
  }

  Future<bool> setSearchHistory(List<String> history) async =>
      (await preferences).setStringList('searchHistory', history);

  Future<UserInfo> get thisUserInfo async => LeanCloudUser.getThisUserInfo();

  Future<UserInfo> userInfoOf(String userObjectId) {
    return LeanCloudUser.getUserInfo(userObjectId);
  }

  Future<void> signOut() async {
    await User.signOut();
  }

  Future<void> uploadQA(
      {String title, String content, TagSearchResultEntry tag}) async {
    LeanCloudFunction.callFunctionWithToken(myData: {
      'user_id': await thisUserObjectId,
      'question_title': title,
      'question_detail': content,
      'father_group': tag.fatherGroup, // TODO: debug
      'sub_group': tag.subGroup,
    }, functionName: 'upload_QA', sessionToken: await sessionToken);
  }

  Future<void> uploadTrade(
      {String title,
      String content,
      List<FileData> images,
      double price,
      double originalPrice,
      TagSearchResultEntry tagInfo,
      int tagOfThis}) async {
    // TODO: so dirty
    await LeanCloudFunction.callFunctionWithToken(myData: {
      'user_id': await thisUserObjectId,
      'trade_title': title,
      'trade_detail': content,
      'father_group': tagInfo.fatherGroup, // TODO: debug
      'sub_group': tagInfo.subGroup,
      'tag': tagOfThis,
      'images': images?.map((img) => img?.toJson())?.toList(),
      'price': price,
      'original_price': originalPrice,
    }, functionName: 'upload_Trade', sessionToken: await sessionToken);
  }

  Future<void> uploadAnswer(
      {String questionerObjectId,
      String questionObjectId,
      String answerBody}) async {
    await LeanCloudFunction.callFunctionWithToken(myData: {
      'user_id': await thisUserObjectId,
      'question_id': questionObjectId,
      'question_user_id': questionerObjectId,
      'answer_detail': answerBody,
    }, functionName: 'upload_answer', sessionToken: await sessionToken);
  }

  Future<Set<TagSearchResultEntry>> recommendedPostTagsOf(
      String keyword) async {
    final searchData = await LeanCloudFunction.search(
        query: keyword, clazz: 'Group_info_QA', type: SearchType.tag);
    print(searchData?.results);
    Set<TagSearchResultEntry> recommendedTags =
        searchData?.results?.toSet(); // TODO: fix this
    return recommendedTags;
  }

  Future<Set<TagSearchResultEntry>> allTagsOf(PostType type) async {
    if (type == PostType.ask) {
      return (await LeanCloudFunction.query(className: 'Group_info_QA'))
          .map((tag) => TagSearchResultEntry.fromJson(tag))
          .toSet();
    } else {
      return (await LeanCloudFunction.query(className: 'Group_info_Trade'))
          .map((tag) => TagSearchResultEntry.fromJson(tag))
          .toSet();
    }
  }

  Future<List<PostSearchResultEntry>> get thisUserTransactions async {
    List<Map> rawTransactions = await LeanCloudFunction.callFunctionWithToken(
        myData: {'user_id': await thisUserObjectId, 'start': 0, 'end': -1},
        functionName: 'user_trade_order_by_date',
        sessionToken: await sessionToken);
    return rawTransactions
        .map((transaction) => PostSearchResultEntry.fromJson(transaction))
        .toList();
  }

  Future<List<PostSearchResultEntry>> get thisUserPosts async {
    List<Map> rawTransactions = await LeanCloudFunction.callFunctionWithToken(
        myData: {'user_id': await thisUserObjectId, 'start': 0, 'end': -1},
        functionName: 'user_question_order_by_date',
        sessionToken: await sessionToken);
    return rawTransactions
        .map((transaction) => PostSearchResultEntry.fromJson(transaction))
        .toList();
  }
}
