class Answer {
  final String title;
  final String body; // Might be of rich text

  final String auther; //Might be a User

  Answer(this.title, this.body, this.auther);
}
