// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'login_info.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

LoginInfo _$LoginInfoFromJson(Map<String, dynamic> json) {
  return LoginInfo(
      objectId: json['objectId'] as String,
      username: json['username'] as String,
      sessionToken: json['sessionToken'] as String,
      avatar: FileData.fromJson(json['avatar']),
      isNewUser: json['isNewUser'] as bool);
}

Map<String, dynamic> _$LoginInfoToJson(LoginInfo instance) => <String, dynamic>{
      'username': instance.username,
      'sessionToken': instance.sessionToken,
      'isNewUser': instance.isNewUser,
      'objectId': instance.objectId,
      'avatar': instance.avatar.toJson()
    };
