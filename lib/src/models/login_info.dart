import 'package:json_annotation/json_annotation.dart';
import 'package:meta/meta.dart';
import 'package:most/src/api/leancloud.dart';
part 'login_info.g.dart';

@JsonSerializable(nullable: false)
class LoginInfo {
  final String username;
  final String sessionToken;
  final bool isNewUser;
  final String objectId;
  final FileData avatar;

  LoginInfo(
      {@required this.avatar,
      @required this.objectId,
      @required this.username,
      @required this.sessionToken,
      this.isNewUser = false});
  factory LoginInfo.fromJson(Map<String, dynamic> json) =>
      _$LoginInfoFromJson(json);
  Map<String, dynamic> toJson() => _$LoginInfoToJson(this);
}
