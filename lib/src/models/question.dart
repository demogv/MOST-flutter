import 'package:meta/meta.dart';

import 'answer.dart';

class Question {
  final List<String> tags;

  final String title;
  final String body; // Maybe of rich text
  final String auther; // Might be a User

  final int followerCount;
  final int answerCount;

  final List<Answer> answers;

  Question(this.tags, this.title, this.body, this.auther, this.followerCount,
      this.answerCount, this.answers);
}
