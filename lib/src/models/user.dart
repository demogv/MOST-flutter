import 'dart:async';

import '../common/events.dart';
import '../common/repository.dart';
import '../api/leancloud.dart';
import '../api/microsoft_graph_api.dart';

class User {
  // User(this.name, this.email);

  String name;
  String email;
  String studentId;

  static Future<Null> signUpOrLogIn({String studentId}) async {
    // get ID token from MS
    await getIdToken(user: studentId).catchError((onError) {
      print('getTokenError: $onError');
      signOut();

      // Send to LeanCloud for validation
    }).then((idToken) async {
      print(idToken);
      if (idToken != null) {
        return await leanCloud.user.loginWithIdToken(idToken: idToken);
      }
    }).catchError((onError) {
      print('leancloud Error:$onError');
      signOut();

      // Save info and fire login event
    }).then((loginInfo) {
      if (loginInfo != null) {
        // Save info
        Repository.get().setSessionToken(loginInfo.sessionToken);
        Repository.get().setThisUsername(loginInfo.username);
        Repository.get().setThisUserObjectId(loginInfo.objectId);

        // Fire login event
        eventBus.fire(UserLoggedInEvent(
            username: loginInfo.username,
            sessionToken: loginInfo.sessionToken,
            isNewUser: loginInfo.isNewUser,
            avatar: loginInfo.avatar));

        // // Login with platform specific code
        // LeancloudSDK.LeancloudUser.loginWithSessionToken(
        //     loginInfo.sessionToken);
      } else {
        eventBus.fire(ErrorEvent('No user info!'));
      }
    });
  }

  /// Signs out from Azure AD, LeanCloud SDK, and
  /// LeanCloud (REST API). And clears secure storage.
  /// Delete including caches and any other insensitive info.
  static Future<void> signOut() async {
    signOutAzureAD();

    final String sessionToken = await Repository.get().sessionToken;
    final String objectId = await Repository.get().thisUserObjectId;
    if (sessionToken != null && objectId != null) {
      LeanCloudUser.logout(sessionToken: sessionToken, objectId: objectId);
    }

    Repository.get().secureStorage.deleteAll();
    (await Repository.get().preferences).clear();
  }
}
