import 'package:most/src/api/leancloud.dart';

/// Infomation that can be edited by user mutually.
class UserInfo {
  final String objectId;
  final String username;
  final String bio;
  final FileData avatar;
  final String createdAt;
  final String sessionToken;
  final String alipayAccount;

  const UserInfo(this.avatar, this.username, this.objectId, this.createdAt,
      this.sessionToken, this.alipayAccount, this.bio);

  UserInfo.fromJson(Map<String, dynamic> json)
      : avatar = FileData.fromJson(json['avatar']),
        username = json['username'],
        bio = json['bio'],
        objectId = json['objectId'],
        createdAt = json['createdAt'],
        sessionToken = json['sessionToken'],
        alipayAccount = json['alipay_account'];

  // Map<String, dynamic> toJson() => {
  //       'username': username,
  //       'avatar': avatar,
  //       'objectId': objectId,
  //       'createdAt': createdAt,
  //       'sessionToken': sessionToken,
  //     };
}

enum userInfoType {
  post,
  goods,
  favorite,
  history,
  transaction,
  followed,
  followers,
  coupons
}
