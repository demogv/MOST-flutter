import 'package:meta/meta.dart';
import 'package:most/src/models/user_info.dart';

class SuggestionsEntry {
  final title;

  SuggestionsEntry({@required this.title});

  SuggestionsEntry.fromJson(Map<String, String> json) : title = json['title'];

  Map<String, String> toJson() => {
        'title': title,
      };
}

enum SearchType { post, person, tag }

class SearchResponse<SearchResultEntry> {
  List results; // 符合查询条件的结果文档。
  final int hits; // 符合查询条件的文档总数
  final String sid; // 标记本次查询结果，下次查询继续传入这个 sid 用于查找后续的数据，用来支持翻页查询。

  SearchResponse(this.results, this.hits, this.sid);
  SearchResponse.fromJson(Map<String, dynamic> json,
      {SearchType searchType = SearchType.post})
      : results = (json['results']).map((entry) {
          switch (searchType) {
            case SearchType.post:
              return PostSearchResultEntry.fromJson(entry);
            case SearchType.tag:
              return TagSearchResultEntry.fromJson(entry);
            default:
          }
        })?.toList(),
        hits = json['hits'],
        sid = json['sid'];
}

// class SearchResults {
//   final String objectId;
//   final String username;
//   final String avatar;
//   final String createdAt;

//   const SearchResults(
//       this.avatar, this.username, this.objectId, this.createdAt);

//   SearchResults.fromJson(Map<String, dynamic> json)
//       : avatar = json['avatar'],
//         username = json['username'],
//         objectId = json['objectId'],
//         createdAt = json['createdAt'];

//   Map<String, dynamic> toJson() => {
//         'username': username,
//         'avatar': avatar,
//         'objectId': objectId,
//         'createdAt': createdAt,
//       };
// }

class SearchResultEntry {
  SearchResultEntry.fromJson();
  SearchResultEntry();
}

class TagSearchResultEntry extends SearchResultEntry {
  final int fatherGroup;
  final int subGroup;
  final String groupTitle;
  final String groupDescription;

  TagSearchResultEntry(
      {this.fatherGroup,
      this.subGroup,
      this.groupTitle,
      this.groupDescription});

  TagSearchResultEntry.fromJson(Map<String, dynamic> json)
      : fatherGroup = json['father_group'],
        subGroup = json['sub_group'],
        groupTitle = json['group_title'],
        groupDescription = json['group_description'];
}

class PostSearchResultEntry {
  final String appUrl;
  final String deeplink;
  final String updatedAt;
  final String createdAt;
  final String objectId;
  final String title;
  final String body;
  final double price;
  final List likedUserList;
  // final String authorUsername;
  // final String autherAvatarUri;
  final Map<String, dynamic> raw;
  final UserInfo publisher;

  PostSearchResultEntry(
      {this.objectId,
      this.createdAt,
      this.appUrl,
      this.deeplink,
      this.updatedAt,
      this.title,
      this.body,
      this.raw,
      this.likedUserList,
      // this.authorUsername,
      // this.autherAvatarUri,
      this.publisher,
      this.price});

  // // Update complete user info
  // Future _getAuthorInfo() async {
  //   this.authorInfo = await LeanCloudUser.getUserInfo(this.authorObjectId);
  // }

  PostSearchResultEntry.fromJson(Map<String, dynamic> json)
      : appUrl = json['_app_url'],
        deeplink = json['_deeplink'],
        updatedAt = json['updatedAt'],
        createdAt = json['createdAt'],
        objectId = json['objectId'],
        title = json['question_title'] ?? json['trade_title'],
        body = json['trade_detail'] ??
            json['answer_detail'] ??
            json['question_detail'], // Order matters!
        likedUserList = json['liked_user_list'],
        // authorUsername = json['username'],
        publisher = json['publisher'] != null
            ? UserInfo.fromJson(json['publisher'])
            : null,
        price = json['price'],
        // autherAvatarUri =
        //     json['user_image'] != null ? json['user_image']['url'] : null,
        raw = json;
}
