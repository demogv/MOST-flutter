import 'dart:io';

import 'package:flutter/material.dart';
import 'package:most/src/widgets/add_post_fab.dart';
import 'package:most/src/widgets/card.dart';
import 'package:most/src/widgets/modern_card.dart';
import 'package:most/src/widgets/search_bar.dart';
import '../common/repository.dart';
import '../../src/api/leancloud.dart';
import '../../src/widgets/image_text_widget.dart';
import 'package:fab_menu/fab_menu.dart';
import 'package:flutter_platform_widgets/flutter_platform_widgets.dart';
import 'package:groovin_material_icons/groovin_material_icons.dart';

class CirclesPage extends StatefulWidget {
  CirclesPage({Key key}) : super(key: key);
  @override
  State<StatefulWidget> createState() {
    return new CirclesPageState();
  }
}

class CirclesPageState extends State<CirclesPage> {
  List followingUsers;
  List dataQA;
  List dataTrade;
  List dataRecommend;
  String userId;

  TextStyle subtitleStyle =
      new TextStyle(fontSize: 12.0, color: const Color(0xFFB5BDC0));
  TextStyle contentStyle = new TextStyle(fontSize: 15.0, color: Colors.black);

  CirclesPageState({Key key});

  //设置字体
  textStyle({int size, Color color, FontWeight fontWeight}) {
    return new TextStyle(
      fontSize: size.toDouble(),
      decoration: TextDecoration.none,
      color: color,
      fontWeight: fontWeight,
    );
  }

  setData() async {
    String userIdGet = await Repository.get().thisUserObjectId;
    // var followingUsersData = await LeanCloudFunction.query(
    //   body: {'include': 'followee'},
    //   className: '/users/$userIdGet/followers',
    //   contentType: ContentType.json,
    // );
    var newDataQA = await LeanCloudFunction.callFunction(
        myData: {'user_id': userIdGet, 'start': 0, 'end': 2},
        functionName: 'user_question_order_by_date');
    var newDataTrade = await LeanCloudFunction.callFunction(
        myData: {'user_id': userIdGet, 'start': 0, 'end': 2},
        functionName: 'user_trade_order_by_date');
    var newDataRecommend = await LeanCloudFunction.callFunction(
        myData: {'user_id': '5b3b9f4fe37d040039df5a6a'},
        functionName: 'getAnswer');
    if (this.mounted) {
      // ensure the object is still in the tree.
      this.setState(() {
        userId = userIdGet;
        // followingUsers = followingUsersData;
        dataQA = newDataQA;
        dataTrade = newDataTrade;
        dataRecommend = newDataRecommend;
        PageStorage.of(context)
            .writeState(context, userId, identifier: 'userId');
        PageStorage.of(context)
            .writeState(context, followingUsers, identifier: 'followingUsers');
        PageStorage.of(context)
            .writeState(context, dataQA, identifier: 'dataQA');
        PageStorage.of(context)
            .writeState(context, dataTrade, identifier: 'dataTrade');
      });
    }
  }

  @override
  initState() {
    super.initState();
    setData();
  }

  @override
  Widget build(BuildContext context) {
    userId = PageStorage.of(context).readState(context, identifier: 'userId');
    followingUsers = PageStorage.of(context)
        .readState(context, identifier: 'followingUsers');
    dataQA = PageStorage.of(context).readState(context, identifier: 'dataQA');
    dataTrade =
        PageStorage.of(context).readState(context, identifier: 'dataTrade');
    var _body = (userId == null &&
            // followingUsers == null &&
            dataQA == null &&
            dataTrade == null)
        ? new Center(
            child: PlatformCircularProgressIndicator(
              android: (_) => MaterialProgressIndicatorData(
                    backgroundColor: Color(0XFFFF2D55),
                  ),
              ios: (_) => CupertinoProgressIndicatorData(),
            ),
          )
        : new ListView(
            children: <Widget>[
              // new Container(
              //   height: 64.0,
              //   margin: EdgeInsets.fromLTRB(0.0, 20.0, 20.0, 0.0),
              //   child: new Row(
              //     children: <Widget>[
              //       new Container(
              //         margin: EdgeInsets.fromLTRB(25.0, 0.0, 7.0, 0.0),
              //         child: Text(
              //           '我的关注',
              //           style: textStyle(
              //               size: 16,
              //               color: Color(0XFF545454),
              //               fontWeight: FontWeight.w600),
              //         ),
              //       ),
              //       new Container(
              //         width: 1.0,
              //         height: 64.0,
              //         margin: EdgeInsets.all(0.0),
              //         color: Color(0XFFA5A5A5),
              //       ),
              //       new Flexible(
              //         fit: FlexFit.tight,
              //         flex: 1,
              //         child: setFollowerWidget(),
              //       ),
              //     ],
              //   ),
              // ),
              Container(
                padding: const EdgeInsets.fromLTRB(16.0, 20.0, 16.0, 8.0),
                child: Text(
                  '问答讯息',
                  style: Theme.of(context)
                      .textTheme
                      .subhead
                      .copyWith(fontWeight: FontWeight.w800),
                ),
              ),
              Theme(
                data: Theme.of(context)
                    .copyWith(primaryColor: Color.fromRGBO(35, 47, 52, 1.0)),
                child: new InfoCard(),
              ),
              Container(
                padding: const EdgeInsets.fromLTRB(16.0, 20.0, 16.0, 8.0),
                child: Text(
                  '交易讯息',
                  style: Theme.of(context)
                      .textTheme
                      .subhead
                      .copyWith(fontWeight: FontWeight.w800),
                ),
              ),
              Theme(
                data: Theme.of(context)
                    .copyWith(primaryColor: Color.fromRGBO(35, 47, 52, 1.0)),
                child: Card(
                  color: Colors.white,
                  elevation: 0.0,
                  shape: RoundedRectangleBorder(),
                  child: Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: <Widget>[
                        FlatButton(
                          padding: EdgeInsets.all(16.0),
                          onPressed: () => {},
                          child: Stack(
                            fit: StackFit.loose,
                            overflow: Overflow.visible,
                            children: <Widget>[
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.stretch,
                                children: <Widget>[
                                  Text(
                                    'Ali Connor - 25 min ago',
                                    style: Theme.of(context)
                                        .textTheme
                                        .body1
                                        .copyWith(fontWeight: FontWeight.w300),
                                  ),
                                  Container(height: 4.0),
                                  Text(
                                    '抢到课了',
                                    style: Theme.of(context)
                                        .textTheme
                                        .title
                                        .copyWith(fontWeight: FontWeight.w800),
                                  ),
                                  Container(height: 8.0),
                                  Text("我会帮你干好活的！不必担心。"),
                                ],
                              ),
                              Positioned(
                                right: -8.0,
                                top: -16.0,
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    IconButton(
                                      iconSize: 20.0,
                                      padding: const EdgeInsets.all(0.0),
                                      onPressed: () => {},
                                      icon: Icon(
                                          GroovinMaterialIcons.star_outline),
                                    ),
                                    IconButton(
                                      padding: const EdgeInsets.all(0.0),
                                      onPressed: () => {},
                                      iconSize: 20.0,
                                      icon: Icon(
                                          GroovinMaterialIcons.dots_vertical),
                                    )
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                        Divider(),
                        FlatButton(
                          padding: EdgeInsets.all(16.0),
                          onPressed: () => {},
                          child: Stack(
                            fit: StackFit.loose,
                            children: <Widget>[
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.stretch,
                                children: <Widget>[
                                  Text(
                                    'Ali Connor - 25 min ago',
                                    style: Theme.of(context)
                                        .textTheme
                                        .body1
                                        .copyWith(fontWeight: FontWeight.w300),
                                  ),
                                  Container(height: 4.0),
                                  Text(
                                    'CSC 1002 怎么样',
                                    style: Theme.of(context)
                                        .textTheme
                                        .title
                                        .copyWith(fontWeight: FontWeight.w800),
                                  ),
                                  Container(height: 8.0),
                                  Text("我会帮你干好活的！不必担心。"),
                                ],
                              ),
                              Positioned(
                                right: -8.0,
                                top: -16.0,
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    IconButton(
                                      iconSize: 20.0,
                                      padding: const EdgeInsets.all(0.0),
                                      onPressed: () => {},
                                      icon: Icon(
                                          GroovinMaterialIcons.star_outline),
                                    ),
                                    IconButton(
                                      padding: const EdgeInsets.all(0.0),
                                      onPressed: () => {},
                                      iconSize: 20.0,
                                      icon: Icon(
                                          GroovinMaterialIcons.dots_vertical),
                                    )
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                      ]),
                ),
              ),
              Container(
                padding: const EdgeInsets.fromLTRB(16.0, 20.0, 16.0, 8.0),
                child: Text(
                  '为您推荐',
                  style: Theme.of(context)
                      .textTheme
                      .subhead
                      .copyWith(fontWeight: FontWeight.w800),
                ),
              ),
              Theme(
                data: Theme.of(context)
                    .copyWith(primaryColor: Color.fromRGBO(35, 47, 52, 1.0)),
                child: Card(
                  color: Colors.white,
                  elevation: 0.0,
                  shape: RoundedRectangleBorder(),
                  child: Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: <Widget>[
                        FlatButton(
                          padding: EdgeInsets.all(16.0),
                          onPressed: () => {},
                          child: Stack(
                            fit: StackFit.loose,
                            overflow: Overflow.visible,
                            children: <Widget>[
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.stretch,
                                children: <Widget>[
                                  Text(
                                    'Ali Connor - 25 min ago',
                                    style: Theme.of(context)
                                        .textTheme
                                        .body1
                                        .copyWith(fontWeight: FontWeight.w300),
                                  ),
                                  Container(height: 4.0),
                                  Text(
                                    '罗马国三旗帜',
                                    style: Theme.of(context)
                                        .textTheme
                                        .title
                                        .copyWith(fontWeight: FontWeight.w800),
                                  ),
                                  Container(height: 8.0),
                                  Text("正文跟我各位那啊我呃啊。"),
                                ],
                              ),
                              Positioned(
                                right: -8.0,
                                top: -16.0,
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    IconButton(
                                      iconSize: 20.0,
                                      padding: const EdgeInsets.all(0.0),
                                      onPressed: () => {},
                                      icon: Icon(
                                          GroovinMaterialIcons.star_outline),
                                    ),
                                    IconButton(
                                      padding: const EdgeInsets.all(0.0),
                                      onPressed: () => {},
                                      iconSize: 20.0,
                                      icon: Icon(
                                          GroovinMaterialIcons.dots_vertical),
                                    )
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                        Divider(),
                        FlatButton(
                          padding: EdgeInsets.all(16.0),
                          onPressed: () => {},
                          child: Stack(
                            fit: StackFit.loose,
                            children: <Widget>[
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.stretch,
                                children: <Widget>[
                                  Text(
                                    'Ali Connor - 25 min ago',
                                    style: Theme.of(context)
                                        .textTheme
                                        .body1
                                        .copyWith(fontWeight: FontWeight.w300),
                                  ),
                                  Container(height: 4.0),
                                  Text(
                                    '你好',
                                    style: Theme.of(context)
                                        .textTheme
                                        .title
                                        .copyWith(fontWeight: FontWeight.w800),
                                  ),
                                  Container(height: 8.0),
                                  Text("我会帮你干好活的！不必担心。"),
                                ],
                              ),
                              Positioned(
                                right: -8.0,
                                top: -16.0,
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    IconButton(
                                      iconSize: 20.0,
                                      padding: const EdgeInsets.all(0.0),
                                      onPressed: () => {},
                                      icon: Icon(
                                          GroovinMaterialIcons.star_outline),
                                    ),
                                    IconButton(
                                      padding: const EdgeInsets.all(0.0),
                                      onPressed: () => {},
                                      iconSize: 20.0,
                                      icon: Icon(
                                          GroovinMaterialIcons.dots_vertical),
                                    )
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                      ]),
                ),
              ),
              new MyCard(
                cardData: dataQA,
                cardTitle: '问答讯息',
                isFirstCard: true,
                numberOfLines: 3,
              ),
              new MyCard(
                cardData: dataTrade,
                cardTitle: '交易讯息',
                isFirstCard: false,
                numberOfLines: 3,
              ),
              new MyCard(
                cardData: [],
                cardTitle: '为您推荐',
                isFirstCard: false,
                numberOfLines: 4,
              ),
            ],
          );

    return new Scaffold(
      appBar: new AppBar(
        // textTheme: Theme.of(context).textTheme.copyWith(
        //       title: Theme.of(context)
        //           .textTheme
        //           .title
        //           .copyWith(color: Theme.of(context).primaryColor),
        //     ),
        // iconTheme: Theme.of(context)
        //     .iconTheme
        //     .copyWith(color: Theme.of(context).primaryColor),
        backgroundColor: Colors.white,
        title: Image.asset(
          Repository.mostLogoUri,
          height: 24.0,
        ),
        // title: new Text(
        //   'MOST',
        // ),
        actions: <Widget>[new SearchButton()],
      ),
      // appBar: buildAppBar(context),
      body: _body,
      floatingActionButton: AddPostFAB(),
      floatingActionButtonLocation:
          fabMenuLocation, // Remember to set floatingActionButtonLocation to fabMenuLocation')
    );
  }

  Widget setFollowerWidget() {
    if (followingUsers == []) {
      return Text('您还没有关注任何人喔');
    } else {
      return new ListView(
          scrollDirection: Axis.horizontal,
          children: followingUsers.map((dynamic item) {
            return new Container(
                margin: EdgeInsets.fromLTRB(14.0, 0.0, 0.0, 0.0),
                child: ImageTextCard(
                  imageStirng: item['user_image']['url'],
                  isNetImage: true,
                  title: item['username'],
                ));
          }).toList());
    }
  }

  String listToJsonString(List list) {
    String result = '[';
    for (var i = 0; i < list.length; i++) {
      result += '"' + list[i] + '"';
      if (i != list.length - 1) {
        result += ',';
      }
    }
    result += ']';
    return result;
  }
}

class CardTile {}
