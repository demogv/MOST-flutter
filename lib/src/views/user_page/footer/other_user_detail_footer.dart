import 'package:flutter/material.dart';
import 'answer_showcase.dart';
import 'question_showcase.dart';
import 'trade_showcase.dart';

class UserShowcase extends StatefulWidget {
  UserShowcase({Key key, this.footerData, this.selfOrOther});

  final List footerData;
  final bool selfOrOther;

  @override
  __UserShowcaseState createState() => new __UserShowcaseState(footerData: footerData, selfOrOther: selfOrOther);
}

class __UserShowcaseState extends State<UserShowcase>
    with TickerProviderStateMixin {
  List footerData;
  bool selfOrOther;
  List<Tab> _tabs;
  List<Widget> _pages;
  TabController _controller;

  __UserShowcaseState({Key key, this.footerData, this.selfOrOther});

  @override
  void initState() {
    super.initState();
    _tabs = [
      new Tab(text: '近期问题'),
      new Tab(text: '近期回答'),
      new Tab(text: '二手货'),
    ];
    _pages = [
      new QuestionShowcase(questionData: setList(footerData[0]),),
      new AnswerShowcase(answerData: setList(footerData[1]),),
      new TradeShowcase(tradeData: setList(footerData[2]),),
    ];
    _controller = new TabController(
      length: _tabs.length,
      vsync: this,
    );
  }

  @override
  Widget build(BuildContext context) {
    return new Padding(
      padding: const EdgeInsets.all(16.0),
      child: new Column(
        children: <Widget>[
          new TabBar(
            controller: _controller,
            tabs: _tabs,
            indicatorColor: Colors.white,
          ),
          new SizedBox.fromSize(
            size: const Size.fromHeight(300.0),
            child: new TabBarView(
              controller: _controller,
              children: _pages,
            ),
          ),
        ],
      ),
    );
  }

  List setList(List originList) {
    List newList = [];
    if (originList.length > 0) {
      for (var i = 0; i < originList.length; i++) {
        newList.add(originList[i]);
        newList.add('line');
      }
    }
    if (newList.length == 0) {
      newList.add('noData');
    } else if (originList.length == 10) {
      newList.add('end');
    } else {
      newList.add('noMore');
    }
    return newList;
  }

}
