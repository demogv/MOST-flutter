import 'package:flutter/material.dart';
import 'package:date_format/date_format.dart';
import 'package:most/src/views/question_details.dart';

class QuestionShowcase extends StatefulWidget {
  final List questionData;
  QuestionShowcase({Key key, this.questionData}) : super(key: key);
  @override
  State<StatefulWidget> createState() {
    return new QuestionShowcaseState(questionData: questionData);
  }
}

class QuestionShowcaseState extends State<QuestionShowcase> {
  List questionData;

  bool followedOrNot;
  QuestionShowcaseState({Key key, this.questionData});

  //设置字体
  textStyle({int size, Color color, FontWeight fontWeight}) {
    return new TextStyle(
      fontSize: size.toDouble(),
      decoration: TextDecoration.none,
      color: color,
      fontWeight: fontWeight,
    );
  }

  @override
  Widget build(BuildContext context) {
    Widget _singleItemDisplay(dynamic item) {
      if (item == 'noData') {
        return new Container(
          height: 66.0,
          color: Colors.transparent,
          padding: EdgeInsets.fromLTRB(0.0, 20.0, 0.0, 10.0),
          child: Text(
            '什么都没有╮(╯▽╰)╭',
            style: textStyle(size: 14, color: Colors.white),
          ),
        );
      } else if (item == 'end') {
        return new Container(
          height: 66.0,
          color: Colors.transparent,
          padding: EdgeInsets.fromLTRB(0.0, 20.0, 0.0, 10.0),
          child: Text(
            '只显示这么多喔(oﾟωﾟo) ',
            style: textStyle(size: 14, color: Colors.white),
          ),
        );
      } else if (item == 'noMore') {
        return new Container(
          height: 66.0,
          color: Colors.transparent,
          padding: EdgeInsets.fromLTRB(0.0, 20.0, 0.0, 10.0),
          child: Text(
            '没有更多了喔(￣▽￣)',
            style: textStyle(size: 14, color: Colors.white),
          ),
        );
      } else if (item == 'line') {
        return new Divider(
          height: 0.5,
          color: Colors.white,
        );
      }
      return new Container(
        height: 66.0,
        color: Colors.transparent,
        padding: EdgeInsets.fromLTRB(0.0, 10.0, 0.0, 10.0),
        child: new GestureDetector(
          onTap: () {
            if (item['tag'] == 1) {
              Navigator.of(context).push(new MaterialPageRoute(builder: (ctx) {
                return new QuestionDetailPage(
                    questionId: item['objectId'],
                    postTime: item['createdAt']['iso'],
                    questionBody: item['question_detail'],
                    questionTitle: item['question_title'],
                    userAvatarUrl: item['user_image']['url'],
                    userName: item['username']);
              }));
            } else if (item['tag'] == 2 || item['tag'] == 3) {
              Navigator.of(context).push(new MaterialPageRoute(builder: (ctx) {
                return new QuestionDetailPage(
                    questionId: item['question_id'],
                    postTime: item['question_createdAt']['iso'],
                    questionBody: item['question_detail'],
                    questionTitle: item['question_title'],
                    userAvatarUrl: item['question_user_image']['url'],
                    userName: item['question_username']);
              }));
            }
          },
          child: new Column(
            children: <Widget>[
              new Row(
                children: <Widget>[
                  new Container(
                    width: 20.0,
                    height: 20.0,
                    decoration: new BoxDecoration(
                      image: new DecorationImage(
                          image: new NetworkImage(
                              item['user_image']['url'].toString())),
                      borderRadius: new BorderRadius.all(
                        const Radius.circular(10.0),
                      ),
                    ),
                  ),
                  new Container(
                    height: 20.0,
                    margin: const EdgeInsets.fromLTRB(6.0, 0.0, 0.0, 0.0),
                    child: new Text(item['username'].toString(),
                        style: textStyle(
                            size: 12,
                            color: Colors.white,
                            fontWeight: FontWeight.w500)),
                  ),
                  new Container(
                    height: 20.0,
                    margin: const EdgeInsets.fromLTRB(6.0, 0.0, 0.0, 0.0),
                    child: new Text(getStates(item),
                        style: textStyle(
                            size: 12,
                            color: Colors.white,
                            fontWeight: FontWeight.w400)),
                  ),
                  new Container(
                      height: 20.0,
                      margin: const EdgeInsets.fromLTRB(6.0, 0.0, 0.0, 0.0),
                      child: new Text(
                          getDate(DateTime.parse(
                              item['createdAt']['iso'].toString())),
                          style: textStyle(size: 12, color: Colors.white)))
                ],
              ),
              new Container(
                  margin: const EdgeInsets.fromLTRB(0.0, 5.0, 0.0, 0.0),
                  padding: EdgeInsets.all(0.0),
                  child: new Row(
                    children: <Widget>[
                      new Expanded(
                          child: new Text(
                        getDetailString(item),
                        style: textStyle(
                            size: 14,
                            color: Colors.white,
                            fontWeight: FontWeight.w500),
                      ))
                    ],
                  ))
            ],
          ),
        ),
      );
    }

    Widget itemsWidget = new Column(
        children: questionData.map((dynamic item) {
      return _singleItemDisplay(item);
    }).toList());

    return new Container(
      margin: EdgeInsets.fromLTRB(20.0, 20.0, 20.0, 20.0),
      child: itemsWidget,
    );
  }

  String getStates(Map<String, dynamic> item) {
    if (item['tag'] == 1) {
      return '发布了问题';
    } else if (item['tag'] == 2) {
      if (item['question_title'].toString().length > 6) {
        return '回答了"' +
            item['question_title'].toString().substring(0, 6) +
            '...' +
            '"';
      } else {
        return '回答了"' + item['question_title'].toString() + '"';
      }
    } else if (item['tag'] == 3) {
      if (item['answer_detail'].toString().length > 6) {
        return '评论了"' +
            item['answer_detail'].toString().substring(0, 6) +
            '...' +
            '"';
      } else {
        return '评论了"' + item['answer_detail'].toString() + '"';
      }
    } else if (item['tag'] == 4) {
      return '发布了二手书';
    } else if (item['tag'] == 5) {
      return '发布了闲置品';
    }
    return '';
  }

  String getDetailString(Map<String, dynamic> item) {
    if (item['tag'] == 1) {
      return item['question_title'];
    } else if (item['tag'] == 2) {
      return item['answer_detail'];
    } else if (item['tag'] == 3) {
      return item['comment_detail'];
    } else if (item['tag'] == 4 || item['tag'] == 5) {
      return item['trade_title'];
    }
    return '';
  }

  String getDate(DateTime date) {
    return formatDate(date, [mm, '月', dd, '日', hh, ':', nn]).toString();
  }
}
