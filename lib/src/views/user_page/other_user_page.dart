import 'package:flutter/material.dart';
import 'package:most/src/models/user_info.dart';
import 'package:most/src/views/user_page/footer/user_detail_footer.dart';
import 'footer/other_user_detail_footer.dart';
import 'user_page_body.dart';
import 'header/user_detail_header.dart';
import '../../../src/api/leancloud.dart';
import '../../common/repository.dart';
import 'package:meta/meta.dart';

class UserDetailsPage extends StatefulWidget {
  UserDetailsPage({
    Key key,
    this.userId,
  }) : super(key: key);

  final String userId;
  @override
  _UserDetailsPageState createState() =>
      new _UserDetailsPageState(userId: userId);
}

class _UserDetailsPageState extends State<UserDetailsPage> {
  UserInfo user;
  String userId;
  String myId;
  List<UserInfo> followers;
  List<UserInfo> followees;
  List footerData;

  _UserDetailsPageState({Key key, this.userId});

  setData() async {
    String myIdGet = await Repository.get().thisUserObjectId;
    final _user = await Repository.get().userInfoOf(userId);
    // Map _user = await LeanCloudFunction.callFunction(
    //   functionName: 'get_other_user',
    //   myData: {'other_user_id': userId},
    // );
    // List footerGet;
    List footerGet = await LeanCloudFunction.callFunction(
      functionName: 'other_user_info',
      myData: {'other_user_id': userId},
    );
    final _followers = await LeanCloudUser.followersOf(userId);
    final _followees = await LeanCloudUser.followeesOf(userId);
    // List followers = await LeanCloudFunction.query(
    //   body: {'include': 'follower'},
    //   className: '/users/' + userId + '/followers',
    // );
    // List followees = await LeanCloudFunction.query(
    //   body: {'include': 'followee'},
    //   className: '/users/' + userId + '/followees',
    // );
    if (this.mounted) {
      // ensure the object is still in the tree.
      this.setState(() {
        myId = myIdGet;
        user = _user;
        footerData = footerGet;
        followers = _followers;
        followees = _followees;
      });
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    setData();
  }

  @override
  Widget build(BuildContext context) {
    var linearGradient = const BoxDecoration(
      gradient: const LinearGradient(
        begin: FractionalOffset.centerRight,
        end: FractionalOffset.bottomLeft,
        colors: <Color>[
          const Color(0xFF413070),
          const Color(0xFF2B264A),
        ],
      ),
    );

    var _body = (user == null)
        ? new Center(
            child: new CircularProgressIndicator(
              backgroundColor: Color(0XFFFF2D55),
            ),
          )
        : new Container(
            decoration: new BoxDecoration(color: Color(0xBB8338f4)),
            child: SingleChildScrollView(
              child: new Container(
                decoration: linearGradient,
                child: new Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    new UserDetailHeader(
                      myId: myId,
                      followees: followees,
                      followers: followers,
                      user: user,
                      userId: userId,
                    ),
                    new Padding(
                      padding: const EdgeInsets.all(24.0),
                      child: new UserDetailBody(user),
                    ),
                    new FriendShowcase(user),
                    new UserShowcase(
                      footerData: footerData,
                      selfOrOther: (userId == myId),
                    ),
                  ],
                ),
              ),
            ),
          );

    return new Scaffold(
      body: _body,
    );
  }
}
