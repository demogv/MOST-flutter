import 'package:flutter/material.dart';
import 'package:most/src/models/user_info.dart';
import 'diagonally_cut_colored_image.dart';
import 'package:meta/meta.dart';
import '../../../../src/api/leancloud.dart';

class UserDetailHeader extends StatefulWidget {
  final String myId;
  final String userId;
  final UserInfo user;
  final List<UserInfo> followers;
  final List<UserInfo> followees;
  UserDetailHeader(
      {Key key,
      this.user,
      this.followees,
      this.followers,
      this.myId,
      this.userId})
      : super(key: key);
  @override
  State<StatefulWidget> createState() {
    return new UserDetailHeaderState(
        user: user,
        followees: followees,
        followers: followers,
        myId: myId,
        userId: userId);
  }
}

class UserDetailHeaderState extends State<UserDetailHeader> {
  UserInfo user;
  List<UserInfo> followers;
  List<UserInfo> followees;
  String myId;
  String userId;
  //这个图片目前是不存在的

  static const BACKGROUND_IMAGE = 'images/profile_header_background.png';
  bool followedOrNot;
  UserDetailHeaderState(
      {Key key,
      this.user,
      this.followers,
      this.followees,
      this.myId,
      this.userId});

  @override
  void initState() {
    super.initState();
    checkFollower();
  }

  checkFollower() {
    bool check = false;
    if (userId != myId && followers.length > 0) {
      for (var i in followers) {
        if (i.objectId == myId) {
          check = true;
          break;
        }
      }
    }
    setState(() {
      followedOrNot = check;
    });
  }

  Widget _buildDiagonalImageBackground(BuildContext context) {
    var screenWidth = MediaQuery.of(context).size.width;
    return new DiagonallyCutColoredImage(
      new Image.asset(
        BACKGROUND_IMAGE,
        width: screenWidth,
        height: 280.0,
        fit: BoxFit.cover,
      ),
      color: const Color(0xBB8338f4),
    );
  }

  Widget _buildAvatar() {
    return new Hero(
      tag: user.avatar.uri.toString(),
      child: new CircleAvatar(
        backgroundImage: new NetworkImage(user.avatar.uri.toString()),
        radius: 50.0,
      ),
    );
  }

  Widget _buildFollowerInfo(TextTheme textTheme) {
    var followerStyle =
        textTheme.subhead.copyWith(color: const Color(0xBBFFFFFF));

    return new Padding(
      padding: const EdgeInsets.only(top: 16.0),
      child: new Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          new Text(followees.length.toString() + ' 关注', style: followerStyle),
          new Text(
            ' | ',
            style: followerStyle.copyWith(
                fontSize: 24.0, fontWeight: FontWeight.normal),
          ),
          new Text(' ' + followers.length.toString() + ' 粉丝',
              style: followerStyle),
        ],
      ),
    );
  }

  Widget _buildActionButtons(ThemeData theme) {
    return new Padding(
      padding: const EdgeInsets.only(
        top: 16.0,
        left: 16.0,
        right: 16.0,
      ),
      child: followButton(theme),
    );
  }

  Widget followButton(ThemeData theme) {
    print(userId);
    print(myId);
    if (userId != myId) {
      return new DecoratedBox(
        decoration: new BoxDecoration(
          color: followedOrNot ? Color(0XFFFF2D55) : theme.accentColor,
          border: new Border.all(color: Colors.white30),
          borderRadius: new BorderRadius.circular(30.0),
        ),
        child: _createPillButton(
          followedOrNot ? '取消关注' : '关注ta',
          textColor: Colors.white70,
          backgroundColor: Colors.transparent,
        ),
      );
    } else {
      return Container();
    }
  }

  Widget _createPillButton(
    String text, {
    Color backgroundColor = Colors.transparent,
    Color textColor = Colors.white70,
  }) {
    return new ClipRRect(
      borderRadius: new BorderRadius.circular(30.0),
      child: new FlatButton(
        padding: EdgeInsets.fromLTRB(24.0, 12.0, 24.0, 12.0),
        color: backgroundColor,
        textColor: textColor,
        onPressed: () async {
          var result;
          if (!followedOrNot) {
            // result = await cloudFunction.postData(
            //   urlAddition: '/users/'+myId+'/friendship/'+userId,
            //   body: {},
            // );
          } else {
            // result = await cloudFunction.deleteData(
            //   urlAddition: '/users/'+myId+'/friendship/'+userId,
            //   body: {},
            // );
          }

          final _followers = await LeanCloudUser.followersOf(userId);
          final _followees = await LeanCloudUser.followeesOf(userId);
          // List followersGet = await LeanCloudFunction.query(
          //   body: {'include': 'follower'},
          //   className: '/users/' + userId + '/followers',
          // );
          // List followeesGet = await LeanCloudFunction.query(
          //   body: {'include': 'followee'},
          //   className: '/users/' + userId + '/followees',
          // );
          bool newRelationship = !followedOrNot;
          setState(() {
            followees = _followees;
            followers = _followers;
            followedOrNot = newRelationship;
          });
        },
        child: new Text(text),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    var theme = Theme.of(context);
    var textTheme = theme.textTheme;

    return new Stack(
      children: <Widget>[
        _buildDiagonalImageBackground(context),
        new Align(
          alignment: FractionalOffset.bottomCenter,
          heightFactor: 1.4,
          child: new Column(
            children: <Widget>[
              _buildAvatar(),
              _buildFollowerInfo(textTheme),
              _buildActionButtons(theme),
            ],
          ),
        ),
        new Positioned(
          top: 26.0,
          left: 4.0,
          child: new BackButton(color: Colors.white),
        ),
      ],
    );
  }
}
