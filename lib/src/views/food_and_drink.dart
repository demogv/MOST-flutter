import 'package:flutter/material.dart';
import 'package:most/src/api/leancloud.dart';
import 'package:most/src/common/repository.dart';
import 'dart:async';
import 'package:date_format/date_format.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:flutter_platform_widgets/flutter_platform_widgets.dart';
import 'package:most/src/utils/utils.dart';
import 'package:most/src/widgets/answer_end_line.dart';
//import 'package:flutter_search_bar/flutter_search_bar.dart';

class FoodDrinkDetailPage extends StatefulWidget {
  final Map<String, dynamic> shopData;
  FoodDrinkDetailPage({Key key, this.shopData}) : super(key: key);
  @override
  State<StatefulWidget> createState() {
    return new FoodDrinkDetailPageState(shopData: shopData);
  }
}

class FoodDrinkDetailPageState extends State<FoodDrinkDetailPage> {
  //用来测试用的数据
  Map<String, dynamic> shopData;
  List commentData = [1];
  List shopItems = [
    [
      {
        'name': '珍珠奶茶1',
        'detail': '贼好喝',
        'price': '15',
        'imgUrl': 'http://a136.idata.over-blog.com/1/93/20/57/1190605551.jpg',
        'saleNum': '11'
      },
      {
        'name': '珍珠奶茶2',
        'detail': '贼好喝',
        'price': '11',
        'imgUrl': 'http://a136.idata.over-blog.com/1/93/20/57/1190605551.jpg',
        'saleNum': '22'
      },
      {
        'name': '珍珠奶茶3',
        'detail': '贼好喝',
        'price': '14',
        'imgUrl': 'http://a136.idata.over-blog.com/1/93/20/57/1190605551.jpg',
        'saleNum': '14'
      },
      {
        'name': '珍珠奶茶4',
        'detail': '贼好喝',
        'price': '13',
        'imgUrl': 'http://a136.idata.over-blog.com/1/93/20/57/1190605551.jpg',
        'saleNum': '2'
      },
      {
        'name': '珍珠奶茶5',
        'detail': '贼好喝',
        'price': '16',
        'imgUrl': 'http://a136.idata.over-blog.com/1/93/20/57/1190605551.jpg',
        'saleNum': '16'
      },
    ],
    [
      {
        'name': '奶盖茶1',
        'detail': '超级好喝',
        'price': '18',
        'imgUrl':
            'https://img.tulaoshi.com/ueditor/201611/14/f87a93871d7bcd6c812137effc175b55.jpg',
        'saleNum': '100'
      },
      {
        'name': '奶盖茶2',
        'detail': '超级好喝',
        'price': '19',
        'imgUrl':
            'https://img.tulaoshi.com/ueditor/201611/14/f87a93871d7bcd6c812137effc175b55.jpg',
        'saleNum': '20'
      },
      {
        'name': '奶盖茶3',
        'detail': '超级好喝',
        'price': '14',
        'imgUrl':
            'https://img.tulaoshi.com/ueditor/201611/14/f87a93871d7bcd6c812137effc175b55.jpg',
        'saleNum': '4'
      },
    ],
    [
      {
        'name': '纯茶1',
        'detail': '交关好喝',
        'price': '20',
        'imgUrl':
            'http://www.teauo.com/d/file/tea/Oolongtea/Efficacy/20130923/9162b075a374b48bd8e01d23248e56fa.jpg',
        'saleNum': '10'
      },
      {
        'name': '纯茶2',
        'detail': '交关好喝',
        'price': '22',
        'imgUrl':
            'http://www.teauo.com/d/file/tea/Oolongtea/Efficacy/20130923/9162b075a374b48bd8e01d23248e56fa.jpg',
        'saleNum': '200'
      },
      {
        'name': '纯茶3',
        'detail': '交关好喝',
        'price': '11',
        'imgUrl':
            'http://www.teauo.com/d/file/tea/Oolongtea/Efficacy/20130923/9162b075a374b48bd8e01d23248e56fa.jpg',
        'saleNum': '40'
      },
    ],
    [
      {
        'name': '珍珠奶盖茶',
        'detail': '老好喝',
        'price': '25',
        'imgUrl':
            'https://cdn.haochu.com/uploads/caipu/20171201/15121083822107.jpg',
        'saleNum': '106'
      },
      {
        'name': '奶盖珍珠茶',
        'detail': '老好喝',
        'price': '26',
        'imgUrl':
            'https://cdn.haochu.com/uploads/caipu/20171201/15121083822107.jpg',
        'saleNum': '25'
      },
    ],
  ];
  List<String> groupName = ['超赞奶茶', '香甜奶盖', '品茗', '双重美食'];
  List dataList = [];
  //RegExp regExp1 = new RegExp("</.*>");
  //RegExp regExp2 = new RegExp("<.*>");
  TextStyle subtitleStyle =
      new TextStyle(fontSize: 12.0, color: const Color(0xFFB5BDC0));
  TextStyle contentStyle = new TextStyle(fontSize: 15.0, color: Colors.black);
  num curPage = 1;
  ScrollController _controller = new ScrollController();
  TextEditingController _inputController = new TextEditingController();

  FoodDrinkDetailPageState({Key key, this.shopData});

  //设置字体
  textStyle({int size, Color color, FontWeight fontWeight}) {
    return new TextStyle(
      fontSize: size.toDouble(),
      decoration: TextDecoration.none,
      color: color,
      fontWeight: fontWeight,
    );
  }

  //获取评论
  getReply() async {
    //测试一下跑云函数
    final List newCommentData = await LeanCloudFunction.callFunction(
        myData: {'trade_id': '5b42b959f84da500341a7a2e', 'tag': '4'},
        functionName: 'get_trade_comment',
        sessionToken: await Repository.get().sessionToken);
    if (this.mounted) {
      setState(() {
        newCommentData.add('COMPLETE');
        commentData = newCommentData;
      });
    }
  }

  @override
  void initState() {
    super.initState();
    for (var i = 0; i < shopItems.length; i++) {
      dataList.add(groupName[i]);
      dataList.addAll(shopItems[i]);
    }
    //getReply();
  }

  @override
  Widget build(BuildContext context) {
    Widget _groupNameDisplay(String item) {
      return new Container(
        height: 30.0,
        child: new Text(
          item,
          style: textStyle(
              size: 14, color: Colors.black, fontWeight: FontWeight.w500),
        ),
      );
    }

    Widget _singleItemDisplay(dynamic item) {
      if (item is String) {
        return new Container(
          height: 20.0,
          child: Text(item),
        );
      } else {
        return new Container(
          height: 80.0,
          padding: EdgeInsets.fromLTRB(0.0, 6.0, 0.0, 6.0),
          child: new Row(
            children: <Widget>[
              new Container(
                padding: EdgeInsets.all(0.0),
                child: Image.network(
                  item['imgUrl'],
                  fit: BoxFit.fill,
                ),
              ),
              new Column(
                children: <Widget>[
                  new Text(
                    item['name'],
                    style: textStyle(
                        size: 18,
                        color: Colors.black,
                        fontWeight: FontWeight.w500),
                  ),
                  new Flexible(
                    child: Text(
                      item['detail'],
                      style: textStyle(
                          size: 14,
                          color: Colors.black,
                          fontWeight: FontWeight.normal),
                    ),
                  ),
                  new Text(
                    '¥' + item['price'],
                    style: textStyle(
                        size: 18,
                        color: Colors.red,
                        fontWeight: FontWeight.w500),
                  ),
                ],
              )
            ],
          ),
        );
      }
    }

    Widget groupWidget = new ListView(
        scrollDirection: Axis.vertical,
        shrinkWrap: true,
        children: groupName.map((String item) {
          return _groupNameDisplay(item);
        }).toList());

    Widget itemsWidget = new ListView(
        scrollDirection: Axis.vertical,
        shrinkWrap: true,
        children: dataList.map((dynamic item) {
          return _singleItemDisplay(item);
        }).toList());

    //问题标题的Widget
    /*var questionTitleView = new Container(
      color: const Color(0xfffcfcfc),
      child: new Row(
        children: <Widget>[
          //红点
          new Container(
            width: 8.0,
            height: 20.0,
            margin: new EdgeInsets.fromLTRB(0.0, 5.0, 10.0, 5.0),
            decoration: new BoxDecoration(
              color: Color(0XFFFF2D55),
              borderRadius:  new BorderRadius.all(
                const Radius.circular(3.5)
              )
            ),
          ),
          //问题标题
          new Flexible(
            flex: 1,
            fit: FlexFit.tight,
            child: new Text(tradeTitle,style: this.textStyle(size: 18, color: Colors.black, fontWeight: FontWeight.w600),),
          ),
          //回答按钮
          new Container(
            width: 54.0,
            height: 30.0,
            margin: new EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 0.0),
            child: new FlatButton(
              padding: new EdgeInsets.all(0.0),
              child: new Text("购买",style: this.textStyle(size: 14, color: Colors.white, fontWeight: FontWeight.w500),),
              onPressed: null,
              shape: new RoundedRectangleBorder(borderRadius: new BorderRadius.circular(9.0))
            ),
            decoration: new BoxDecoration(
              color: Color(0XFFFF2D55),
              borderRadius:  new BorderRadius.all(
                const Radius.circular(9.0)
              )
            ),
          ),
        ],
      ),
    );

    //用户有关的Widget
    var userInfoView = new Container(
    color: const Color(0xfffcfcfc),
    child: new Row(
        children: <Widget>[
          //放用户图片
          new Container(
            width: 40.0,
            height: 40.0,
            decoration: new BoxDecoration(
              image: new DecorationImage(
                image: new NetworkImage(userImageUrl)
              ),
              borderRadius: new BorderRadius.all(
                const Radius.circular(20.0),
              ),
            ),
          ),
          //问题标题
          new Flexible(
            flex: 1,
            fit: FlexFit.tight,
            child: new Container(
              margin: new EdgeInsets.fromLTRB(10.0, 0.0, 0.0, 0.0),
              child: new Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  new Text(userName + ' ' + '发布于', style: this.textStyle(size: 14, color: Color(0xFF818181), fontWeight: FontWeight.w600),),
                  new Text(postTime, style: this.textStyle(size: 14, color: Color(0xFF818181), fontWeight: FontWeight.w600),),
                ],
              ),
            )
          ),
          priceWidget(),
        ],
      )
    );*/

    var _body = commentData == null
        ? new Center(
            child: new PlatformCircularProgressIndicator(
              android: (_) => MaterialProgressIndicatorData(
                    backgroundColor: Color(0XFFFF2D55),
                  ),
              ios: (_) => CupertinoProgressIndicatorData(),
            ),
          )
        : new Column(
            children: <Widget>[
              new Container(
                child: new Row(
                  children: <Widget>[
                    new Container(
                      width: 80.0,
                      child: groupWidget,
                    ),
                    new Flexible(
                      child: itemsWidget,
                    ),
                  ],
                ),
              )
              /* new Container(
          height: 30.0,
          margin: new EdgeInsets.fromLTRB(20.0, 20.0, 20.0, 0.0),
          child: questionTitleView,
        ),
        new Container(
          height: 40.0,
          margin: new EdgeInsets.fromLTRB(20.0, 20.0, 20.0, 0.0),
          child:  userInfoView,
        ),
        new Padding(
              padding: const EdgeInsets.fromLTRB(20.0, 20.0, 20.0, 0.0),
              child: new Row(
                children: <Widget>[
                  new Expanded(
                      child: new Text(questionDetail, style: textStyle(size: 14, color: Color(0XFF2F2F2F), fontWeight: FontWeight.w500), textAlign: TextAlign.start,),
                  )
                ],
              )
        ),
        new Container(
          height: 200.0,
          padding: EdgeInsets.all(20.0),
          child: new Swiper(
            itemBuilder: (BuildContext context, int index) {
              return new Image.network(
                images[index],
                fit: BoxFit.fitWidth
              );
            },
            pagination: new SwiperPagination(
              margin: new EdgeInsets.all(5.0)
            ),
            loop: true,
            index: 0,
            autoplay: true,
            autoplayDisableOnInteraction: true,
            itemCount: images.length,
            viewportFraction: 0.8,
            scale: 0.9,
          ),
        ),
        new Flexible(
          flex: 1,
          fit: FlexFit.tight,
          child: new Container(
            margin: new EdgeInsets.fromLTRB(20.0, 20.0, 20.0, 0.0),
            child: new ListView.builder(
              itemCount: commentData.length*2,
              itemBuilder: renderListItem,
              controller: _controller,
            ),
          ),
        ),
        new Container(
          margin: EdgeInsets.fromLTRB(16.0, 10.0, 16.0, 0.0),
          height: 44.0,
          padding: EdgeInsets.fromLTRB(0.0, 7.0, 0.0, 7.0),
          color: Color(0XFFF7F7F7),
          child: new TextField(
            maxLines: 1,
            cursorColor: Colors.white,
            style: textStyle(size: 16, color: Colors.black, fontWeight: FontWeight.normal),
            controller: _inputController,
            decoration: new InputDecoration(
              hintText: "评论点啥呗～",
              contentPadding: const EdgeInsets.fromLTRB(5.0, 5.0, 5.0, 5.0),
              border: new OutlineInputBorder(
                borderRadius: const BorderRadius.all(const Radius.circular(10.0)),
                borderSide: BorderSide(color: Colors.white)
              )
            ),
          ),
        ),*/
            ],
          );

    return new PlatformScaffold(
        appBar: new PlatformAppBar(
          backgroundColor: Colors.white,
          title: new Text(
            "商品详情",
          ),
          // android: (BuildContext context) => MaterialAppBarData(
          //       iconTheme: new IconThemeData(color: Colors.white),
          //     ),
          // ios: (_) => CupertinoNavigationBarData(),
        ),
        body: _body);
  }

  Widget renderListItem(BuildContext context, int i) {
    if (i.isEven && i != 0) {
      return new Divider(
        height: 1.0,
      );
    } else if (i.isEven && i == 0) {
      return new Divider(
        height: 1.0,
        color: Colors.white,
      );
    }
    i -= 1;
    i ~/= 2;
    return _renderAnswerRow(context, i);
  }

  //用的是测试的变量，日后需要稍作调整
  /* Widget priceWidget() {
    if (originalPrice.toString() == '') {
      return new Container(
        height: 40.0,
        padding: EdgeInsets.fromLTRB(0.0, 5.0, 0.0, 5.0),
        child: new Text(price.toString()+"¥", style: textStyle(size: 30, color: Color(0XFF263238), fontWeight: FontWeight.w600),)
      ); 
    } else {
      return new Container(
        height: 40.0,
        padding: EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 0.0),
        child: new Column(
          children: <Widget>[
            new Container(
              height: 20.0,
              margin: EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 10.0),
              child: new Text(price.toString()+"¥", style: textStyle(size: 20, color: Color(0XFF263238), fontWeight: FontWeight.w600),),
            ),
            new Container(
              height: 10.0,
              margin: EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 0.0),
              child: new Text("原价: "+originalPrice.toString()+"¥", style: textStyle(size: 10, color: Color(0xFF959595), fontWeight: FontWeight.w500),),
            ),
          ],
        )
      );
    }
  }*/
  //渲染回答列表
  _renderAnswerRow(context, i) {
    var listItem = commentData[i];
    if (listItem is String && listItem == 'COMPLETE') {
      return new EndLine();
    }
    String userName = listItem['username'].toString();
    String userImageUrl = listItem['user_image']['url'].toString();
    DateTime dateTime = DateTime.parse(listItem['createdAt']['iso'].toString());
    String content = listItem['comment_detail'].toString();
    var row = new Container(
      margin: const EdgeInsets.fromLTRB(0.0, 20.0, 0.0, 20.0),
      child: new Column(
        children: <Widget>[
          new Row(
            children: <Widget>[
              //放用户图片
              new Container(
                width: 20.0,
                height: 20.0,
                decoration: new BoxDecoration(
                  image: new DecorationImage(
                      image: new NetworkImage(userImageUrl)),
                  borderRadius: new BorderRadius.all(
                    const Radius.circular(10.0),
                  ),
                ),
              ),
              new Container(
                height: 20.0,
                margin: const EdgeInsets.fromLTRB(6.0, 0.0, 0.0, 0.0),
                child: new Text(userName + ' 评论于',
                    style: textStyle(
                        size: 12,
                        color: Color(0xFF818181),
                        fontWeight: FontWeight.normal)),
              ),
              new Container(
                  height: 20.0,
                  margin: const EdgeInsets.fromLTRB(6.0, 0.0, 0.0, 0.0),
                  child: new Text(
                    getDate(dateTime),
                    style: subtitleStyle,
                  ))
            ],
          ),
          new Container(
              margin: const EdgeInsets.fromLTRB(0.0, 5.0, 0.0, 0.0),
              child: new Row(
                children: <Widget>[
                  new Expanded(
                      child: new Text(
                    content,
                    style: contentStyle,
                  ))
                ],
              ))
        ],
      ),
    );
    return new Builder(
      builder: (ctx) {
        return new InkWell(
          //onTap: () {
          //  getComment(answerToComment: answerToComment, ctx: ctx);
          //},
          child: row,
        );
      },
    );
  }

  /*
  void sendReply(authorId) {
    String replyStr = _inputController.text;
    if (replyStr == null || replyStr.length == 0 || replyStr.trim().length == 0) {
      return;
    } else {
      DataUtils.isLogin().then((isLogin) {
        if (isLogin) {
          DataUtils.getAccessToken().then((token) {
            Map<String, String> params = new Map();
            params['access_token'] = token;
            params['id'] = "${tweetData['id']}";
            print("id: ${tweetData['id']}");
            params['catalog'] = "3";
            params['content'] = replyStr;
            params['authorid'] = "$authorId";
            print("authorId: $authorId");
            params['isPostToMyZone'] = "0";
            params['dataType'] = "json";
            NetUtils.get(Api.COMMENT_REPLY, params: params).then((data) {
              if (data != null) {
                var obj = json.decode(data);
                var error = obj['error'];
                if (error != null && error == '200') {
                  // 回复成功
                  Navigator.of(context).pop();
                  getReply(false);
                }
              }
            });
          });
        }
      });
    }
  }*/

}
