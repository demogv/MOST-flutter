import 'package:flutter/material.dart';
import 'package:most/src/views/goods_details.dart';
import 'package:most/src/views/question_details.dart';
import 'package:most/src/widgets/card.dart';

class MessageCollection extends StatefulWidget {
  MessageCollection({Key key, this.messageType}) : super(key: key);

  final MessageType messageType;
  // const headingStyle = TextStyle();

  @override
  _MessageCollectionState createState() => new _MessageCollectionState();
}

class _MessageCollectionState extends State<MessageCollection> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return new MyCard(
      cardData: [],
      cardTitle: '问答讯息',
      isFirstCard: true,
      numberOfLines: 3,
    );
  }
}

class _Heading extends StatelessWidget {
  const _Heading(this.text);

  final String text;

  @override
  Widget build(BuildContext context) {
    final ThemeData theme = Theme.of(context);
    return new Container(
      height: 48.0,
      padding: const EdgeInsetsDirectional.only(start: 16.0),
      alignment: AlignmentDirectional.centerStart,
      child: new Text(
        text,
        style: theme.textTheme.headline.copyWith(
          color: theme.primaryColor,
        ),
      ),
    );
  }
}

enum MessageType { hotMessages, latestMessages }

Column getMessageList(BuildContext context, {messageType: MessageType}) {
  return Column(
    children: <Widget>[
      new ListTile(
        onTap: () {
          //我拿来试一下questionDetail
          Navigator.of(context).push(new MaterialPageRoute(builder: (ctx) {
            return new GoodsDetailPage(
                //tweetData: listItem,
                );
          }));
        },
        leading: const Text('🐈️'),
        title: Text('''最优化是不是很无聊啊？'''),
        subtitle: Text('小明 三分钟前 回答'),
        isThreeLine: true,
      ),
      new ListTile(
        onTap: () {
          //我拿来试一下questionDetail
          Navigator.of(context).push(new MaterialPageRoute(builder: (ctx) {
            return new QuestionDetailPage(
                //tweetData: listItem,
                );
          }));
        },
        leading: const Text('😄️'),
        title: Text('加州阳光 3分钟前 回复'),
        subtitle: Text('''信院食堂有什么好吃又便宜的美食吗？'''),
        isThreeLine: true,
      ),
    ],
  );
}

final messageHeading = {
  MessageType.hotMessages: '热门问题',
  MessageType.latestMessages: '最新问题',
};

Widget getMessageHeading({messageType: MessageType}) {
  return new ListTile(
    title: _Heading(messageHeading[messageType]),
    trailing: IconButton(
      icon: Icon(Icons.view_list),
      onPressed: () => 'hello', // TODO: Implement onPressed
    ),
  );
}
