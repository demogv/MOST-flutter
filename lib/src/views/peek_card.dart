import 'package:flutter/material.dart';

class PeekCard extends StatefulWidget {
  PeekCard({Key key, this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  _PeekCardState createState() => new _PeekCardState();
}

class _PeekCardState extends State<PeekCard> {
  @override
  Widget build(BuildContext context) {
    return new ListTile(
      leading: new Text(widget.title),
      title: peekItems(widget.title),
      trailing: new IconButton(
        // child: Text('全部'),
        icon: Icon(Icons.view_list),
        onPressed: () {}, // TODO: full screen dialog
      ),
    );
  }
}

Widget peekItems(String type) {
  return new Row(
    children: <Widget>[
      new Flexible(
        flex: 1,
        fit: FlexFit.tight,
        child: new Row(
          // scrollDirection: Axis.horizontal,

          children: <Widget>[
            new Column(
              children: <Widget>[
                Icon(Icons.wb_auto),
                Text('大王'),
              ],
            ),
            new Column(
              children: <Widget>[
                Icon(Icons.gavel),
                Text('小花'),
              ],
            ),
            new Column(
              children: <Widget>[
                Icon(Icons.ac_unit),
                Text('小花'),
              ],
            ),
            new Column(
              children: <Widget>[
                Icon(Icons.cake),
                Text('小花'),
              ],
            ),
            new Column(
              children: <Widget>[
                Icon(Icons.fiber_smart_record),
                Text('小花'),
              ],
            ),
          ],
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        ),
      ),
    ],
  );
}
