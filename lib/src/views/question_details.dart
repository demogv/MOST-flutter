import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:most/src/api/leancloud.dart';
import 'package:most/src/common/repository.dart';
import 'package:most/src/utils/leancloudImageDelegate.dart';
import 'package:most/src/utils/utils.dart';
import 'package:most/src/views/answer.dart';
import 'package:most/src/widgets/answer_end_line.dart';
import 'dart:async';
import 'package:zefyr/zefyr.dart';
import 'package:quill_delta/quill_delta.dart';
import 'package:date_format/date_format.dart';
//import 'package:flutter_search_bar/flutter_search_bar.dart';
import 'package:flutter_platform_widgets/flutter_platform_widgets.dart';
import 'package:groovin_material_icons/groovin_material_icons.dart';

class QuestionDetailPage extends StatefulWidget {
  final String questionId;
  final String questionTitle;
  final String questionBody;
  final String userName;
  final String userAvatarUrl;
  final String postTime;
  final String userId;

  QuestionDetailPage({
    Key key,
    this.questionId,
    this.questionTitle,
    this.questionBody,
    this.userName,
    this.userAvatarUrl,
    this.postTime,
    this.userId,
    // @required this.controller
  }) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return new QuestionDetailPageState(
        questionId: questionId,
        postTime: postTime,
        questionBody: questionBody,
        questionTitle: questionTitle,
        authorAvatarUrl: userAvatarUrl,
        authorUsername: userName);
  }
}

class QuestionDetailPageState extends State<QuestionDetailPage> {
  final String questionId;
  final String questionTitle;
  final String questionBody;
  String authorUsername;
  String authorAvatarUrl;
  String postTime;
  // final String authorId;

  List answerData;
  List commentData;
  //RegExp regExp1 = new RegExp("</.*>");
  //RegExp regExp2 = new RegExp("<.*>");
  TextStyle subtitleStyle =
      new TextStyle(fontSize: 12.0, color: const Color(0xFFB5BDC0));
  TextStyle contentStyle = new TextStyle(fontSize: 15.0, color: Colors.black);
  num curPage = 1;
  ScrollController _controller = new ScrollController();
  TextEditingController _inputController = new TextEditingController();

  QuestionDetailPageState(
      {Key key,
      this.questionId,
      this.questionTitle,
      this.questionBody,
      this.authorUsername,
      this.authorAvatarUrl,
      this.postTime});

  //设置字体
  textStyle({int size, Color color, FontWeight fontWeight}) {
    return new TextStyle(
      fontSize: size.toDouble(),
      decoration: TextDecoration.none,
      color: color,
      fontWeight: fontWeight,
    );
  }

  // Future initUserInfo() async {
  //   final info = LeanCloudUser.getUserInfo(userObjectId);
  // }

  //获取回答
  getReply() async {
    //测试一下跑云函数
    var newAnswerData = await LeanCloudFunction.callFunction(
        myData: {'question_id': questionId}, functionName: 'getAnswer');
    if (this.mounted) {
      setState(() {
        answerData = newAnswerData;
        answerData.add('COMPLETE');
      });
    }
  }

  //获取评论
  getComment({String answerToComment, ctx}) async {
    //测试一下跑云函数
    var newCommentData = await LeanCloudFunction.callFunction(
        myData: {'answer_to_comment': answerToComment},
        functionName: 'getComment');
    commentData = newCommentData;
    // showCommentBottomView(newCommentData, ctx); // TODO: restore this
  }

  @override
  void initState() {
    super.initState();
    getReply();
  }

  @override
  Widget build(BuildContext context) {
    //问题标题的Widget
    var questionTitleView = new Container(
      color: const Color(0xfffcfcfc),
      child: new Row(
        children: <Widget>[
          //红点
          new Container(
            width: 8.0,
            height: 20.0,
            margin: new EdgeInsets.fromLTRB(16.0, 5.0, 10.0, 5.0),
            decoration: new BoxDecoration(
                color: Color(0XFFFF2D55),
                borderRadius: new BorderRadius.all(const Radius.circular(3.5))),
          ),
          //问题标题
          new Flexible(
            flex: 1,
            fit: FlexFit.tight,
            child: new Text(
              questionTitle,
              style: this.textStyle(
                  size: 18, color: Colors.black, fontWeight: FontWeight.w600),
            ),
          ),
          //回答按钮
          new Padding(
            //width: 54.0,
            //height: 30.0,
            padding: new EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 0.0),
            child: new RaisedButton(
              color: Theme.of(context).buttonColor,
              // textColor: Colors.white,
              child: Row(
                children: <Widget>[
                  Icon(
                    GroovinMaterialIcons.plus,
                    color: Theme.of(context).accentTextTheme.button.color,
                  ),
                  Text(
                    '回答',
                    style: Theme.of(context).accentTextTheme.button,
                  ),
                ],
              ),
              onPressed: () {
                Navigator.of(context).push(MaterialPageRoute(
                    builder: (_) => AnswerPage(
                          questionerObjectId: widget.userId,
                          questionTitle: questionTitle,
                          questionObjectId: questionId,
                        )));
              },

              //     padding: new EdgeInsets.all(0.0),
              //     child: new Row(children: <Widget>[new Icon],)
              //       style: this.textStyle(
              //           size: 14,
              //           color: Colors.white,
              //           fontWeight: FontWeight.w500),
              //     ),
              //     onPressed: null,
              //     shape: new RoundedRectangleBorder(
              //         borderRadius: new BorderRadius.circular(9.0))),
              // decoration: new BoxDecoration(
              //     color: Color(0XFFFF2D55),
              //     borderRadius: new BorderRadius.all(const Radius.circular(9.0))),
            ),
          ),
        ],
      ),
    );

    //用户有关的Widget
    var userInfoView = new Container(
        color: const Color(0xfffcfcfc),
        child: new Row(
          children: <Widget>[
            //放用户图片
            new CircleAvatar(
              radius: 20.0,
              backgroundImage:
                  NetworkImage(authorAvatarUrl ?? Repository.defaultfAvatarUri),
            ),
            //用户名和发布时间
            new Container(
              margin: new EdgeInsets.fromLTRB(10.0, 0.0, 0.0, 0.0),
              child: new Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  LimitedBox(
                    child: Text(
                      authorUsername,
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                      style: textStyle(
                          size: 14,
                          color: Color(0xFF818181),
                          fontWeight: FontWeight.w600),
                    ),
                    maxWidth: 64.0,
                  ),
                  Text('发布于 ${getDate(DateTime.parse(postTime))}',
                      style: textStyle(
                        size: 14,
                        color: Color(0xFF818181),
                      )),
                ],
              ),
            )
          ],
        ));

    var _body = answerData == null
        ? new Center(
            child: new PlatformCircularProgressIndicator(
              android: (_) => MaterialProgressIndicatorData(
                    backgroundColor: Color(0XFFFF2D55),
                  ),
              ios: (_) => CupertinoProgressIndicatorData(),
            ),
          )
        : new Material(
          color: Colors.transparent,
            child: new Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                new Container(
                  height: 30.0,
                  margin: new EdgeInsets.fromLTRB(8.0, 12.0, 8.0, 0.0),
                  child: questionTitleView,
                ),
                new Container(
                  height: 40.0,
                  margin: new EdgeInsets.fromLTRB(8.0, 12.0, 8.0, 0.0),
                  child: userInfoView,
                ),
                new Flexible(
                  flex: 1,
                  fit: FlexFit.tight,
                  child: new Container(
                    margin: new EdgeInsets.fromLTRB(20.0, 12.0, 20.0, 0.0),
                    child: new ListView.builder(
                      itemCount:
                          answerData.length == 0 ? 1 : answerData.length * 2,
                      itemBuilder: renderListItem,
                      controller: _controller,
                    ),
                  ),
                ),
              ],
            ),
          );

    return new PlatformScaffold(
        appBar: new PlatformAppBar(
          // backgroundColor: Color(0xFFC62828),
          title: new Text(
            "问题详情",
          ),
          // android: (BuildContext context) => MaterialAppBarData(
          //       iconTheme: new IconThemeData(color: Colors.white),
          //     ),
          // ios: (_) => CupertinoNavigationBarData(),
        ),
        body: _body);
  }

  Widget renderListItem(BuildContext context, int i) {
    if (i == 0) {
      return getQuestionDetailView();
    }
    i -= 1;
    if (i.isOdd && i != 2 * answerData.length - 3) {
      return new Divider(
        height: 1.0,
      );
    } else if (i.isOdd && i == 2 * answerData.length - 3) {
      return new Divider(
        height: 0.0,
        color: Colors.white,
      );
    }
    i ~/= 2;
    return _renderAnswerRow(context, i);
  }

  Widget renderCommentItem(BuildContext context, int i) {
    if (commentData.length == 0) {
      return new EndLine();
    }
    if (i.isOdd && i != 0) {
      return new Divider(
        height: 1.0,
      );
    } else if (i.isOdd && i == 0) {
      return new Divider(
        height: 1.0,
        color: Colors.white,
      );
    }
    return _renderCommentRow(context, i);
  }

  //渲染回答列表
  _renderAnswerRow(context, i) {
    final listItem = answerData[i];
    if (listItem is String && listItem == 'COMPLETE') {
      return new EndLine();
    }
    String userName = listItem['username'].toString();
    String userImageUrl = listItem['user_image']['url'].toString();
    DateTime dateTime = DateTime.parse(listItem['createdAt']['iso'].toString());
    String content = listItem['answer_detail'].toString();
    String answerToComment = listToJsonString(listItem['answer_to_comment']);
    var row = new Container(
      margin: const EdgeInsets.fromLTRB(0.0, 20.0, 0.0, 20.0),
      child: new Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          new Row(
            children: <Widget>[
              //放用户图片
              new Container(
                width: 20.0,
                height: 20.0,
                decoration: new BoxDecoration(
                  image: new DecorationImage(
                      image: new NetworkImage(userImageUrl)),
                  borderRadius: new BorderRadius.all(
                    const Radius.circular(10.0),
                  ),
                ),
              ),
              new Container(
                height: 20.0,
                margin: const EdgeInsets.fromLTRB(8.0, 0.0, 0.0, 0.0),
                child: Row(
                  children: <Widget>[
                    LimitedBox(
                      child: Text(
                        userName,
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis,
                        style: textStyle(
                            size: 12,
                            color: Color(0xFF818181),
                            fontWeight: FontWeight.normal),
                      ),
                      maxWidth: 64.0,
                    ),
                    Text(' 回答于 ${getDate(dateTime)}',
                        style: textStyle(
                          size: 12,
                          color: Color(0xFF818181),
                        )),
                  ],
                ),
              ),
            ],
          ),
          new Container(
            alignment: Alignment.topLeft,
            // margin: EdgeInsets.all(20.0),
            child: ZefyrEditor(
              padding: EdgeInsets.fromLTRB(12.0, 12.0, 12.0, 0.0),
              // autofocus: true,

              controller: new ZefyrController(
                  new NotusDocument.fromJson(json.decode(content))),
              focusNode: new FocusNode(),
              scrollable: false,
              enabled: false,
              imageDelegate: LeanCloudImageDelegate(),
            ),
          ),
        ],
      ),
    );
    return new Builder(
      builder: (ctx) {
        return new InkWell(
          onTap: () {
            getComment(answerToComment: answerToComment, ctx: ctx);
          },
          child: row,
        );
      },
    );
  }
  

  //渲染回答列表
  _renderCommentRow(context, i) {
    var listItem = commentData[i];
    String userName = listItem['username'].toString();
    String userImageUrl = listItem['user_image']['url'].toString();
    String content = listItem['comment_detail'].toString();
    DateTime dateTime = DateTime.parse(listItem['createdAt']['iso'].toString());
    var row = new Container(
      margin: const EdgeInsets.fromLTRB(0.0, 10.0, 0.0, 10.0),
      child: new Column(
        children: <Widget>[
          new Row(
            children: <Widget>[
              //放用户图片
              new Container(
                width: 20.0,
                height: 20.0,
                decoration: new BoxDecoration(
                  image: new DecorationImage(
                      image: new NetworkImage(userImageUrl)),
                  borderRadius: new BorderRadius.all(
                    const Radius.circular(10.0),
                  ),
                ),
              ),
              new Container(
                height: 20.0,
                margin: const EdgeInsets.fromLTRB(6.0, 0.0, 0.0, 0.0),
                child: new Text(userName + ' 评论于',
                    style: textStyle(
                        size: 12,
                        color: Color(0xFF818181),
                        fontWeight: FontWeight.normal)),
              ),
              new Padding(
                  padding: const EdgeInsets.fromLTRB(0.0, 0.0, 10.0, 0.0),
                  child: new Text(
                    getDate(dateTime),
                    style: subtitleStyle,
                  ))
            ],
          ),
          new Padding(
              padding: const EdgeInsets.fromLTRB(0.0, 0.0, 10.0, 0.0),
              child: new Row(
                children: <Widget>[
                  new Expanded(
                      child: new Text(
                    content,
                    style: contentStyle,
                  ))
                ],
              ))
        ],
      ),
    );
    return new Builder(
      builder: (ctx) {
        return new InkWell(
          child: row,
        );
      },
    );
  }

  showCommentBottomView(answerList, ctx) {
    showModalBottomSheet(
        context: ctx,
        builder: (sheetCtx) {
          return new Column(
            key: GlobalKey(),
            children: <Widget>[
              new Container(
                height: 24.0,
                margin: EdgeInsets.fromLTRB(16.0, 12.0, 16.0, 0.0),
                child: Text(
                  '评论信息',
                  style: Theme.of(context)
                      .textTheme
                      .title
                      .copyWith(fontWeight: FontWeight.bold),
                ),
              ),
              new Container(
                margin: new EdgeInsets.fromLTRB(16.0, 20.0, 16.0, 0.0),
                child: new ListView.builder(
                  itemCount:
                      commentData.length == 0 ? 1 : commentData.length * 2 - 1,
                  itemBuilder: renderCommentItem,
                  controller: _controller,
                ),
              ),
              SizedBox(
                width: 128.0,
                height: 32.0,
                child: TextField(
                  maxLines: 1,
                  style: textStyle(
                      size: 20,
                      color: Colors.black,
                      fontWeight: FontWeight.normal),
                  controller: _inputController,
                  // autofocus: true,
                  decoration: new InputDecoration(
                      hintText: "评论点啥呗～",
                      contentPadding:
                          const EdgeInsets.fromLTRB(5.0, 5.0, 5.0, 5.0),
                      border: new UnderlineInputBorder(
                        borderRadius:
                            const BorderRadius.all(const Radius.circular(10.0)),
                      )),
                ),
              )
              // Container(height: 50.0,)
              // top: 2.0,
            ],
          );
        });
  }

  /*
  void sendReply(authorId) {
    String replyStr = _inputController.text;
    if (replyStr == null || replyStr.length == 0 || replyStr.trim().length == 0) {
      return;
    } else {
      DataUtils.isLogin().then((isLogin) {
        if (isLogin) {
          DataUtils.getAccessToken().then((token) {
            Map<String, String> params = new Map();
            params['access_token'] = token;
            params['id'] = "${tweetData['id']}";
            print("id: ${tweetData['id']}");
            params['catalog'] = "3";
            params['content'] = replyStr;
            params['authorid'] = "$authorId";
            print("authorId: $authorId");
            params['isPostToMyZone'] = "0";
            params['dataType'] = "json";
            NetUtils.get(Api.COMMENT_REPLY, params: params).then((data) {
              if (data != null) {
                var obj = json.decode(data);
                var error = obj['error'];
                if (error != null && error == '200') {
                  // 回复成功
                  Navigator.of(context).pop();
                  getReply(false);
                }
              }
            });
          });
        }
      });
    }
  }*/

  Widget getQuestionDetailView() {
    return questionBody == null?
    new Container(
      alignment: Alignment.topLeft,
      // margin: EdgeInsets.only(top: 92.0),
      child: ZefyrEditor(
        // padding: EdgeInsets.only(top: 24.0, left: 24),
        // autofocus: true,
        controller: new ZefyrController(
            new NotusDocument.fromJson(json.decode(questionBody))),
        scrollable: false,
        focusNode: new FocusNode(),
        enabled: false,
        imageDelegate: LeanCloudImageDelegate(),
      ),
    ) : new Container(
      height: 20.0,
    );
  }
}
