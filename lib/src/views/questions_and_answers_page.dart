import 'package:flutter/material.dart';
import 'package:most/src/common/repository.dart';
import 'package:most/src/views/question_and_answer.dart';
import 'package:most/src/widgets/add_post_fab.dart';
import 'package:most/src/widgets/card.dart';
import 'package:most/src/widgets/search_bar.dart';

import '../../src/api/leancloud.dart';
import '../../src/widgets/image_text_widget.dart';
import 'package:fab_menu/fab_menu.dart';
import 'package:flutter_platform_widgets/flutter_platform_widgets.dart';

class QuestionsAndAnswersPage extends StatefulWidget {
  QuestionsAndAnswersPage({Key key}) : super(key: key);
  @override
  State<StatefulWidget> createState() {
    return new QuestionsAndAnswersPageState();
  }
}

class QuestionsAndAnswersPageState extends State<QuestionsAndAnswersPage> {
  List dataQ1;
  List dataQ2;
  List dataA1;

  TextStyle subtitleStyle =
      new TextStyle(fontSize: 12.0, color: const Color(0xFFB5BDC0));
  TextStyle contentStyle = new TextStyle(fontSize: 15.0, color: Colors.black);

  QuestionsAndAnswersPageState({Key key});

  //设置字体
  textStyle({int size, Color color, FontWeight fontWeight}) {
    return new TextStyle(
      fontSize: size.toDouble(),
      decoration: TextDecoration.none,
      color: color,
      fontWeight: fontWeight,
    );
  }

  setData() async {
    var newDataQ1 = await LeanCloudFunction.callFunction(
        myData: {'father_group': 0, 'sub_group': 0, 'start': 0, 'end': 3},
        functionName: 'recommend_QA');
    var newDataQ2 = await LeanCloudFunction.callFunction(
        myData: {'father_group': 0, 'sub_group': 0, 'start': 0, 'end': 3},
        functionName: 'new_QA');
    var newDataA1 = await LeanCloudFunction.callFunction(
        myData: {'father_group': 0, 'sub_group': 0, 'start': 0, 'end': 3},
        functionName: 'recommend_answer');
    if (this.mounted) {
      this.setState(() {
        dataQ1 = newDataQ1;
        dataQ2 = newDataQ2;
        dataA1 = newDataA1;
        PageStorage.of(context)
            .writeState(context, dataQ1, identifier: 'dataQ1');
        PageStorage.of(context)
            .writeState(context, dataQ2, identifier: 'dataQ2');
        PageStorage.of(context)
            .writeState(context, dataA1, identifier: 'dataA1');
      });
    }
  }

  @override
  initState() {
    super.initState();
    setData();
  }

  @override
  Widget build(BuildContext context) {
    dataQ1 = PageStorage.of(context).readState(context, identifier: 'dataQ1');
    dataQ2 = PageStorage.of(context).readState(context, identifier: 'dataQ2');
    dataA1 = PageStorage.of(context).readState(context, identifier: 'dataA1');
    var _body = (dataQ1 == null && dataQ2 == null && dataA1 == null)
        ? new Center(
            child: PlatformCircularProgressIndicator(
              android: (_) => MaterialProgressIndicatorData(
                    backgroundColor: Color(0XFFFF2D55),
                  ),
              ios: (_) => CupertinoProgressIndicatorData(),
            ),
          )
        : new ListView(
            children: <Widget>[
              new Container(
                height: 64.0,
                margin: EdgeInsets.fromLTRB(0.0, 20.0, 25.0, 0.0),
                child: new Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    // new Container(
                    //   margin: EdgeInsets.fromLTRB(25.0, 0.0, 14.0, 0.0),
                    //   child: Text(
                    //     '话题',
                    //     style: textStyle(
                    //         size: 16,
                    //         color: Color(0XFF545454),
                    //         fontWeight: FontWeight.w600),
                    //   ),
                    // ),
                    // new Container(
                    //   width: 1.0,
                    //   height: 64.0,
                    //   margin: EdgeInsets.all(0.0),
                    //   color: Color(0XFFA5A5A5),
                    // ),
                    new Flexible(
                      fit: FlexFit.tight,
                      flex: 1,
                      child: new Row(
                        children: <Widget>[
                          new Expanded(
                            child: GestureDetector(
                              onTap: () {
                                Navigator.of(context)
                                    .push(new MaterialPageRoute(builder: (ctx) {
                                  return new QAPage(
                                    fatherGroup: 1,
                                    subGroup: 0,
                                    titleName: '学术中心',
                                  );
                                }));
                              },
                              child: ImageTextCard(
                                imageStirng: 'graphics/QA1.png',
                                isNetImage: false,
                                title: '学术中心',
                              ),
                            ),
                          ),
                          new Expanded(
                            child: GestureDetector(
                              onTap: () {
                                Navigator.of(context)
                                    .push(new MaterialPageRoute(builder: (ctx) {
                                  return new QAPage(
                                    fatherGroup: 2,
                                    subGroup: 0,
                                    titleName: '个人发展',
                                  );
                                }));
                              },
                              child: ImageTextCard(
                                imageStirng: 'graphics/QA2.png',
                                isNetImage: false,
                                title: '个人发展',
                              ),
                            ),
                          ),
                          new Expanded(
                            child: GestureDetector(
                              onTap: () {
                                Navigator.of(context)
                                    .push(new MaterialPageRoute(builder: (ctx) {
                                  return new QAPage(
                                    fatherGroup: 3,
                                    subGroup: 0,
                                    titleName: '校园指南',
                                  );
                                }));
                              },
                              child: ImageTextCard(
                                imageStirng: 'graphics/QA3.png',
                                isNetImage: false,
                                title: '校园指南',
                              ),
                            ),
                          ),
                          new Expanded(
                            child: GestureDetector(
                              onTap: () {
                                Navigator.of(context)
                                    .push(new MaterialPageRoute(builder: (ctx) {
                                  return new QAPage(
                                    fatherGroup: 4,
                                    subGroup: 0,
                                    titleName: '南腔北调',
                                  );
                                }));
                              },
                              child: ImageTextCard(
                                imageStirng: 'graphics/QA4.png',
                                isNetImage: false,
                                title: '南腔北调',
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              new MyCard(
                cardData: dataQ1,
                cardTitle: '热门问题',
                isFirstCard: true,
                numberOfLines: 4,
              ),
              new MyCard(
                cardData: dataQ2,
                cardTitle: '最新问题',
                isFirstCard: false,
                numberOfLines: 4,
              ),
              new MyCard(
                cardData: dataA1,
                cardTitle: '热门回答',
                isFirstCard: false,
                numberOfLines: 4,
              ),
            ],
          );

    return new Scaffold(
      appBar: new AppBar(
        automaticallyImplyLeading: false,
        // textTheme: Theme.of(context).textTheme.copyWith(
        //       title: Theme.of(context)
        //           .textTheme
        //           .title
        //           .copyWith(color: Theme.of(context).primaryColor),
        //     ),
        // iconTheme: Theme.of(context)
        //     .iconTheme
        //     .copyWith(color: Theme.of(context).primaryColor),
        backgroundColor: Colors.white,
        title: Image.asset(
          Repository.mostLogoUri,
          height: 24.0,
        ),
        // title: new Text(
        //   'MOST',
        // ),
        actions: <Widget>[new SearchButton()],
      ),
      // appBar: buildAppBar(context),
      body: _body,
      floatingActionButton: AddPostFAB(),
      floatingActionButtonLocation:
          fabMenuLocation, // Remember to set floatingActionButtonLocation to fabMenuLocation')
    );
  }

  String listToJsonString(List list) {
    String result = '[';
    for (var i = 0; i < list.length; i++) {
      result += '"' + list[i] + '"';
      if (i != list.length - 1) {
        result += ',';
      }
    }
    result += ']';
    return result;
  }
}
