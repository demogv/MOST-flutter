import 'dart:async';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:most/src/api/leancloud.dart';
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';
import '../common/events.dart';
import '../common/repository.dart';

// import '../models/main.dart';
import '../models/user.dart';

import 'package:flutter_platform_widgets/flutter_platform_widgets.dart';
import 'package:groovin_material_icons/groovin_material_icons.dart';
import 'package:image_picker/image_picker.dart';
import 'package:image_cropper/image_cropper.dart';

class Login extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return new _LoginState();
  }
}

class _LoginState extends State<Login> {
  final _studentIdController = new TextEditingController();
  final _formKey = GlobalKey<FormFieldState>();

  // Never put it in `build` to avoid loading multiple times
  FlutterWebviewPlugin flutterWebviewPlugin;

  /// Parse id token uri which incorrectly has a `#` instead of a `?`.
  /// Returns the id_token.
  String _parseIdToken(String uri) {
    uri = uri.replaceFirst('#', '?');
    final Map<String, String> parameters = Uri.parse(uri).queryParameters;
    if (parameters.containsKey('id_token')) {
      return parameters['id_token'];
    } else {
      print('No id token.');
      return null;
    }
  }

  @override
  void initState() {
    super.initState();

    // Listen the event of opening a login webview.
    eventBus.on<OpenLoginWebViewEvent>().listen(
      (event) async {
        // flutterWebviewPlugin.close();
        print('event.url: ${event.url}');

        // flutterWebviewPlugin.launch(event.url,
        //     clearCache: true, clearCookies: true);

        await Navigator.of(context).push(
          new MaterialPageRoute(
            builder: (BuildContext context) {
              return new WebviewScaffold(
                // TODO: add autofill
                clearCache: true,

                // clearCookies: true,
                url: event.url,
                appBar: new AppBar(
                  title: Text('使用 CUHK(SZ) 学生账号登录'),
                ),
              );
            },
          ),
        );
      },
    );

    // Go to home page when logged in
    eventBus.on<UserLoggedInEvent>().listen((loginInfo) {
      // if (loginInfo.isNewUser) {
      if (false) {
        // TODO: remove this
        Navigator.of(context).push(MaterialPageRoute<void>(
            builder: (BuildContext context) => NewUserWizard(
                  avatar: loginInfo.avatar,
                )));
      } else {
        Navigator.of(context)
            .pushNamedAndRemoveUntil('/home', (Route<dynamic> r) => false)
            .then(
              (onValue) => Scaffold.of(context).showSnackBar(
                    new SnackBar(
                      content: Text('登录成功!'),
                    ),
                  ),
            );
      }
    });
  }

  // https://stackoverflow.com/questions/24085385/checking-if-string-is-numeric-in-dart#24085491
  bool _isNumeric(String str) {
    try {
      double.parse(str);
    } on FormatException {
      return false;
    } finally {
      return true;
    }
  }

  String _studentIdValidater(String id) {
    if (id.isEmpty) return '请输入学号噢';
    if (id.length != 9) return '请完整输入学号噢，形如 11701234';
    if (!_isNumeric(id)) return '学号都是数字噢，形如 11701234';
    if (!id.startsWith('11')) return '格式不对噢，再检查看看，正确格式形如 11701234';
  }

  @override
  void dispose() {
    // TODO: implement dispose
    flutterWebviewPlugin.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return PlatformScaffold(
      body: SafeArea(
        child: Stack(
          children: <Widget>[
            Container(
              margin: EdgeInsets.only(top: 52.0, left: 52.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Image.asset(
                    Repository.mostLogoUri,
                    width: 84.0,
                  ),
                  Container(height: 16.0),
                  Text(
                    '欢迎，',
                    style: Theme.of(context).textTheme.display1.copyWith(
                        fontWeight: FontWeight.bold, color: Colors.black),
                  ),
                  Text(
                    '使用学生账号登录 MOST',
                    style: Theme.of(context).textTheme.subhead.copyWith(
                        fontWeight: FontWeight.bold, color: Colors.black54),
                  ),
                ],
              ),
            ),
            Positioned(
              bottom: 284.0,
              left: 52.0,
              right: 52.0,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: <Widget>[
                  Text(
                    'CUHK(SZ) 学生账号',
                    style: Theme.of(context)
                        .textTheme
                        .title
                        .copyWith(color: Colors.black87),
                  ),
                  TextFormField(
                    key: _formKey,
                    controller: _studentIdController,
                    autofocus: true,
                    keyboardType: TextInputType.numberWithOptions(),
                    validator: _studentIdValidater,
                    maxLength: 9,
                    style: Theme.of(context).textTheme.title,
                    decoration: InputDecoration(
                      hintText: '1180XXXXX',

                      // labelStyle: Theme.of(context)
                      //     .textTheme
                      //     .title
                      //     .copyWith(color: Colors.black87),
                      suffixText: '@link.cuhk.edu.cn',

                      suffixStyle: Theme.of(context)
                          .textTheme
                          .subhead
                          .copyWith(color: Colors.black45),

                      // errorText: '请输入你的学号,例如 117020000',
                    ),
                  ),
                  Container(height: 32.0),
                  PlatformButton(
                    color: Theme.of(context).primaryColorDark,
                    padding: EdgeInsets.symmetric(vertical: 12.0),
                    android: (_) => MaterialRaisedButtonData(
                        elevation: 4.0,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(56.0))),

                    child: Text(
                      '继续',
                      style: Theme.of(context).textTheme.button.copyWith(
                          color: Colors.white, fontWeight: FontWeight.bold),
                    ),

                    onPressed: () async {
                      if (_formKey.currentState.validate()) {
                        // flutterWebviewPlugin.dispose();
                        flutterWebviewPlugin = new FlutterWebviewPlugin();

                        // Init listening
                        flutterWebviewPlugin.onHttpError.listen((onData) {
                          print(onData.code);
                        });
                        // When redirected to localhost, close webview.
                        flutterWebviewPlugin.onUrlChanged.listen((String url) {
                          // TODO: since it takes time, prompt a loading page
                          // print('url: $url');
                          print(url.substring(0, 16));
                          if (url.substring(0, 16) == 'http://localhost') {
                            print('oh');
                            eventBus
                                .fire(UserIdTokenRetrieved(_parseIdToken(url)));
                            flutterWebviewPlugin.close();
                            Navigator.of(context).push(
                              MaterialPageRoute(
                                builder: (BuildContext context) =>
                                    new PlatformScaffold(
                                      body: Center(
                                        child:
                                            PlatformCircularProgressIndicator(),
                                      ),
                                    ),
                              ),
                            );
                          }
                        });

                        final String studentId =
                            _studentIdController.value.text;
                        User.signUpOrLogIn(studentId: studentId);
                        // // await getIdTokenAndAccessToken(user: studentId);
                        // final a = Most();
                        // a.test();
                        // await most.user.signUpOrlogIn(studentId: studentId);
                        // most.user.getUserInfo();
                        // print('hello');
                        // TODO: upload data to leancloud
                      }
                    }, // TODO: write validation check
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class NewUserWizard extends StatefulWidget {
  final FileData avatar;
  final String username;

  const NewUserWizard({Key key, this.avatar, this.username}) : super(key: key);

  @override
  _NewUserWizardState createState() => _NewUserWizardState();
}

class _NewUserWizardState extends State<NewUserWizard> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  Widget _buildNewUserWizard() {
    return Center(
        child: NewUserForm(
      avatar: widget.avatar,
    ));
  }

  @override
  Widget build(BuildContext context) {
    return PlatformScaffold(
      // appBar: PlatformAppBar(
      //   leading: Icon(GroovinMaterialIcons.fire),
      //   title: Text('欢迎你，新朋友'),
      // ),
      body: Material(
        child: _buildNewUserWizard(),
      ),
    );
  }
}

// Create a Form Widget
class NewUserForm extends StatefulWidget {
  final FileData avatar;

  const NewUserForm({Key key, this.avatar}) : super(key: key);

  @override
  NewUserFormState createState() {
    return NewUserFormState();
  }
}

// Create a corresponding State class. This class will hold the data related to
// the form.
class NewUserFormState extends State<NewUserForm> {
  // Create a global key that will uniquely identify the Form widget and allow
  // us to validate the form
  //
  // Note: This is a GlobalKey<FormState>, not a GlobalKey<MyCustomFormState>!
  final _formKey = GlobalKey<FormState>();
  FileData _avatar;
  final _usernameController = TextEditingController();

  void initState() {
    super.initState();
    _avatar = widget.avatar;
  }

  Future<File> _cropImage(File imageFile) async {
    return await ImageCropper.cropImage(
      sourcePath: imageFile.path,
      ratioX: 1.0,
      ratioY: 1.0,
      maxWidth: 512,
      maxHeight: 512,
    );
  }

  Future _getImage() async {
    File image = await ImagePicker.pickImage(source: ImageSource.gallery);

    // Crop image
    image = await _cropImage(image);
    // print(image.);
    // image = await _cropImage(image);
    // TODO: Remove this

    if (image != null) {
      final imageFileData = await LeanCloudFile.upload(
        image.uri,
      );

      print(imageFileData.uri.toString());

      // ensure the object is still in the tree.
      if (this.mounted) {
        // Only setState when uploading was successful
        if (imageFileData != null) {
          setState(() {
            _avatar = imageFileData;
          });
        } else {
          final snackBar = SnackBar(content: Text('抱歉，头像更换失败'));
          Scaffold.of(context).showSnackBar(snackBar);
        }
      }
    }
  }

  String _usernameValidator(String username) {
    if (username.isEmpty) {
      return '取个名字吧~';
    } else if (username.length > 15) {
      return '昵称不可以超过 15 字噢';
    } else {
      return null;
    }
  }

  @override
  Widget build(BuildContext context) {
    // Build a Form widget using the _formKey we created above
    return Form(
      key: _formKey,
      child: SafeArea(
        child: Stack(
          children: <Widget>[
            Positioned(
              top: 52.0,
              left: 52.0,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Image.asset(
                    Repository.mostLogoUri,
                    width: 84.0,
                  ),
                  Container(height: 16.0),
                  Text(
                    '你好，新朋友',
                    style: Theme.of(context).textTheme.display1.copyWith(
                        fontWeight: FontWeight.bold, color: Colors.black),
                  ),
                  Text(
                    '设置一下头像和用户名吧',
                    style: Theme.of(context).textTheme.subhead.copyWith(
                        fontWeight: FontWeight.bold, color: Colors.black54),
                  ),
                ],
              ),
            ),
            Positioned(
              bottom: 84.0,
              left: 52.0,
              right: 52.0,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: <Widget>[
                  FlatButton(
                    shape: CircleBorder(),
                    child: CircleAvatar(
                      radius: 60.0,
                      backgroundImage: (_avatar?.uri?.toString() != null)
                          ? NetworkImage(_avatar?.uri?.toString())
                          : AssetImage(Repository.defaultfAvatarUri),

                      //     img != null
                      // ? Image.file(img)
                      // : Container(),
                    ),
                    onPressed: () async => await _getImage(),
                  ),
                  Padding(
                    padding: EdgeInsets.fromLTRB(0.0, 28.0, 0.0, 64.0),
                    child: TextFormField(
                      controller: _usernameController,
                      maxLength: 15,
                      decoration: InputDecoration(
                        labelText: '用户名',
                        // hintText:
                      ),
                      validator: _usernameValidator,
                    ),
                  ),
                  RaisedButton(
                    onPressed: () async {
                      // Validate will return true if the form is valid, or false if
                      // the form is invalid.
                      if (_formKey.currentState.validate()) {
                        Navigator.of(context).pushNamed(
                          '/home',
                          // (Route<dynamic> r) => false,
                        );
                        // Update local data
                        Repository.get()
                            .setThisUsername(_usernameController.text);
                        // Update online data
                        final sessionToken =
                            await Repository.get().sessionToken;
                        final userObjectId =
                            await Repository.get().thisUserObjectId;
                        await LeanCloudUser.updateUserInfo(
                            userObjectId,
                            UserEditableInfo(
                                avatar: _avatar,
                                username: _usernameController.text),
                            sessionToken);
                        // Go to home page

                        Scaffold.of(context)
                            .showSnackBar(SnackBar(content: Text('正在进入')));
                      }
                    },
                    color: Theme.of(context).primaryColorDark,
                    padding: EdgeInsets.symmetric(vertical: 12.0),
                    elevation: 4.0,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(56.0)),
                    child: Text(
                      '保存并进入首页',
                      style: Theme.of(context).textTheme.button.copyWith(
                          color: Colors.white, fontWeight: FontWeight.bold),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
