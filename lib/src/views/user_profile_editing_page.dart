import 'dart:async';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:image_picker/image_picker.dart';
import 'package:most/src/api/leancloud.dart';
import '../common/repository.dart';

class UserProfileEditing extends StatefulWidget {
  const UserProfileEditing({Key key}) : super(key: key);
  @override
  _UserProfileEditingState createState() => _UserProfileEditingState();
}

class _UserProfileEditingState extends State<UserProfileEditing> {
  final _formKey = GlobalKey<FormState>();

  // Controllers
  final _usernameController = TextEditingController();
  final _alipayAccountController = TextEditingController();
  // TextEditingController _collegeController;
  // TextEditingController _schoolController;
  String _avatarUri;
  FileData _avatar;

  Future _initUserProfile() async {
    _usernameController.text = await Repository.get().thisUsername;
    final thisUserInfo = await Repository.get().thisUserInfo;
    if (this.mounted && thisUserInfo != null) {
      setState(() {
        _usernameController.text = thisUserInfo?.username;
        _alipayAccountController.text = thisUserInfo?.alipayAccount;
        _avatar = thisUserInfo?.avatar;
      });
    }
  }

  @override
  void initState() {
    super.initState();
    _initUserProfile();
  }

  String _usernameValidator(String username) {
    if (username.isEmpty) {
      return '取个名字吧~';
    } else if (username.length > 15) {
      return '昵称不可以超过 15 字噢';
    } else {
      return null;
    }
  }

  Future<File> _cropImage(File imageFile) async {
    return await ImageCropper.cropImage(
      sourcePath: imageFile.path,
      ratioX: 1.0,
      ratioY: 1.0,
      maxWidth: 512,
      maxHeight: 512,
    );
  }

  Future _getImage() async {
    File image = await ImagePicker.pickImage(source: ImageSource.gallery);

    // Crop image
    image = await _cropImage(image);
    // print(image.);
    // image = await _cropImage(image);
    // TODO: Remove this

    if (image != null) {
      final imageFileData = await LeanCloudFile.upload(
        image.uri,
      );

      print(imageFileData.uri.toString());

      // ensure the object is still in the tree.
      if (this.mounted) {
        // Only setState when uploading was successful
        if (imageFileData != null) {
          setState(() {
            _avatar = imageFileData;
          });
        } else {
          final snackBar = SnackBar(content: Text('抱歉，头像更换失败'));
          Scaffold.of(context).showSnackBar(snackBar);
        }
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('编辑个人资料'),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.save),
            onPressed: () async {
              final info = UserEditableInfo(
                  alipayAccount: _alipayAccountController.text,
                  avatar: _avatar,
                  username: _usernameController.text);
              if (_formKey.currentState.validate()) {
                LeanCloudUser.updateUserInfo(
                    await Repository.get().thisUserObjectId,
                    info,
                    await Repository.get().sessionToken);
                Navigator.of(context).pop();
              }
            },
          )
        ],
      ),
      body: Form(
        autovalidate: true,
        key: _formKey,
        child: ListView(
          padding: EdgeInsets.all(56.0),
          children: <Widget>[
            Container(
              alignment: Alignment.topCenter,
              child: FlatButton(
                shape: CircleBorder(),
                child: CircleAvatar(
                    radius: 60.0,
                    child: (_avatar != null || _avatarUri != null)
                        ? FadeInImage.assetNetwork(
                            image: _avatar?.uri?.toString() ?? _avatarUri,
                            placeholder: Repository.defaultfAvatarUri,
                          )
                        : Image.asset(Repository.defaultfAvatarUri)
                    //     img != null
                    // ? Image.file(img)
                    // : Container(),
                    ),
                onPressed: () async => await _getImage(),
              ),
            ),
            TextFormField(
              controller: _usernameController,
              maxLength: 15,
              validator: _usernameValidator,
              decoration: InputDecoration(
                labelText: '用户名',
              ),
            ),
            TextFormField(
              controller: _alipayAccountController,
              decoration: InputDecoration(
                labelText: '支付宝账户名（收款）',
                helperText: '若输入错误，收款须联系我们噢',
              ),
            ),
          ],
        ),
      ),
    );
  }
}
