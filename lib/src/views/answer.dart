import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:most/src/common/repository.dart';
import 'package:zefyr/zefyr.dart';
import '../utils/leancloudImageDelegate.dart';

class AnswerPage extends StatefulWidget {
  final String questionerObjectId;
  final String questionObjectId;
  final String questionTitle;

  const AnswerPage(
      {Key key,
      this.questionerObjectId,
      this.questionObjectId,
      this.questionTitle})
      : super(key: key);

  @override
  _AnswerPageState createState() => _AnswerPageState();
}

class _AnswerPageState extends State<AnswerPage> {
  final ZefyrController _controller = ZefyrController(NotusDocument());
  final FocusNode _focusNode = new FocusNode();

  final snackBar = SnackBar(
    content: Text('还是要输点内容噢'),
    action: SnackBarAction(
      label: '好',
      onPressed: () {},
    ),
  );
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          '回答 ${widget.questionTitle}',
        ),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.send),
            onPressed: () {
              final doc = json.encode(_controller.document.toJson());
              if (doc.isEmpty) {
                Scaffold.of(context).showSnackBar(snackBar);
              } else {
                Repository.get().uploadAnswer(
                  questionObjectId: widget.questionObjectId,
                  answerBody: doc,
                  questionerObjectId: widget.questionerObjectId,
                );
                Navigator.of(context)
                    .pushNamedAndRemoveUntil('/', (Route<dynamic> r) => false);
              }
            },
          )
        ],
      ),
      body: ZefyrEditor(
        controller: _controller,
        focusNode: _focusNode,
        enabled: true,
        imageDelegate: LeanCloudImageDelegate(),
        autofocus: true,
      ),
    );
  }
}
