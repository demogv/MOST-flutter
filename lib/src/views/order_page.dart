import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'dart:async';
import 'package:date_format/date_format.dart';
import 'package:flutter_platform_widgets/flutter_platform_widgets.dart';
import 'package:most/src/api/leancloud.dart';
import 'package:most/src/common/repository.dart';
import 'package:most/src/utils/utils.dart';
//import 'package:flutter_search_bar/flutter_search_bar.dart';

class OrderPage extends StatefulWidget {
  final String orderId;
  final String myId;

  OrderPage({Key key, this.orderId, this.myId}) : super(key: key);
  @override
  State<StatefulWidget> createState() {
    return new OrderPageState(orderId: orderId, myId: myId);
  }
}

class OrderPageState extends State<OrderPage> {
  String orderId;
  String myId;

  List conversationData;
  Map<String, dynamic> orderObject;
  bool keepUpdating;
  //RegExp regExp1 = new RegExp("</.*>");
  //RegExp regExp2 = new RegExp("<.*>");
  TextStyle subtitleStyle =
      new TextStyle(fontSize: 12.0, color: const Color(0xFFB5BDC0));
  TextStyle contentStyle = new TextStyle(fontSize: 15.0, color: Colors.black);
  //num curPage = 1;
  ScrollController _controller = new ScrollController();
  TextEditingController _inputController = new TextEditingController();

  OrderPageState({Key key, this.orderId, this.myId});

  //设置字体
  textStyle({int size, Color color, FontWeight fontWeight}) {
    return new TextStyle(
      fontSize: size.toDouble(),
      decoration: TextDecoration.none,
      color: color,
      fontWeight: fontWeight,
    );
  }

  getData() async {
    // TODO: avoid master key
    // String userIdGet = await Repository.get().thisUserObjectId;
    // LeanCloudFunction cloudFunction = LeanCloudFunction();
    // var orderData = await LeanCloudFunction.callFunction(myData: {
    //   'order_id': orderId,
    // }, functionName: 'get_order');
    // List newConversationData = await LeanCloudFunction.getWithMasterKey(
    //     body: {},
    //     urlAddition:
    //         '/rtm/conversations/' + orderData['conversation_id'] + '/messages');
    // setState(() {
    //   conversationData = newConversationData;
    //   myId = userIdGet;
    //   orderObject = orderData;
    //   keepUpdating = true;
    //   startUpdating();
    // });
  }

  @override
  void initState() {
    super.initState();
    getData();
  }

  startUpdating() async {
    while (keepUpdating) {
      await Future.delayed(const Duration(seconds: 2), () async {
        if (conversationData.length == 0) {
          // TODO: avoid master key
          // var newConversationData = await LeanCloudFunction.getWithMasterKey(
          //     body: {},
          //     urlAddition: '/rtm/conversations/' +
          //         orderObject['conversation_id'] +
          //         '/messages');
          // setState(() {
          //   conversationData = newConversationData;
          // });
        } else {
          // List newConversationData = await LeanCloudFunction.getWithMasterKey(
          //     body: {
          //       'till_timestamp': conversationData[0]['timestamp'].toString(),
          //       'till_msgid': conversationData[0]['msg-id'].toString(),
          //     },
          //     urlAddition: '/rtm/conversations/' +
          //         orderObject['conversation_id'] +
          //         '/messages');
          // setState(() {
          //   for (var i in newConversationData) {
          //     conversationData.insert(0, i);
          //   }
          // });
        }
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    var _body = orderObject == null && conversationData == null
        ? new Center(
            child: new PlatformCircularProgressIndicator(
              android: (_) => MaterialProgressIndicatorData(
                    backgroundColor: Color(0XFFFF2D55),
                  ),
              ios: (_) => CupertinoProgressIndicatorData(),
            ),
          )
        : new Column(
            children: <Widget>[
              new Container(
                height: 30.0,
                margin: new EdgeInsets.fromLTRB(20.0, 20.0, 20.0, 0.0),
                child: questionTitleView(),
              ),
              new Container(
                height: 40.0,
                margin: new EdgeInsets.fromLTRB(20.0, 20.0, 20.0, 0.0),
                child: userInfoView(),
              ),
              new Flexible(
                  flex: 1, fit: FlexFit.tight, child: getConversationWidget()),
              new Row(
                children: <Widget>[
                  new Container(
                    margin: new EdgeInsets.symmetric(horizontal: 4.0),
                    child: new IconButton(
                      //拍照
                      icon: new Icon(Icons.photo_camera), //拍照图标
                      onPressed: () =>
                          getImageFromCamera(), //触发发送消息事件执行的函数_handleSubmitted
                    ),
                  ),
                  new Container(
                    margin: new EdgeInsets.symmetric(horizontal: 4.0),
                    child: new IconButton(
                      //选择图片
                      icon: new Icon(Icons.image), //选择图片图标
                      onPressed: () =>
                          getImageFromGallery(), //触发发送消息事件执行的函数_handleSubmitted
                    ),
                  ),
                  new Flexible(
                    child: new TextField(
                      maxLines: 1,
                      cursorColor: Colors.white,
                      style: textStyle(
                          size: 16,
                          color: Colors.black,
                          fontWeight: FontWeight.normal),
                      controller: _inputController,
                      decoration: new InputDecoration(
                          hintText: "",
                          contentPadding:
                              const EdgeInsets.fromLTRB(5.0, 5.0, 5.0, 5.0),
                          border: new OutlineInputBorder(
                              borderRadius: const BorderRadius.all(
                                  const Radius.circular(10.0)),
                              borderSide: BorderSide(color: Colors.white))),
                    ),
                  ),
                  new Container(
                    margin: new EdgeInsets.symmetric(horizontal: 4.0),
                    child: new IconButton(
                        //发送按钮
                        icon: new Icon(Icons.send), //发送按钮图标
                        onPressed: () => _handleSubmitted(_inputController
                            .text)), //触发发送消息事件执行的函数_handleSubmitted
                  ),
                ],
              ),
            ],
          );

    return new PlatformScaffold(
      appBar: new PlatformAppBar(
        backgroundColor: Color(0xFFC62828),
        title: new Text("订单详情", style: new TextStyle(color: Colors.white)),
        android: (BuildContext context) => MaterialAppBarData(
              iconTheme: new IconThemeData(color: Colors.white),
            ),
        ios: (_) => CupertinoNavigationBarData(),
      ),
      body: _body,
    );
  }

  getImageFromGallery() async {
    String ref;
    var image = await ImagePicker.pickImage(source: ImageSource.gallery);
    Uri localFileUri = Uri.tryParse(image.uri.path);
    if (localFileUri != null) {
      print(localFileUri);
      await LeanCloudFile.upload(localFileUri)
          .then((onValue) => ref = onValue.toString())
          .catchError((e) => print(e));
      ref = '<img/>' + ref;
      // TODO: no master key
      // LeanCloudFunction cloudFunction = LeanCloudFunction();
      // await cloudFunction.postWithMasterKey(
      //   urlAddition: '/rtm/conversations/' +
      //       orderObject['conversation_id'] +
      //       '/messages',
      //   body: {'from_client': myId, 'message': ref},
      // );
      //var newConversationData = await cloudFunction.getWithMasterKey(
      //  body: {'reversed': 'true'},
      //  urlAddition: '/rtm/conversations/'+ orderObject['conversation_id'] +'/messages');
      //setState(() {
      //  conversationData = newConversationData;
      //});
    }
  }

  getImageFromCamera() async {
    String ref;
    var image = await ImagePicker.pickImage(source: ImageSource.camera);
    Uri localFileUri = Uri.tryParse(image.path);
    if (localFileUri != null) {
      print(localFileUri);
      await LeanCloudFile.upload(localFileUri)
          .then((onValue) => ref = onValue.toString())
          .catchError((e) => print(e));
      ref = '<img/>' + ref;
      LeanCloudFunction cloudFunction = LeanCloudFunction();
      // await cloudFunction.postWithMasterKey(
      //   urlAddition: '/rtm/conversations/' +
      //       orderObject['conversation_id'] +
      //       '/messages',
      //   body: {'from_client': myId, 'message': ref},
      // );
      // var newConversationData = await cloudFunction.getWithMasterKey(
      //   body: {'reversed': 'true'},
      //   urlAddition: '/rtm/conversations/'+ orderObject['conversation_id'] +'/messages');
      // setState(() {
      //   conversationData = newConversationData;
      // });
    }
  }

  _handleSubmitted(String text) async {
    // LeanCloudFunction cloudFunction = LeanCloudFunction();
    // await cloudFunction.postWithMasterKey(
    //   urlAddition:
    //       '/rtm/conversations/' + orderObject['conversation_id'] + '/messages',
    //   body: {'from_client': myId, 'message': text},
    // );
    // var newConversationData = await cloudFunction.getWithMasterKey(
    //   body: {'reversed': 'true'},
    //   urlAddition: '/rtm/conversations/'+ orderObject['conversation_id'] +'/messages');
    // setState(() {
    //   conversationData = newConversationData;
    // });
  }

  Widget getConversationWidget() {
    if (conversationData.length == 0) {
      return new Center(
        child: Text('快开始交流如何收货吧'),
      );
    } else {
      return new Container(
        margin: new EdgeInsets.fromLTRB(20.0, 20.0, 20.0, 0.0),
        child: new ListView.builder(
          itemCount: conversationData.length,
          itemBuilder: renderOrderItem,
          controller: _controller,
          reverse: true,
        ),
      );
    }
  }

  Widget renderOrderItem(BuildContext context, int i) {
    return _renderOrderRow(context, i);
  }

  _renderOrderRow(context, i) {
    var listItem = conversationData[i];
    var row = new Container();
    if (listItem['from'] == myId) {
      String userImageUrl = orderObject['buyer_image']['url'].toString();
      String content = listItem['data'].toString();
      row = new Container(
        margin: const EdgeInsets.fromLTRB(0.0, 20.0, 0.0, 20.0),
        child: new Column(
          children: <Widget>[
            new Row(
              children: <Widget>[
                new Flexible(
                  flex: 1,
                  fit: FlexFit.tight,
                  child: new Container(),
                ),
                new Container(
                  color: Colors.white,
                  margin: EdgeInsets.only(right: 10.0),
                  padding: EdgeInsets.all(10.0),
                  child: textOrImage(content),
                ),
                //放用户图片
                new Container(
                  width: 40.0,
                  height: 40.0,
                  decoration: new BoxDecoration(
                    image: new DecorationImage(
                        image: new NetworkImage(userImageUrl)),
                    borderRadius: new BorderRadius.all(
                      const Radius.circular(20.0),
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
      );
    } else {
      String userImageUrl = orderObject['seller_image']['url'].toString();
      String content = listItem['data'].toString();
      row = new Container(
        margin: const EdgeInsets.fromLTRB(0.0, 20.0, 0.0, 20.0),
        child: new Column(
          children: <Widget>[
            new Row(
              children: <Widget>[
                //放用户图片
                new Container(
                  width: 40.0,
                  height: 40.0,
                  decoration: new BoxDecoration(
                    image: new DecorationImage(
                        image: new NetworkImage(userImageUrl)),
                    borderRadius: new BorderRadius.all(
                      const Radius.circular(20.0),
                    ),
                  ),
                ),
                new Flexible(
                  flex: 1,
                  fit: FlexFit.tight,
                  child: new Container(),
                ),
                new Container(
                  color: Colors.white,
                  margin: EdgeInsets.only(right: 10.0),
                  padding: EdgeInsets.all(10.0),
                  child: textOrImage(content),
                ),
              ],
            ),
          ],
        ),
      );
    }
    return new Builder(
      builder: (ctx) {
        return new InkWell(
          child: row,
        );
      },
    );
  }

  Widget textOrImage(String content) {
    if (content.startsWith('<img/>')) {
      return Container(
        height: 200.0,
        width: 200.0,
        child: Image(
          fit: BoxFit.scaleDown,
          image: NetworkImage(content.replaceAll('<img/>', '')),
        ),
      );
    } else {
      return Text(
        content,
        style: textStyle(
            size: 16, color: Colors.black, fontWeight: FontWeight.w500),
      );
    }
  }

  //用户有关的Widget
  Widget userInfoView() {
    return new Container(
        color: const Color(0xfffcfcfc),
        child: new Row(
          children: <Widget>[
            //放用户图片
            new Container(
              width: 40.0,
              height: 40.0,
              decoration: new BoxDecoration(
                image: new DecorationImage(
                    image: new NetworkImage((myId == orderObject['buyer_id'])
                        ? orderObject['seller_image']['url']
                        : orderObject['buyer_image']['url'])),
                borderRadius: new BorderRadius.all(
                  const Radius.circular(20.0),
                ),
              ),
            ),
            //问题标题
            new Flexible(
                flex: 1,
                fit: FlexFit.tight,
                child: new Container(
                  margin: new EdgeInsets.fromLTRB(10.0, 0.0, 0.0, 0.0),
                  child: new Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      new Text(
                        (myId == orderObject['buyer_id'])
                            ? ('卖方: ' + orderObject['seller_name'])
                            : ('买方: ' + orderObject['buyer_name']),
                        style: this.textStyle(
                            size: 14,
                            color: Color(0xFF818181),
                            fontWeight: FontWeight.w600),
                      ),
                      new Text(
                        '交易完成于' +
                            getDate(DateTime.parse(
                                orderObject['createdAt']['iso'])),
                        style: this.textStyle(
                            size: 14,
                            color: Color(0xFF818181),
                            fontWeight: FontWeight.w600),
                      ),
                    ],
                  ),
                )),
            priceWidget(),
          ],
        ));
  }

  Widget questionTitleView() {
    return new Container(
      color: const Color(0xfffcfcfc),
      child: new Row(
        children: <Widget>[
          //红点
          new Container(
            width: 8.0,
            height: 20.0,
            margin: new EdgeInsets.fromLTRB(0.0, 5.0, 10.0, 5.0),
            decoration: new BoxDecoration(
                color: Color(0XFFFF2D55),
                borderRadius: new BorderRadius.all(const Radius.circular(3.5))),
          ),
          //交易标题
          new Flexible(
            flex: 1,
            fit: FlexFit.tight,
            child: new Text(
              orderObject['order_title'],
              style: this.textStyle(
                  size: 18, color: Colors.black, fontWeight: FontWeight.w600),
            ),
          ),
          new Container(
            child: FlatButton(
              onPressed: () {
                LeanCloudFunction.callFunction(myData: {
                  'order_id': orderId,
                  'seller_alipay_account': 'xcffl@outlook.com',
                  'price': orderObject['price']
                }, functionName: 'alipay_trans');
              },
              child: Text('确认收货'),
            ),
          ),
        ],
      ),
    );
  }

  //用的是测试的变量，日后需要稍作调整
  Widget priceWidget() {
    return new Container(
        height: 40.0,
        padding: EdgeInsets.fromLTRB(0.0, 5.0, 0.0, 5.0),
        child: new Text(
          orderObject['price'].toString() + "¥",
          style: textStyle(
              size: 30, color: Color(0XFF263238), fontWeight: FontWeight.w600),
        ));
  }

  /*
  void sendReply(authorId) {
    String replyStr = _inputController.text;
    if (replyStr == null || replyStr.length == 0 || replyStr.trim().length == 0) {
      return;
    } else {
      DataUtils.isLogin().then((isLogin) {
        if (isLogin) {
          DataUtils.getAccessToken().then((token) {
            Map<String, String> params = new Map();
            params['access_token'] = token;
            params['id'] = "${tweetData['id']}";
            print("id: ${tweetData['id']}");
            params['catalog'] = "3";
            params['content'] = replyStr;
            params['authorid'] = "$authorId";
            print("authorId: $authorId");
            params['isPostToMyZone'] = "0";
            params['dataType'] = "json";
            NetUtils.get(Api.COMMENT_REPLY, params: params).then((data) {
              if (data != null) {
                var obj = json.decode(data);
                var error = obj['error'];
                if (error != null && error == '200') {
                  // 回复成功
                  Navigator.of(context).pop();
                  getReply(false);
                }
              }
            });
          });
        }
      });
    }
  }*/

}
