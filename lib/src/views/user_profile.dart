import 'package:flutter/material.dart';
import 'package:incrementally_loading_listview/incrementally_loading_listview.dart';
import 'package:most/src/models/search_info.dart';
import 'package:most/src/models/user_info.dart';
import 'package:most/src/widgets/coupons.dart';
import 'package:most/src/widgets/modern_card.dart';
import 'package:transparent_image/transparent_image.dart';
import 'dart:async';
import '../common/repository.dart';

/* TODO: See https://github.com/CodemateLtd/FlutterMates */

class _ProfileTabsPage extends StatefulWidget {
  final String id; // TODO: check validation
  final userInfoType type;
  String get label {
    switch (type) {
      case userInfoType.post:
        return '我的发帖';
      case userInfoType.favorite:
        return '收藏';
      case userInfoType.history:
        return '浏览历史';
      case userInfoType.transaction:
        return '交易记录';
      case userInfoType.followed:
        return '我关注的';
      case userInfoType.followers:
        return '我的粉丝';
      case userInfoType.goods:
        return '我的宝贝';
      case userInfoType.coupons:
        return '卡券';
      default:
        return '未知'; // Error
    }
  }

  const _ProfileTabsPage({Key key, @required this.id, @required this.type})
      : assert(type != null),
        assert(id != null),
        super(key: key);

  @override
  __ProfileTabsPageState createState() => __ProfileTabsPageState();
}

class __ProfileTabsPageState extends State<_ProfileTabsPage> {
  // Data

  List<PostSearchResultEntry> data;
  bool _hasMoreItems;
  static const int _loadMoreOffsetFromBottom = 5;
  static const int _initialLoadQuantity = 10;

  Future<List<PostSearchResultEntry>> moreDataFrom(int start, int end) async {
    switch (widget.type) {
      case userInfoType.post:
        return await Repository.get().thisUserPosts;
      // case userInfoType.favorite:
      //   tabView = Container();
      //   break;
      // case userInfoType.history:
      //   tabView = Container();
      //   break;
      case userInfoType.transaction:
        return await Repository.get().thisUserTransactions;
      // case userInfoType.followed:
      //   tabView = Container();
      //   break;
      // case userInfoType.followers:
      //   tabView = Container();
      //   break;
      // case userInfoType.goods:
      //   tabView = Container();
      //   break;
      case userInfoType.coupons:
        break;

      default:
        break;
    }
  }

  Future<void> _initData() async {
    if (data != null) _hasMoreItems = data.length >= _loadMoreOffsetFromBottom;
  }

  Future _loadMoreItems() async {
    final _data = await Repository.get().thisUserTransactions;

    // _hasMoreItems = data.length < _maxItems;
  }

  // View

  @override
  Widget build(BuildContext context) {
    // 外接 coupons tab
    if (widget.type == userInfoType.coupons)
      return CouponsTab(userId: widget.id);
    // If data is not loaded
    if (data == null)
      return Center(
        child: CircularProgressIndicator(),
      );
    // // If data is loaded
    // return FutureBuilder(
    //       future: _initData(),
    //       builder: (context, snapshot) {
    //         switch (snapshot.connectionState) {
    //           case ConnectionState.waiting:
    //             return Center(child: CircularProgressIndicator());
    //           case ConnectionState.done:
    //             return IncrementallyLoadingListView(
    //               hasMore: () => _hasMoreItems,
    //               itemCount: () => data.length,
    //               loadMore: () async {
    //                 // can shorten to "loadMore: _loadMoreItems" but this syntax is used to demonstrate that
    //                 // functions with parameters can also be invoked if needed
    //                 await _loadMoreItems();
    //               },
    //               onLoadMore: () {
    //                 setState(() {
    //                   _loadingMore = true;
    //                 });
    //               },
    //               onLoadMoreFinished: () {
    //                 setState(() {
    //                   _loadingMore = false;
    //                 });
    //               },
    //               loadMoreOffsetFromBottom: _loadMoreOffsetFromBottom,
    //               itemBuilder: (context, index) {
    //                 final item = data[index];
    //                 if ((_loadingMore ?? false) && index == items.length - 1) {
    //                   return Column(
    //                     children: <Widget>[
    //                       InfoTile(item: item),
    //                       Card(
    //                         child: Padding(
    //                           padding: const EdgeInsets.all(16.0),
    //                           child: Column(
    //                             children: <Widget>[
    //                               Row(
    //                                 crossAxisAlignment:
    //                                     CrossAxisAlignment.start,
    //                                 children: <Widget>[
    //                                   Container(
    //                                     width: 60.0,
    //                                     height: 60.0,
    //                                     color: Colors.grey,
    //                                   ),
    //                                   Padding(
    //                                     padding: const EdgeInsets.fromLTRB(
    //                                         8.0, 0.0, 0.0, 0.0),
    //                                     child: Container(
    //                                       color: Colors.grey,
    //                                       child: Text(
    //                                         item.name,
    //                                         style: TextStyle(
    //                                             color: Colors.transparent),
    //                                       ),
    //                                     ),
    //                                   )
    //                                 ],
    //                               ),
    //                               Padding(
    //                                 padding: const EdgeInsets.fromLTRB(
    //                                     0.0, 8.0, 0.0, 0.0),
    //                                 child: Container(
    //                                   color: Colors.grey,
    //                                   child: Text(
    //                                     item.message,
    //                                     style: TextStyle(
    //                                         color: Colors.transparent),
    //                                   ),
    //                                 ),
    //                               )
    //                             ],
    //                           ),
    //                         ),
    //                       ),
    //                     ],
    //                   );
    //                 }
    //                 return new InfoCard(item: item);
    //               },
    //             );
    //           default:
    //             return Text('Something went wrong');
    //         }
    //       }),
    // );
  }
}

class PostTab extends StatefulWidget {
  final String userObjectId;
  const PostTab({
    Key key,
    this.userObjectId,
  }) : super(key: key);

  @override
  PostTabState createState() {
    return new PostTabState();
  }
}

class PostTabState extends State<PostTab> {
  @override
  Widget build(BuildContext context) {
    return Container();
  }
}

/// Shows as "My" page as well as others' profile page.
/// When [isThisUser] is set to true, it is illustrated as "My" page.
class UserProfile extends StatefulWidget {
  UserProfile({Key key, this.id, this.isThisUser = false}) : super(key: key);
  final String id; // TODO: check validation
  final bool isThisUser;

  _UserProfileState createState() => _UserProfileState();
}

class _UserProfileState extends State<UserProfile>
    with SingleTickerProviderStateMixin {
  TabController _controller;

  UserInfo info;
  String username;

  List<_ProfileTabsPage> _profileTabsPages;

  Future _initUserProfile() async {
    final String _username = await Repository.get().thisUsername;
    if (this.mounted) {
      setState(() {
        username = _username;
      });
    }

    final thisUserInfo = widget.isThisUser
        ? await Repository.get().thisUserInfo
        : await Repository.get().userInfoOf(widget.id);
    if (this.mounted) {
      setState(() {
        info = thisUserInfo;
      });
    }
  }

  @override
  void initState() {
    super.initState();

    _initUserProfile();

    if (widget.id != null) {
      // ensure id is not null
      _profileTabsPages = <_ProfileTabsPage>[
        new _ProfileTabsPage(type: userInfoType.post, id: widget.id),
        new _ProfileTabsPage(type: userInfoType.goods, id: widget.id),
        // new _ProfileTabsPage(type: userInfoType.favorite, id: widget.id),
        // new _ProfileTabsPage(type: userInfoType.history, id: widget.id),
        // new _ProfileTabsPage(type: userInfoType.followed, id: widget.id),
        // new _ProfileTabsPage(type: userInfoType.followers, id: widget.id),
        new _ProfileTabsPage(type: userInfoType.coupons, id: widget.id),
      ];
      _controller =
          new TabController(vsync: this, length: _profileTabsPages.length);
    }
  }

  @override
  void dispose() {
    _profileTabsPages.clear();
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    // If no id, do nothing.
    if (widget.id == null)
      return Center(
        child: CircularProgressIndicator(
          // strokeWidth: 4.0,
          // backgroundColor: Colors.blue,
          // value: 0.2,
          valueColor: new AlwaysStoppedAnimation<Color>(Colors.red),
        ),
      );

    final actions = <Widget>[
      // IconButton(
      //   onPressed: () {},
      //   icon: Icon(Icons.feedback),
      // ),
      // IconButton(
      //     onPressed: () {},
      //     icon: Icon(
      //       Icons.help,
      //     )),
      // Only show settings button when it's this user's page.
      widget.isThisUser
          ? IconButton(
              onPressed: () => Navigator.of(context).pushNamed('/profile/edit'),
              icon: Icon(
                Icons.edit,
              ))
          : Container(),
      widget.isThisUser
          ? IconButton(
              onPressed: () => Navigator.of(context).pushNamed('/settings'),
              icon: Icon(
                Icons.settings,
              ))
          : Container(),
      // overflow menu
      // PopupMenuButton(
      //   // onSelected: _select,
      //   itemBuilder: (BuildContext context) {
      //     return [new PopupMenuItem(child: Text('测试'))];
      //   },
      // ),
    ];

    // TODO: implement build
    return new Scaffold(
        appBar: new AppBar(
          automaticallyImplyLeading: false,
          title: FlatButton(
            onPressed: () => Navigator.of(context).pushNamed('/profile/edit'),
            child: new Row(
              children: <Widget>[
                new Container(
                  padding: const EdgeInsets.fromLTRB(0.0, 16.0, 8.0, 16.0),
                  child: new CircleAvatar(
                    radius: 12.0,
                    backgroundImage: info != null
                        ? new NetworkImage(info?.avatar?.uri?.toString())
                        : AssetImage(Repository.defaultfAvatarUri),
                  ),
                ),
                LimitedBox(
                  child: new Text(
                    (info?.username ?? username) ??
                        '', // Use local stored username to speed up illustration
                    overflow: TextOverflow.ellipsis,
                    style: Theme.of(context)
                        .textTheme
                        .title
                        .copyWith(fontWeight: FontWeight.bold),
                  ),
                  maxWidth: 64.0,
                )
              ],
            ),
          ),
          actions: actions,
          bottom: new TabBar(
            isScrollable: true,
            controller: _controller,
            tabs: _profileTabsPages
                .map((_ProfileTabsPage page) =>
                    new Tab(text: page.label.toUpperCase()))
                .toList(),
          ),
//          subtitle: new Container(
////            margin: const EdgeInsets.only(top: 2.0),
//            child: new Text("未来世界的幸存者"),
//
//          ),

          // leading: new Container(
          //   child: new CircleAvatar(
          //       backgroundImage: new NetworkImage(
          //           "https://pic1.zhimg.com/v2-ec7ed574da66e1b495fcad2cc3d71cb9_xl.jpg"),
          //       radius: 20.0),
          // ),
          // title: new ConstrainedBox(
          //   constraints: BoxConstraints.loose(
          //       Size.fromHeight(kToolbarHeight - kTabLabelPadding.top)),
          //   child: new ListTile(
          //     contentPadding: const EdgeInsets.all(0.0),
          //     leading: new Container(
          //       child: new CircleAvatar(
          //           backgroundImage: new NetworkImage(
          //               "https://pic1.zhimg.com/v2-ec7ed574da66e1b495fcad2cc3d71cb9_xl.jpg"),
          //           radius: 20.0),
          //     ),
          //     title: new Container(
          //       margin: const EdgeInsets.only(bottom: 2.0),
          //       child: new Text("learner"),
          //     ),
          //     subtitle: new Container(
          //       margin: const EdgeInsets.only(top: 2.0),
          //       child: new Text("未来世界的幸存者"),
          //     ),
          //   ),
          // ),
        ),
        body: TabBarView(controller: _controller, children: _profileTabsPages));
  }

  // static const appBarInitialHeight = 400.0;
  // @override
  // Widget build(BuildContext context) {
  //   // TODO: implement build
  //   return new Scaffold(
  //     body: new Builder(
  //       builder: (context) => new SliverFab(
  //             floatingActionButton: new FloatingActionButton(
  //               heroTag: 'main_fab',
  //               onPressed: () => Scaffold.of(context).showSnackBar(
  //                   new SnackBar(content: new Text("You clicked FAB!"))),
  //               child: new Icon(Icons.edit),
  //             ),
  //             expandedHeight: appBarInitialHeight,
  //             topScalingEdge: appBarInitialHeight * 0.6,
  //             slivers: <Widget>[
  //               new SliverAppBar(
  //                 expandedHeight: appBarInitialHeight,
  //                 // bottom: new UserProfileCard(),
  //                 pinned: true,
  //                 // floating: true,
  //                 // flexibleSpace:
  //                 flexibleSpace: new FlexibleSpaceBar(
  //                   title: Text('我的'),
  //                   centerTitle: true,
  //                   background: new Card(
  //                     child: new Column(
  //                       children: <Widget>[
  //                         FadeInImage.memoryNetwork(
  //                           fit: BoxFit.cover,
  //                           placeholder: kTransparentImage,
  //                           image:
  //                               "https://images.unsplash.com/photo-1529629933013-5454ef3ca25e",
  //                         ),
  //                         // This gradient ensures that the toolbar icons are distinct
  //                         // against the background image.
  //                         const DecoratedBox(
  //                           decoration: const BoxDecoration(
  //                             gradient: const LinearGradient(
  //                               begin: const Alignment(0.0, 1.0),
  //                               end: const Alignment(0.0, -1.0),
  //                               colors: const <Color>[
  //                                 const Color(0x60000000),
  //                                 const Color(0x00000000)
  //                               ],
  //                             ),
  //                           ),
  //                         ),
  //                       ],
  //                     ),
  //                   ),
  //                 ),
  //                 bottom: new PreferredSize(child: new CoreProfileCard(),preferredSize: Size.,
  //               ),
  //               // bottom: new Text("查看或编辑个人主页"),
  //               // background: new UserProfileCard(),
  //               new SliverList(
  //                 delegate: new SliverChildListDelegate(
  //                   ,
  //                 ),
  //               ),
  //             ],
  //           ),
  //     ),
  //   );
}

class Choice {
  const Choice({this.title, this.icon});

  final String title;
  final IconData icon;
}

class CoreProfileCard extends StatelessWidget {
  const CoreProfileCard({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return new Card(
      margin: EdgeInsets.all(16.0),
      child: Column(
        children: <Widget>[
          // new Stack(
          //   children: <Widget>[
          //     FadeInImage.memoryNetwork(
          //       fit: BoxFit.cover,
          //       placeholder: kTransparentImage,
          //       image:
          //           "https://images.unsplash.com/photo-1529629933013-5454ef3ca25e",
          //     ),
          //     // This gradient ensures that the toolbar icons are distinct
          //     // against the background image.
          //     const DecoratedBox(
          //       decoration: const BoxDecoration(
          //         gradient: const LinearGradient(
          //           begin: const Alignment(0.0, 1.0),
          //           end: const Alignment(0.0, -1.0),
          //           colors: const <Color>[
          //             const Color(0x60000000),
          //             const Color(0x00000000)
          //           ],
          //         ),
          //       ),
          //     ),
          //   ],
          // ),
          new ListTile(
            leading: new Container(
              child: new CircleAvatar(
                  backgroundImage: new NetworkImage(
                      "https://pic1.zhimg.com/v2-ec7ed574da66e1b495fcad2cc3d71cb9_xl.jpg"),
                  radius: 20.0),
            ),
            title: new Container(
              margin: const EdgeInsets.only(bottom: 2.0),
              child: new Text("learner"),
            ),
            subtitle: new Container(
              margin: const EdgeInsets.only(top: 2.0),
              child: new Text("未来世界的幸存者"),
            ),
          ),
          new ButtonTheme.bar(
            padding: EdgeInsets.only(bottom: 8.0),
            child: new ButtonBar(
              children: <Widget>[
                new FlatButton(
                  child: const Text('编辑'),
                  onPressed: () {}, // TODO: Impelement profile edit
                ),
                new FlatButton(
                  child: const Text('测试'),
                  onPressed: () {}, // TODO: Impelement profile edit
                ),
              ],
            ),
          ),
          const Divider(),
          new ButtonTheme.bar(
            // make buttons use the appropriate styles for cards
            child: new ButtonBar(
              alignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                // new FlatButton(
                //     shape: CircleBorder(),
                //     onPressed: () {},
                //     child: new Container(
                //       height: 50.0,
                //       child: new Column(
                //         children: <Widget>[
                //           new Text(
                //             "18",
                //             style: new TextStyle(
                //                 fontSize: 16.0, color: GlobalConfig.fontColor),
                //           ),
                //           new Text(
                //             "收藏",
                //             style: new TextStyle(
                //                 fontSize: 12.0, color: GlobalConfig.fontColor),
                //           ),
                //         ],
                //       ),
                //     )),
                // new FlatButton(
                //     shape: CircleBorder(),
                //     onPressed: () {},
                //     child: new Container(
                //       height: 50.0,
                //       child: new Column(
                //         children: <Widget>[
                //           new Text(
                //             "210",
                //             style: new TextStyle(
                //                 fontSize: 16.0, color: GlobalConfig.fontColor),
                //           ),
                //           new Text(
                //             "关注",
                //             style: new TextStyle(
                //                 fontSize: 12.0, color: GlobalConfig.fontColor),
                //           ),
                //         ],
                //       ),
                //     )),
                // new FlatButton(
                //     shape: CircleBorder(),
                //     onPressed: () {},
                //     child: new Container(
                //       height: 50.0,
                //       child: new Column(
                //         children: <Widget>[
                //           new Text(
                //             "52",
                //             style: new TextStyle(
                //                 fontSize: 16.0, color: GlobalConfig.fontColor),
                //           ),
                //           new Text(
                //             "粉丝",
                //             style: new TextStyle(
                //                 fontSize: 12.0, color: GlobalConfig.fontColor),
                //           ),
                //         ],
                //       ),
                //     )),
                new FlatButton(
                    onPressed: () {},
                    child: new Container(
                      height: 50.0,
                      child: new Column(
                        children: <Widget>[
                          new Text(
                            "30",
                            style: new TextStyle(
                                fontSize: 16.0, color: GlobalConfig.fontColor),
                          ),
                          new Text(
                            "发帖",
                            style: new TextStyle(
                                fontSize: 12.0, color: GlobalConfig.fontColor),
                          ),
                        ],
                      ),
                    )),
                new FlatButton(
                    onPressed: () {},
                    child: new Container(
                      height: 50.0,
                      child: new Column(
                        children: <Widget>[
                          new Text(
                            "5",
                            style: new TextStyle(
                                fontSize: 16.0, color: GlobalConfig.fontColor),
                          ),
                          new Text(
                            "交易",
                            style: new TextStyle(
                                fontSize: 12.0, color: GlobalConfig.fontColor),
                          ),
                        ],
                      ),
                    )),
                new FlatButton(
                    onPressed: () {},
                    child: new Container(
                      height: 50.0,
                      child: new Column(
                        children: <Widget>[
                          new Text(
                            "5",
                            style: new TextStyle(
                                fontSize: 16.0, color: GlobalConfig.fontColor),
                          ),
                          new Text(
                            "卡券",
                            style: new TextStyle(
                                fontSize: 12.0, color: GlobalConfig.fontColor),
                          ),
                        ],
                      ),
                    )),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

// class UserProfileCard extends StatefulWidget {
//   _UserProfileStateCard createState() => _UserProfileStateCard();
// }

// class _UserProfileStateCard extends State<UserProfileCard> {
//   @override
//   Widget build(BuildContext context) {
//     // TODO: implement build
//     return new Container(
//       color: Theme.of(context).cardColor,
//       margin: const EdgeInsets.only(top: 10.0, bottom: 6.0),
//       padding: const EdgeInsets.only(top: 12.0, bottom: 8.0),
//       child: new Column(
//         children: <Widget>[
//           new Container(
//             margin:
//                 const EdgeInsets.only(left: 16.0, right: 16.0, bottom: 16.0),
//             decoration: new BoxDecoration(
//                 color: GlobalConfig.dark == true
//                     ? Colors.white10
//                     : new Color(0xFFF5F5F5),
//                 borderRadius: new BorderRadius.all(new Radius.circular(6.0))),
//             child: new FlatButton(
//                 onPressed: () {},
//                 child: new Container(
//                   child: new ListTile(
//                     leading: new Container(
//                       child: new CircleAvatar(
//                           backgroundImage: new NetworkImage(
//                               "https://pic1.zhimg.com/v2-ec7ed574da66e1b495fcad2cc3d71cb9_xl.jpg"),
//                           radius: 20.0),
//                     ),
//                     title: new Container(
//                       margin: const EdgeInsets.only(bottom: 2.0),
//                       child: new Text("learner"),
//                     ),
//                     subtitle: new Container(
//                       margin: const EdgeInsets.only(top: 2.0),
//                       child: new Text("未来世界的幸存者"),
//                     ),
//                   ),
//                 )),
//           ),
//           new Container(
//             child: new Row(
//               mainAxisAlignment: MainAxisAlignment.center,
//               children: <Widget>[
//                 new Container(
//                   width: (MediaQuery.of(context).size.width - 6.0) / 4,
//                   child: new FlatButton(
//                       onPressed: () {},
//                       child: new Container(
//                         height: 50.0,
//                         child: new Column(
//                           children: <Widget>[
//                             new Container(
//                               child: new Text(
//                                 "57",
//                                 style: new TextStyle(
//                                     fontSize: 16.0,
//                                     color: GlobalConfig.fontColor),
//                               ),
//                             ),
//                             new Container(
//                               child: new Text(
//                                 "我的创作",
//                                 style: new TextStyle(
//                                     fontSize: 12.0,
//                                     color: GlobalConfig.fontColor),
//                               ),
//                             ),
//                           ],
//                         ),
//                       )),
//                 ),
//                 new Container(
//                   height: 14.0,
//                   width: 1.0,
//                   decoration: new BoxDecoration(
//                       border: new BorderDirectional(
//                           start: new BorderSide(
//                               color: GlobalConfig.dark == true
//                                   ? Colors.white12
//                                   : Colors.black12,
//                               width: 1.0))),
//                 ),
//                 new Container(
//                   width: (MediaQuery.of(context).size.width - 6.0) / 4,
//                   child: new FlatButton(
//                       onPressed: () {},
//                       child: new Container(
//                         height: 50.0,
//                         child: new Column(
//                           children: <Widget>[
//                             new Container(
//                               child: new Text(
//                                 "210",
//                                 style: new TextStyle(
//                                     fontSize: 16.0,
//                                     color: GlobalConfig.fontColor),
//                               ),
//                             ),
//                             new Container(
//                               child: new Text(
//                                 "关注",
//                                 style: new TextStyle(
//                                     fontSize: 12.0,
//                                     color: GlobalConfig.fontColor),
//                               ),
//                             )
//                           ],
//                         ),
//                       )),
//                 ),
//                 new Container(
//                   height: 14.0,
//                   width: 1.0,
//                   decoration: new BoxDecoration(
//                       border: new BorderDirectional(
//                           start: new BorderSide(
//                               color: GlobalConfig.dark == true
//                                   ? Colors.white12
//                                   : Colors.black12,
//                               width: 1.0))),
//                 ),
//                 new Container(
//                   width: (MediaQuery.of(context).size.width - 6.0) / 4,
//                   child: new FlatButton(
//                       onPressed: () {},
//                       child: new Container(
//                         height: 50.0,
//                         child: new Column(
//                           children: <Widget>[
//                             new Container(
//                               child: new Text(
//                                 "18",
//                                 style: new TextStyle(
//                                     fontSize: 16.0,
//                                     color: GlobalConfig.fontColor),
//                               ),
//                             ),
//                             new Container(
//                               child: new Text(
//                                 "我的收藏",
//                                 style: new TextStyle(
//                                     fontSize: 12.0,
//                                     color: GlobalConfig.fontColor),
//                               ),
//                             )
//                           ],
//                         ),
//                       )),
//                 ),
//                 new Container(
//                   height: 14.0,
//                   width: 1.0,
//                   decoration: new BoxDecoration(
//                       border: new BorderDirectional(
//                           start: new BorderSide(
//                               color: GlobalConfig.dark == true
//                                   ? Colors.white12
//                                   : Colors.black12,
//                               width: 1.0))),
//                 ),
//                 new Container(
//                     width: (MediaQuery.of(context).size.width - 6.0) / 4,
//                     child: new FlatButton(
//                         onPressed: () {},
//                         child: new Container(
//                           height: 50.0,
//                           child: new Column(
//                             children: <Widget>[
//                               new Container(
//                                 child: new Text(
//                                   "33",
//                                   style: new TextStyle(
//                                       fontSize: 16.0,
//                                       color: GlobalConfig.fontColor),
//                                 ),
//                               ),
//                               new Container(
//                                 child: new Text(
//                                   "最近浏览",
//                                   style: new TextStyle(
//                                       fontSize: 12.0,
//                                       color: GlobalConfig.fontColor),
//                                 ),
//                               )
//                             ],
//                           ),
//                         )))
//               ],
//             ),
//           )
//         ],
//       ),
//     );
//   }
// }

class GlobalConfig {
  static bool dark = false;
  static ThemeData themeData = new ThemeData.light();
  static Color searchBackgroundColor = new Color(0xFFEBEBEB);
  static Color cardBackgroundColor = Colors.white;
  static Color fontColor = Colors.black54;
}

class TransactionsPage extends StatefulWidget {
  @override
  _TransactionsPageState createState() => _TransactionsPageState();
}

class _TransactionsPageState extends State<TransactionsPage> {
  @override
  Widget build(BuildContext context) {
    return Container();
  }
}
