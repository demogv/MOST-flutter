//该类为问答大类点开话题后父类和子类的界面
import 'package:flutter/material.dart';
import 'package:flutter_platform_widgets/flutter_platform_widgets.dart';
import 'package:most/src/api/leancloud.dart';
import 'package:most/src/widgets/card.dart';
import 'package:most/src/widgets/image_text_widget.dart';

//import 'package:flutter_search_bar/flutter_search_bar.dart';

class QAPage extends StatefulWidget {
  final int fatherGroup;
  final int subGroup;
  final String titleName;

  QAPage({Key key, this.fatherGroup, this.subGroup, this.titleName})
      : super(key: key);
  @override
  State<StatefulWidget> createState() {
    return new QAPageState(
        fatherGroup: fatherGroup, subGroup: subGroup, titleName: titleName);
  }
}

class QAPageState extends State<QAPage> {
  int fatherGroup;
  int subGroup;
  String titleName;

  //用来测试用的数据
  List latestList = [3, 2, 1, 4];

  List<String> fatherGroupName = ['学术中心', '个人发展', '校园指南', '南腔北调'];
  List groupInfo;
  List latestGroupInfo;
  List dataQ1;
  List dataQ2;
  List dataA1;
  //RegExp regExp1 = new RegExp("</.*>");
  //RegExp regExp2 = new RegExp("<.*>");
  TextStyle subtitleStyle =
      new TextStyle(fontSize: 12.0, color: const Color(0xFFB5BDC0));
  TextStyle contentStyle = new TextStyle(fontSize: 15.0, color: Colors.black);
  num curPage = 1;

  QAPageState({Key key, this.fatherGroup, this.subGroup, this.titleName});

  //设置字体
  textStyle({int size, Color color, FontWeight fontWeight}) {
    return new TextStyle(
      fontSize: size.toDouble(),
      decoration: TextDecoration.none,
      color: color,
      fontWeight: fontWeight,
    );
  }

  setData() async {
    print(123);
    var newGroupData = await LeanCloudFunction.query(
        body: {'where': '{"father_group":' + fatherGroup.toString() + '}'},
        className: '/Group_info_QA');
    print(111);
    var newDataQ1 = await LeanCloudFunction.callFunction(myData: {
      'father_group': fatherGroup,
      'sub_group': subGroup,
      'start': 0,
      'end': 3
    }, functionName: 'recommend_QA');
    print(222);
    var newDataQ2 = await LeanCloudFunction.callFunction(myData: {
      'father_group': fatherGroup,
      'sub_group': subGroup,
      'start': 0,
      'end': 3
    }, functionName: 'new_QA');
    print(333);
    var newDataA1 = await LeanCloudFunction.callFunction(myData: {
      'father_group': fatherGroup,
      'sub_group': subGroup,
      'start': 0,
      'end': 3
    }, functionName: 'recommend_answer');
    List newLatestGroupInfo = [];
    for (int i = 0; i < latestList.length; i++) {
      for (var j in newGroupData) {
        if (j['sub_group'] == latestList[i]) {
          newLatestGroupInfo.add(j);
          break;
        }
      }
    }
    if (this.mounted) {
      // ensure the object is still in the tree.
      this.setState(() {
        latestGroupInfo = newLatestGroupInfo;
        groupInfo = newGroupData;
        dataQ1 = newDataQ1;
        dataQ2 = newDataQ2;
        dataA1 = newDataA1;
      });
    }
  }

  @override
  void initState() {
    super.initState();
    setData();
  }

  @override
  Widget build(BuildContext context) {
    var _body = (latestGroupInfo == null &&
            dataQ1 == null &&
            dataQ2 == null &&
            dataA1 == null)
        ? new Center(
            child: PlatformCircularProgressIndicator(
              android: (_) => MaterialProgressIndicatorData(
                    backgroundColor: Color(0XFFFF2D55),
                  ),
              ios: (_) => CupertinoProgressIndicatorData(),
            ),
          )
        : new ListView(
            children: <Widget>[
              new Material(
                color: Colors.transparent,
                child: groupWidget(),
              ),
              new MyCard(
                cardData: dataQ1,
                cardTitle: '热门问题',
                isFirstCard: true,
                numberOfLines: 4,
              ),
              new MyCard(
                cardData: dataQ2,
                cardTitle: '最新问题',
                isFirstCard: false,
                numberOfLines: 4,
              ),
              new MyCard(
                cardData: dataA1,
                cardTitle: '热门回答',
                isFirstCard: false,
                numberOfLines: 4,
              ),
            ],
          );

    return new PlatformScaffold(
      appBar: new PlatformAppBar(
        backgroundColor: Colors.white,
        title: new Text(
          titleName,
        ),
        // android: (BuildContext context) => MaterialAppBarData(
        //       iconTheme: new IconThemeData(color: Colors.white),
        //     ),
        // ios: (_) => CupertinoNavigationBarData(),
        //actions: <Widget>[new SearchButton()],
      ),
      // appBar: buildAppBar(context),
      body: _body,
      //floatingActionButton: AddPostFAB(),
      //floatingActionButtonLocation:
      //  fabMenuLocation, // Remember to set floatingActionButtonLocation to fabMenuLocation')
    );
  }

  Iterable<Widget> get subGroupWidgets sync* {
    for (var data in groupInfo) {
      yield new Padding(
          padding: const EdgeInsets.all(8.0),
          child: new GestureDetector(
            onTap: () {
              Navigator.of(context).push(new MaterialPageRoute(builder: (ctx) {
                return new QAPage(
                  fatherGroup: fatherGroup,
                  subGroup: data['sub_group'],
                  titleName: data['group_title'],
                );
              }));
            },
            child: ImageTextCard(
              imageStirng: data['icon_image']['url'].toString(),
              isNetImage: true,
              title: data['group_title'].toString(),
            ),
          ));
    }
  }

  Widget groupWidget() {
    if (subGroup == 0) {
      return new Container(
        height: 64.0,
        margin: EdgeInsets.fromLTRB(0.0, 20.0, 0.0, 0.0),
        child: new Row(
          children: <Widget>[
            // new Container(
            //   margin: EdgeInsets.fromLTRB(25.0, 0.0, 14.0, 0.0),
            //   child: Text(
            //     '话题',
            //     style: textStyle(
            //         size: 16,
            //         color: Color(0XFF545454),
            //         fontWeight: FontWeight.w600),
            //   ),
            // ),
            // new Container(
            //   width: 1.0,
            //   height: 64.0,
            //   margin: EdgeInsets.all(0.0),
            //   color: Color(0XFFA5A5A5),
            // ),
            new Flexible(
              fit: FlexFit.tight,
              flex: 1,
              child: new Row(
                children: <Widget>[
                  new Expanded(
                    child: new GestureDetector(
                      onTap: () {
                        Navigator.of(context)
                            .push(new MaterialPageRoute(builder: (ctx) {
                          return new QAPage(
                            fatherGroup: fatherGroup,
                            subGroup: latestGroupInfo[0]['sub_group'],
                            titleName: latestGroupInfo[0]['group_title'],
                          );
                        }));
                      },
                      child: ImageTextCard(
                        imageStirng:
                            latestGroupInfo[0]['icon_image']['url'].toString(),
                        isNetImage: true,
                        title: latestGroupInfo[0]['group_title'].toString(),
                      ),
                    ),
                  ),
                  new Expanded(
                    child: new GestureDetector(
                      onTap: () {
                        Navigator.of(context)
                            .push(new MaterialPageRoute(builder: (ctx) {
                          return new QAPage(
                            fatherGroup: fatherGroup,
                            subGroup: latestGroupInfo[1]['sub_group'],
                            titleName: latestGroupInfo[1]['group_title'],
                          );
                        }));
                      },
                      child: ImageTextCard(
                        imageStirng:
                            latestGroupInfo[1]['icon_image']['url'].toString(),
                        isNetImage: true,
                        title: latestGroupInfo[1]['group_title'].toString(),
                      ),
                    ),
                  ),
                  new Expanded(
                    child: new GestureDetector(
                      onTap: () {
                        Navigator.of(context)
                            .push(new MaterialPageRoute(builder: (ctx) {
                          return new QAPage(
                            fatherGroup: fatherGroup,
                            subGroup: latestGroupInfo[2]['sub_group'],
                            titleName: latestGroupInfo[2]['group_title'],
                          );
                        }));
                      },
                      child: ImageTextCard(
                        imageStirng:
                            latestGroupInfo[2]['icon_image']['url'].toString(),
                        isNetImage: true,
                        title: latestGroupInfo[2]['group_title'].toString(),
                      ),
                    ),
                  ),
                  new Expanded(
                    child: new GestureDetector(
                      onTap: () {
                        Navigator.of(context)
                            .push(new MaterialPageRoute(builder: (ctx) {
                          return new QAPage(
                            fatherGroup: fatherGroup,
                            subGroup: latestGroupInfo[3]['sub_group'],
                            titleName: latestGroupInfo[3]['group_title'],
                          );
                        }));
                      },
                      child: ImageTextCard(
                        imageStirng:
                            latestGroupInfo[3]['icon_image']['url'].toString(),
                        isNetImage: true,
                        title: latestGroupInfo[3]['group_title'].toString(),
                      ),
                    ),
                  ),
                  unfoldButton(),
                ],
              ),
            ),
          ],
        ),
      );
    } else {
      return Container(
        height: 20.0,
        margin: EdgeInsets.all(0.0),
      );
    }
  }

  Widget unfoldButton() {
    if (groupInfo.length > 4) {
      return new Builder(
        builder: (ctx) {
          return new InkWell(
            onTap: () {
              showGroupBottomView(ctx);
            },
            child: new Container(
              margin: EdgeInsets.fromLTRB(14.0, 0.0, 25.0, 0.0),
              child: Text('展开'),
            ),
          );
        },
      );
    } else {
      return Container(
        width: 0.0,
        margin: EdgeInsets.fromLTRB(14.0, 0.0, 25.0, 0.0),
      );
    }
  }

  showGroupBottomView(ctx) {
    showModalBottomSheet(
        context: ctx,
        builder: (sheetCtx) {
          return new Container(
              height: 300.0,
              padding: EdgeInsets.all(16.0),
              color: Colors.transparent,
              child: new SingleChildScrollView(
                child: Wrap(
                  children: subGroupWidgets.toList(),
                ),
              ));
        });
  }

  String listToJsonString(List list) {
    String result = '[';
    for (var i = 0; i < list.length; i++) {
      result += '"' + list[i] + '"';
      if (i != list.length - 1) {
        result += ',';
      }
    }
    result += ']';
    return result;
  }
}
