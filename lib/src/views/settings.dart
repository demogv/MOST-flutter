import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../common/events.dart';
import '../common/repository.dart';

import 'package:flutter_platform_widgets/flutter_platform_widgets.dart';

class Settings extends StatefulWidget {
  _SettingsState createState() => _SettingsState();
}

class _SettingsState extends State<Settings> {
  /* Process logout */

  @override
  Widget build(BuildContext context) {
    return PlatformScaffold(
      appBar: new PlatformAppBar(
        title: Text('设置'),
      ),
      body: Column(
        children: <Widget>[
          new ListTile(
            title: Text('夜间模式'),
            subtitle: Text('色彩变暗，夜间阅读不刺眼'),
            trailing: PlatformWidget(
              android: (_) => Switch(
                    value: GlobalConfig.dark,
                    onChanged: (nightMode) {
                      if (this.mounted) {
                        // ensure the object is still in the tree.
                        setState(() {
                          GlobalConfig.dark = nightMode;
                        });
                      }
                    }, // TODO: Implement night mode
                  ),
              ios: (_) => CupertinoSwitch(
                    value: GlobalConfig.dark,
                    onChanged: (nightMode) {
                      if (this.mounted) {
                        // ensure the object is still in the tree.
                        setState(() {
                          GlobalConfig.dark = nightMode;
                        });
                      }
                    }, // TODO: Implement night mode
                  ),
            ),
          ),
          SwitchListTile(
            title: Text('夜间模式'),
            subtitle: Text('色彩变暗，夜间阅读不刺眼'),
            value: GlobalConfig.dark,
            onChanged: (nightMode) {
              if (this.mounted) {
                // ensure the object is still in the tree.
                setState(() {
                  GlobalConfig.dark = nightMode;
                });
              }
            }, // TODO: Implement night mode
          ),
          new FlatButton(
            padding: EdgeInsets.all(0.0), // no padding in this layer
            onPressed: () {}, // TODO: Impement font size adjustment
            child: ListTile(
              title: Text('字体大小'),
              subtitle: Text('选择合适的字体大小'),
              trailing: Text(
                '18',
                style: Theme.of(context).textTheme.button,
              ),
            ),
          ),
          new FlatButton(
            padding: const EdgeInsets.all(0.0),
            onPressed: () {}, // TODO: implement help center
            child: ListTile(
              title: Text('帮助'),
              subtitle: Text('有问必答'),
            ),
          ),
          new FlatButton(
            padding: const EdgeInsets.all(0.0),
            onPressed: () {}, // TODO: implement help center
            child: ListTile(
              title: Text('反馈'),
              subtitle: Text('有什么想法欢迎与我们分享'),
            ),
          ),
          new FlatButton(
            padding: const EdgeInsets.all(0.0),
            onPressed: () {}, // TODO: implement help center
            child: ListTile(
              title: Text('了解我们'),
              subtitle: Text('看看 MOST 是个啥'),
            ),
          ),
          const Divider(),
          new FlatButton(
            onPressed: () async {
              /// Clear secure storage, enter login page.
              await Repository.get().signOut();
              print(
                  'secureStorage.accessToken${await Repository.get().secureStorage.getSessionToken()}');
              Navigator.of(context)
                ..pushNamedAndRemoveUntil(
                    '/login', (Route<dynamic> r) => false);
            },
            child: Text(
              '退出登录',
              style: Theme.of(context)
                  .textTheme
                  .button
                  .copyWith(color: Colors.red),
            ),
          ),
        ],
      ),
    );
  }
}

class GlobalConfig {
  static bool dark = false;
  static ThemeData themeData = new ThemeData.light();
  static Color searchBackgroundColor = new Color(0xFFEBEBEB);
  static Color cardBackgroundColor = Colors.white;
  static Color fontColor = Colors.black54;
}
