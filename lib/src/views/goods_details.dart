import 'package:flutter/material.dart';
import 'package:most/src/api/leancloud.dart';
import 'package:most/src/common/repository.dart';
import 'package:most/src/utils/utils.dart';
import 'package:most/src/widgets/answer_end_line.dart';
import 'order_page.dart';
import 'dart:convert';
import 'package:date_format/date_format.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:flutter_alipay/flutter_alipay.dart';
import 'package:flutter_platform_widgets/flutter_platform_widgets.dart';
//import 'package:flutter_search_bar/flutter_search_bar.dart';

class GoodsDetailPage extends StatefulWidget {
  final String tradeId;
  final int tag;

  GoodsDetailPage({Key key, this.tradeId, this.tag}) : super(key: key);
  @override
  State<StatefulWidget> createState() {
    return new GoodsDetailPageState(tradeId: tradeId, tag: tag);
  }
}

class GoodsDetailPageState extends State<GoodsDetailPage> {
  String tradeId;
  int tag;

  Map tradeObject;
  String myId; //The Id of this user.
  List commentData;
  //RegExp regExp1 = new RegExp("</.*>");
  //RegExp regExp2 = new RegExp("<.*>");
  TextStyle subtitleStyle =
      new TextStyle(fontSize: 12.0, color: const Color(0xFFB5BDC0));
  TextStyle contentStyle = new TextStyle(fontSize: 15.0, color: Colors.black);
  num curPage = 1;
  ScrollController _controller = new ScrollController();
  TextEditingController _inputController = new TextEditingController();

  GoodsDetailPageState({Key key, this.tradeId, this.tag});

  //设置字体
  textStyle({int size, Color color, FontWeight fontWeight}) {
    return new TextStyle(
      fontSize: size.toDouble(),
      decoration: TextDecoration.none,
      color: color,
      fontWeight: fontWeight,
    );
  }

  //获取评论
  getData() async {
    //测试一下跑云函数
    String userIdGet = await Repository.get().thisUserObjectId;
    final Map tradeData = await LeanCloudFunction.callFunction(
        myData: {'trade_id': tradeId, 'tag': tag}, functionName: 'get_trade');
    if (this.mounted) {
      setState(() {
        myId = userIdGet;
        tradeObject = tradeData;
        commentData = tradeData['trade_to_comment'];
        commentData.add('COMPLETE');
      });
    }
  }

  @override
  void initState() {
    super.initState();
    getData();
  }

  @override
  Widget build(BuildContext context) {
    var _body = (commentData == null && tradeObject == null && myId == null)
        ? new Center(
            child: new PlatformCircularProgressIndicator(
              android: (_) => MaterialProgressIndicatorData(
                    backgroundColor: Color(0XFFFF2D55),
                  ),
              ios: (_) => CupertinoProgressIndicatorData(),
            ),
          )
        : new Column(
            children: <Widget>[
              new Container(
                height: 30.0,
                margin: new EdgeInsets.fromLTRB(20.0, 20.0, 20.0, 0.0),
                child: questionTitleView(),
              ),
              new Container(
                height: 40.0,
                margin: new EdgeInsets.fromLTRB(20.0, 20.0, 20.0, 0.0),
                child: userInfoView(),
              ),
              new Padding(
                  padding: const EdgeInsets.fromLTRB(20.0, 20.0, 20.0, 0.0),
                  child: new Row(
                    children: <Widget>[
                      new Expanded(
                        child: new Text(
                          tradeObject['trade_detail'] == null
                              ? tradeObject['trade_title']
                              : tradeObject['trade_detail'],
                          style: textStyle(
                              size: 14,
                              color: Color(0XFF2F2F2F),
                              fontWeight: FontWeight.w500),
                          textAlign: TextAlign.start,
                        ),
                      )
                    ],
                  )),
              new Container(
                height: 200.0,
                padding: EdgeInsets.all(20.0),
                child: new Swiper(
                  itemBuilder: (BuildContext context, int index) {
                    return new Image.network(tradeObject['images'][index],
                        fit: BoxFit.fitWidth);
                  },
                  pagination:
                      new SwiperPagination(margin: new EdgeInsets.all(5.0)),
                  loop: true,
                  index: 0,
                  autoplay: true,
                  autoplayDisableOnInteraction: true,
                  itemCount: tradeObject['images'].length,
                  viewportFraction: 0.8,
                  scale: 0.9,
                ),
              ),
              new Flexible(
                flex: 1,
                fit: FlexFit.tight,
                child: new Container(
                  margin: new EdgeInsets.fromLTRB(20.0, 20.0, 20.0, 0.0),
                  child: new ListView.builder(
                    itemCount: commentData.length * 2,
                    itemBuilder: renderListItem,
                    controller: _controller,
                  ),
                ),
              ),
              new Container(
                margin: EdgeInsets.fromLTRB(16.0, 10.0, 16.0, 0.0),
                height: 44.0,
                padding: EdgeInsets.fromLTRB(0.0, 7.0, 0.0, 7.0),
                color: Color(0XFFF7F7F7),
                child: new TextField(
                  maxLines: 1,
                  cursorColor: Colors.white,
                  style: textStyle(
                      size: 16,
                      color: Colors.black,
                      fontWeight: FontWeight.normal),
                  controller: _inputController,
                  decoration: new InputDecoration(
                      hintText: "评论点啥呗～",
                      contentPadding:
                          const EdgeInsets.fromLTRB(5.0, 5.0, 5.0, 5.0),
                      border: new OutlineInputBorder(
                          borderRadius: const BorderRadius.all(
                              const Radius.circular(10.0)),
                          borderSide: BorderSide(color: Colors.white))),
                ),
              ),
            ],
          );

    var _actions = (commentData == null && tradeObject == null && myId == null)
        ? <Widget>[]
        : <Widget>[
            toOrderButton(),
          ];

    return new PlatformScaffold(
        appBar: new PlatformAppBar(
          backgroundColor: Colors.white,
          title: new Text(
            "商品详情",
          ),
          // android: (_) => MaterialAppBarData(
          //       iconTheme: new IconThemeData(color: Colors.white),
          //       actions: _actions,
          //     ),
          // ios: (context) =>
          //     CupertinoNavigationBarData(trailing: Row(children: _actions)),
        ),
        body: _body);
  }

  Widget userInfoView() {
    return new Container(
        color: const Color(0xfffcfcfc),
        child: new Row(
          children: <Widget>[
            //放用户图片
            new Container(
              width: 40.0,
              height: 40.0,
              decoration: new BoxDecoration(
                image: new DecorationImage(
                    image: new NetworkImage(tradeObject['user_image']['url'])),
                borderRadius: new BorderRadius.all(
                  const Radius.circular(20.0),
                ),
              ),
            ),
            //标题
            new Flexible(
                flex: 1,
                fit: FlexFit.tight,
                child: new Container(
                  margin: new EdgeInsets.fromLTRB(10.0, 0.0, 0.0, 0.0),
                  child: new Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      new Text(
                        tradeObject['username'] + ' ' + '发布于',
                        style: this.textStyle(
                            size: 14,
                            color: Color(0xFF818181),
                            fontWeight: FontWeight.w600),
                      ),
                      new Text(
                        getDate(
                            DateTime.parse(tradeObject['createdAt']['iso'])),
                        style: this.textStyle(
                            size: 14,
                            color: Color(0xFF818181),
                            fontWeight: FontWeight.w600),
                      ),
                    ],
                  ),
                )),
            priceWidget(),
          ],
        ));
  }

  Widget questionTitleView() {
    return new Container(
      color: const Color(0xfffcfcfc),
      child: new Row(
        children: <Widget>[
          //红点
          new Container(
            width: 8.0,
            height: 20.0,
            margin: new EdgeInsets.fromLTRB(0.0, 5.0, 10.0, 5.0),
            decoration: new BoxDecoration(
                color: Color(0XFFFF2D55),
                borderRadius: new BorderRadius.all(const Radius.circular(3.5))),
          ),
          //问题标题
          new Flexible(
            flex: 1,
            fit: FlexFit.tight,
            child: new Text(
              tradeObject['trade_title'].toString(),
              style: this.textStyle(
                  size: 18, color: Colors.black, fontWeight: FontWeight.w600),
            ),
          ),
          //回答按钮
          buyButton(),
        ],
      ),
    );
  }

  Widget buyButton() {
    if ((tradeObject['order_id'] == null || tradeObject['order_id'] == '') &&
        myId != tradeObject['user_id']) {
      return new Container(
        width: 54.0,
        height: 30.0,
        margin: new EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 0.0),
        child: new FlatButton(
            padding: new EdgeInsets.all(0.0),
            child: new Text(
              "购买",
              style: this.textStyle(
                  size: 14, color: Colors.white, fontWeight: FontWeight.w500),
            ),
            onPressed: () async {
              var orderString = await LeanCloudFunction.callFunction(myData: {
                'user_id': tradeObject['user_id'],
                'price': tradeObject['price'].toString(),
                'trade_title': tradeObject['trade_title']
              }, functionName: 'get_alipay_request_string');
              var result = await FlutterAlipay.pay(orderString.toString());
              Map response = json.decode(result["result"]);
              if (response['alipay_trade_app_pay_response']['msg'] ==
                  'Success') {
                String alipayId = response['alipay_trade_app_pay_response']
                        ['trade_no']
                    .toString();
                String sessionTokenGet = await Repository.get().sessionToken;
                String orderString =
                    await LeanCloudFunction.callFunctionWithToken(
                  myData: {
                    'trade_id': tradeId,
                    'buyer_id': myId,
                    'seller_id': tradeObject['user_id'],
                    'order_title': tradeObject['trade_title'],
                    'price': tradeObject['price'],
                    'trade_tag': tag,
                    'alipay_id': alipayId,
                  },
                  functionName: 'upload_Order',
                  sessionToken: sessionTokenGet,
                );
                Navigator.of(context).push(MaterialPageRoute(
                  builder: (context) => OrderPage(
                        orderId: orderString,
                        myId: myId,
                      ),
                ));
                getData();
              }
            },
            shape: new RoundedRectangleBorder(
                borderRadius: new BorderRadius.circular(9.0))),
        decoration: new BoxDecoration(
            color: Color(0XFFFF2D55),
            borderRadius: new BorderRadius.all(const Radius.circular(9.0))),
      );
    } else if (tradeObject['order_id'] == null &&
        myId == tradeObject['user_id']) {
      return new Container(
        width: 54.0,
        height: 30.0,
        margin: new EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 0.0),
        child: new FlatButton(
            padding: new EdgeInsets.all(0.0),
            child: new Text(
              "修改",
              style: this.textStyle(
                  size: 14, color: Colors.white, fontWeight: FontWeight.w500),
            ),
            onPressed: () {
              //
            },
            shape: new RoundedRectangleBorder(
                borderRadius: new BorderRadius.circular(9.0))),
        decoration: new BoxDecoration(
            color: Color(0XFFFF2D55),
            borderRadius: new BorderRadius.all(const Radius.circular(9.0))),
      );
    } else {
      return new Center(
        child: Text('已售出'),
      );
    }
  }

  Widget toOrderButton() {
    if (tradeObject['order_id'] != null &&
        (myId == tradeObject['user_id'] || myId == tradeObject['buyer_id'])) {
      return new FlatButton(
        shape: CircleBorder(),
        child: Text('订单'),
        textColor: Theme.of(context).primaryTextTheme.title.color,
        onPressed: () => Navigator.of(context).push(MaterialPageRoute(
              builder: (context) => OrderPage(
                    orderId: tradeObject['order_id'],
                    myId: myId,
                  ),
            )),
      );
    } else {
      return Container();
    }
  }

  Widget renderListItem(BuildContext context, int i) {
    if (i.isEven && i != 0) {
      return new Divider(
        height: 1.0,
      );
    } else if (i.isEven && i == 0) {
      return new Divider(
        height: 1.0,
        color: Colors.white,
      );
    }
    i -= 1;
    i ~/= 2;
    return _renderAnswerRow(context, i);
  }

  //用的是测试的变量，日后需要稍作调整
  Widget priceWidget() {
    if (tradeObject['original_price'].toString() == '') {
      return new Container(
          height: 40.0,
          padding: EdgeInsets.fromLTRB(0.0, 5.0, 0.0, 5.0),
          child: new Text(
            tradeObject['price'].toString() + "¥",
            style: textStyle(
                size: 30,
                color: Color(0XFF263238),
                fontWeight: FontWeight.w600),
          ));
    } else {
      return new Container(
          height: 40.0,
          padding: EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 0.0),
          child: new Column(
            children: <Widget>[
              new Container(
                height: 20.0,
                margin: EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 10.0),
                child: new Text(
                  tradeObject['price'].toString() + "¥",
                  style: textStyle(
                      size: 20,
                      color: Color(0XFF263238),
                      fontWeight: FontWeight.w600),
                ),
              ),
              new Container(
                height: 10.0,
                margin: EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 0.0),
                child: new Text(
                  "原价: " + tradeObject['original_price'].toString() + "¥",
                  style: textStyle(
                      size: 10,
                      color: Color(0xFF959595),
                      fontWeight: FontWeight.w500),
                ),
              ),
            ],
          ));
    }
  }

  //渲染回答列表
  _renderAnswerRow(context, i) {
    var listItem = commentData[i];
    if (listItem is String && listItem == 'COMPLETE') {
      return new EndLine();
    }
    String userName = listItem['username'].toString();
    String userImageUrl = listItem['user_image']['url'].toString();
    DateTime dateTime = DateTime.parse(listItem['createdAt']['iso'].toString());
    String content = listItem['comment_detail'].toString();
    var row = new Container(
      margin: const EdgeInsets.fromLTRB(0.0, 20.0, 0.0, 20.0),
      child: new Column(
        children: <Widget>[
          new Row(
            children: <Widget>[
              //放用户图片
              new Container(
                width: 20.0,
                height: 20.0,
                decoration: new BoxDecoration(
                  image: new DecorationImage(
                      image: new NetworkImage(userImageUrl)),
                  borderRadius: new BorderRadius.all(
                    const Radius.circular(10.0),
                  ),
                ),
              ),
              new Container(
                height: 20.0,
                margin: const EdgeInsets.fromLTRB(6.0, 0.0, 0.0, 0.0),
                child: new Text(userName + ' 评论于',
                    style: textStyle(
                        size: 12,
                        color: Color(0xFF818181),
                        fontWeight: FontWeight.normal)),
              ),
              new Container(
                  height: 20.0,
                  margin: const EdgeInsets.fromLTRB(6.0, 0.0, 0.0, 0.0),
                  child: new Text(
                    getDate(dateTime),
                    style: subtitleStyle,
                  ))
            ],
          ),
          new Container(
              margin: const EdgeInsets.fromLTRB(0.0, 5.0, 0.0, 0.0),
              child: new Row(
                children: <Widget>[
                  new Expanded(
                      child: new Text(
                    content,
                    style: contentStyle,
                  ))
                ],
              ))
        ],
      ),
    );
    return new Builder(
      builder: (ctx) {
        return new InkWell(
          //onTap: () {
          //  getComment(answerToComment: answerToComment, ctx: ctx);
          //},
          child: row,
        );
      },
    );
  }

  /*
  void sendReply(authorId) {
    String replyStr = _inputController.text;
    if (replyStr == null || replyStr.length == 0 || replyStr.trim().length == 0) {
      return;
    } else {
      DataUtils.isLogin().then((isLogin) {
        if (isLogin) {
          DataUtils.getAccessToken().then((token) {
            Map<String, String> params = new Map();
            params['access_token'] = token;
            params['id'] = "${tweetData['id']}";
            print("id: ${tweetData['id']}");
            params['catalog'] = "3";
            params['content'] = replyStr;
            params['authorid'] = "$authorId";
            print("authorId: $authorId");
            params['isPostToMyZone'] = "0";
            params['dataType'] = "json";
            NetUtils.get(Api.COMMENT_REPLY, params: params).then((data) {
              if (data != null) {
                var obj = json.decode(data);
                var error = obj['error'];
                if (error != null && error == '200') {
                  // 回复成功
                  Navigator.of(context).pop();
                  getReply(false);
                }
              }
            });
          });
        }
      });
    }
  }*/

}
