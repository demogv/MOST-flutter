import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:image_cropper/image_cropper.dart';
import 'package:most/src/common/events.dart';
import 'package:most/src/models/post_type.dart';
import 'package:most/src/models/search_info.dart';
import 'package:transparent_image/transparent_image.dart';

import '../common/repository.dart';
import 'package:flutter/material.dart';
import 'package:simple_autocomplete_formfield/simple_autocomplete_formfield.dart';
import 'package:zefyr/zefyr.dart';
//import 'package:bubble_tab_indicator/bubble_tab_indicator.dart';
import 'package:groovin_material_icons/groovin_material_icons.dart';
//import 'package:circle_indicator/circle_indicator.dart';
import 'package:quill_delta/quill_delta.dart';
import '../../src/api/leancloud.dart';

import 'package:image_picker/image_picker.dart';
// import 'package:multiple_image_picker/multiple_image_picker.dart';
import 'package:flutter_platform_widgets/flutter_platform_widgets.dart';
import '../utils/leancloudImageDelegate.dart';

class Post extends StatefulWidget {
  Post({Key key, this.type}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final PostType type;

  @override
  _PostState createState() => new _PostState();
}

class _PostState extends State<Post> with SingleTickerProviderStateMixin {
  // final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  List<Widget> tabList;

  final _formKey = GlobalKey<FormState>();

// Type decision
  Map _typeToName = const {
    PostType.ask: '提问',
    PostType.used_book: '二手书',
    PostType.second_hand: '闲置品',
    PostType.daigou: '代购',
  };
  List<Widget> _formFieldList;

  Widget _buildFormFields() {
    return Form(
      key: _formKey,
      // autovalidate: true,
      child: Column(
        children: _formFieldList,
      ),
    );
  }

  List<Widget> _buildFormFieldList() {
    switch (widget.type) {
      case PostType.ask:
        return _buildAskForm();
      case PostType.used_book:
        return _buildUsedBookForm();
      case PostType.second_hand:
        return _buildSecondHandForm();
      case PostType.daigou:
        return _buildDaigouForm();
      default:
        print('No such post type');
        return _buildAskForm();
    }
  }

  // void onSubmitted(String value) {
  //   setState(() => _scaffoldKey.currentState
  //       .showSnackBar(new SnackBar(content: new Text('You wrote $value!'))));
  // }
  // Backdrop
  final GlobalKey _backdropKey = new GlobalKey(debugLabel: 'Backdrop');
  AnimationController _controller;

  double formHeight;
  final FocusNode _focusNode = new FocusNode();

  // Data fields
  /// Rich text
  final _editorController = ZefyrController(NotusDocument());

  /// Title
  final _titleController = TextEditingController();

  /// Book condition for used books or other second-hand stuff
  // double _stuffCondition = 7.0;

  double _discount = 0.7;

  /// Title
  double _price = 100.0;

  FileData _coverImage;

  /// Departure date for daigo
  DateTime _daigouDepartureDate;

  /// Location for daigo
  String _daigouLocation;

  /// Quantity for used books, second-hand stuff, and daigo
  int _stuffQuantity;

  // Styles
  TextStyle _formTextStyle;
  TextStyle _formLableStyle;

  @override
  void initState() {
    super.initState();
    _controller = new AnimationController(
      duration: const Duration(milliseconds: 300),
      value: 1.0,
      vsync: this,
    );

    // tabList = [
    //   new PostPages(
    //       contentPage: new Ask(
    //     controller: editorController,
    //   )),
    //   new PostPages(contentPage: new UsedBook()),
    //   new PostPages(contentPage: new SecondHand()),
    //   new PostPages(contentPage: new Daigou()),
    // ];
  }

  @override
  void didChangeDependencies() {
    // TODO: implement didChangeDependencies
    _formTextStyle =
        Theme.of(context).textTheme.title.copyWith(color: Colors.white);
    _formLableStyle =
        TextStyle(color: Colors.white70, decorationColor: Colors.blue);
    super.didChangeDependencies();
  }

  @override
  void dispose() {
    _controller.dispose();
    _editorController.dispose();
    _titleController.dispose();
    _focusNode.dispose();
    super.dispose();
  }

  // ListTile() {}
  PreferredSizeWidget _buildTitleField() {
    return PreferredSize(
      preferredSize:
          Size.fromHeight(kToolbarHeight + 72.0 * _formFieldList.length),
      child: Container(margin: EdgeInsets.all(16.0), child: _buildFormFields()),
    );
  }

  Widget _buildStack(BuildContext context, BoxConstraints constraints) {
    const double panelTitleHeight = 48.0;
    final Size panelSize = constraints.biggest;
    final double panelTop = panelSize.height - panelTitleHeight;

    final Animation<RelativeRect> panelAnimation = new RelativeRectTween(
      begin: new RelativeRect.fromLTRB(
        0.0,
        panelTop - MediaQuery.of(context).padding.bottom,
        0.0,
        panelTop - panelSize.height,
      ),
      end: const RelativeRect.fromLTRB(0.0, 0.0, 0.0, 0.0),
    ).animate(
      new CurvedAnimation(
        parent: _controller,
        curve: Curves.linear,
      ),
    );

    final ThemeData theme = Theme.of(context);

    return new Container(
      key: _backdropKey,
      color: theme.primaryColor,
      child: new Stack(
        children: <Widget>[
          new PositionedTransition(
            rect: panelAnimation,
            child: new BackdropPanel(
              title: new Text('详细描述'),
              child: ZefyrEditor(
                // padding: EdgeInsets.only(top: 24.0, left: 24),
                // autofocus: true,
                controller: _editorController,
                focusNode: _focusNode,
                enabled: true,
                imageDelegate: LeanCloudImageDelegate(),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Future<File> _cropImage(File imageFile) async {
    return await ImageCropper.cropImage(
      sourcePath: imageFile.path,
    );
  }

  Future _getImage() async {
    File image = await ImagePicker.pickImage(source: ImageSource.gallery);

    // Crop image
    image = await _cropImage(image);
    // print(image.);
    // image = await _cropImage(image);
    // TODO: Remove this

    if (image != null) {
      final imageFileData = await LeanCloudFile.upload(
        image.uri,
      );

      print(imageFileData.uri.toString());

      // ensure the object is still in the tree.
      if (this.mounted) {
        // Only setState when uploading was successful
        if (imageFileData != null) {
          setState(() {
            _coverImage = imageFileData;
          });
        } else {
          // final snackBar = SnackBar(content: Text('抱歉，头像更换失败'));
          // Scaffold.of(context).showSnackBar(snackBar);
        }
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    final indicatorColor = Colors.white;

    _formFieldList =
        _buildFormFieldList(); // Put here to set every time it rebuilds (to move slider)

    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return new Scaffold(
      appBar: AppBar(
        title: Row(
          children: <Widget>[
            new Text('发布${_typeToName[widget.type]}'),
          ],
        ),
        elevation: 0.0,
        actions: <Widget>[
          new FlatButton(
            shape: CircleBorder(),
            child: Text('继续'),
            textColor: Theme.of(context).primaryTextTheme.title.color,
            onPressed: () {
              if (_formKey.currentState.validate()) {
                Navigator.of(context).push(
                  MaterialPageRoute(
                    builder: (context) => ChooseTag(
                          // Use the first 15 characters as the body if no title provided
                          title: _titleController.text ??
                              _editorController.document.toPlainText(),
                          body:
                              json.encode(_editorController.document.toJson()),
                          type: widget.type,
                          // stuffCondition: _discount,
                          stuffQuantity: _stuffQuantity,
                          daigouDepartureDate: _daigouDepartureDate,
                          daigouLocation: _daigouLocation,
                          images: [
                            _coverImage
                          ], // TODO: should be a list from birth
                          originalPrice: _price,
                          price: _price * _discount,
                        ),
                  ),
                );
              }
            },
          ),
        ],
        bottom: _buildTitleField(),
      ),
      body: new LayoutBuilder(
        builder: _buildStack,
      ),
    );
  }

  List _buildAskForm() => <Widget>[
        TextFormField(
          controller: _titleController,
          autofocus: true,
          // autovalidate: true,
          maxLength: 30,
          style: _formTextStyle,
          validator: (value) {
            if (value.isEmpty) return '还是要输入点什么噢';
          },
          decoration: new InputDecoration(
              prefixIcon: Icon(
                Icons.question_answer,
                color: Colors.white70,
              ),
              filled: true,
              labelText: '你的议题',
              labelStyle: _formLableStyle,
              suffixText: '？',
              suffixStyle: _formTextStyle),
        ),
      ];

  List _buildUsedBookForm() => <Widget>[
        TextFormField(
          controller: _titleController,
          autofocus: true,
          style: _formTextStyle,
          maxLength: 30,
          validator: (value) {
            if (value.isEmpty) return '还是要输入点什么噢';
          },
          decoration: new InputDecoration(
            filled: true,
            prefixIcon: Icon(
              Icons.book,
              color: Colors.white70,
            ),
            labelText: '书名',
            labelStyle: _formLableStyle,
          ),
        ),
        Container(
          padding: const EdgeInsets.fromLTRB(8.0, 8.0, 8.0, 0.0),
          // height: 72.0,
          alignment: Alignment.bottomCenter,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                '价格',
                textAlign: TextAlign.left,
                style: Theme.of(context)
                    .textTheme
                    .subhead
                    .copyWith(fontWeight: FontWeight.bold, color: Colors.white),
              ),
              // Slider(
              //   label: '$_stuffCondition 成新',
              //   value: _stuffCondition,
              //   max: 10.0,
              //   min: 0.0,
              //   activeColor: Colors.white70,
              //   divisions: 20,
              //   onChanged: (value) {
              //     setState(() {
              //       _stuffCondition = value;
              //     });
              //   },
              // ),
              Slider(
                label: '$_price 元',
                value: _price,
                max: 1000.0,
                min: 0.0,
                activeColor: Colors.white70,
                divisions: 2000,
                onChanged: (value) {
                  setState(() {
                    _price = value;
                  });
                },
              ),
            ],
          ),
        ),
        Container(
          padding: const EdgeInsets.fromLTRB(8.0, 8.0, 8.0, 0.0),
          // height: 72.0,
          alignment: Alignment.bottomCenter,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                '折扣',
                textAlign: TextAlign.left,
                style: Theme.of(context)
                    .textTheme
                    .subhead
                    .copyWith(fontWeight: FontWeight.bold, color: Colors.white),
              ),
              Slider(
                label: '$_discount 折',
                value: _discount,
                max: 1.0,
                min: 0.0,
                activeColor: Colors.white70,
                divisions: 20,
                onChanged: (value) {
                  setState(() {
                    _discount = value;
                  });
                },
              ),
            ],
          ),
        ),
        Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: _coverImage != null
                ? CrossAxisAlignment.center
                : CrossAxisAlignment.stretch,
            children: <Widget>[
              _coverImage != null
                  ? FlatButton(
                      child: FadeInImage.memoryNetwork(
                        height: 120.0,
                        image: _coverImage.uri.toString(),
                        placeholder: kTransparentImage,
                      ),
                      onPressed: _getImage,
                    )
                  : RaisedButton.icon(
                      onPressed: _getImage,
                      icon: Icon(
                        Icons.image,
                        color: Theme.of(context).accentIconTheme.color,
                      ),
                      label: Text(
                        '上传封面图',
                        style: Theme.of(context).accentTextTheme.button,
                      ),
                    ),
            ]),
      ];
  List _buildSecondHandForm() => <Widget>[
        new TextFormField(
          controller: _titleController,
          maxLength: 30,
          autofocus: true,
          style: _formTextStyle,
          validator: (value) {
            if (value.isEmpty) return '还是要输入点什么噢';
          },
          decoration: new InputDecoration(
            filled: true,
            prefixIcon: Icon(
              GroovinMaterialIcons.package,
              color: Colors.white70,
            ),
            labelText: '物品名称',
            labelStyle: _formLableStyle,
          ),
        ),
        Container(
          padding: const EdgeInsets.fromLTRB(8.0, 8.0, 8.0, 0.0),
          // height: 72.0,
          alignment: Alignment.bottomCenter,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                '价格',
                textAlign: TextAlign.left,
                style: Theme.of(context)
                    .textTheme
                    .subhead
                    .copyWith(fontWeight: FontWeight.bold, color: Colors.white),
              ),
              // Slider(
              //   label: '$_stuffCondition 成新',
              //   value: _stuffCondition,
              //   max: 10.0,
              //   min: 0.0,
              //   activeColor: Colors.white70,
              //   divisions: 20,
              //   onChanged: (value) {
              //     setState(() {
              //       _stuffCondition = value;
              //     });
              //   },
              // ),
              Slider(
                label: '$_price 元',
                value: _price,
                max: 1000.0,
                min: 0.0,
                activeColor: Colors.white70,
                divisions: 2000,
                onChanged: (value) {
                  setState(() {
                    _price = value;
                  });
                },
              ),
            ],
          ),
        ),
        Container(
          padding: const EdgeInsets.fromLTRB(8.0, 8.0, 8.0, 0.0),
          // height: 72.0,
          alignment: Alignment.bottomCenter,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                '折扣',
                textAlign: TextAlign.left,
                style: Theme.of(context)
                    .textTheme
                    .subhead
                    .copyWith(fontWeight: FontWeight.bold, color: Colors.white),
              ),
              Slider(
                label: '$_discount 折',
                value: _discount,
                max: 1.0,
                min: 0.0,
                activeColor: Colors.white70,
                divisions: 20,
                onChanged: (value) {
                  setState(() {
                    _discount = value;
                  });
                },
              ),
            ],
          ),
        ),
        Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: _coverImage != null
                ? CrossAxisAlignment.center
                : CrossAxisAlignment.stretch,
            children: <Widget>[
              _coverImage != null
                  ? FlatButton(
                      child: FadeInImage.memoryNetwork(
                        height: 120.0,
                        image: _coverImage.uri.toString(),
                        placeholder: kTransparentImage,
                      ),
                      onPressed: _getImage,
                    )
                  : RaisedButton.icon(
                      onPressed: _getImage,
                      icon: Icon(
                        Icons.image,
                        color: Theme.of(context).accentIconTheme.color,
                      ),
                      label: Text(
                        '上传封面图',
                        style: Theme.of(context).accentTextTheme.button,
                      ),
                    ),
            ]),
      ];
  List _buildDaigouForm() => <Widget>[
        new TextFormField(
          controller: _titleController,
          autofocus: true,
          maxLength: 30,
          style: _formTextStyle,
          validator: (value) {
            if (value.isEmpty) return '还是要输入点什么噢';
          },
          decoration: new InputDecoration(
            filled: true,
            labelText: '代购物品',
            prefixIcon: Icon(
              GroovinMaterialIcons.package_variant,
              color: Colors.white70,
            ),
            labelStyle: _formLableStyle,
          ),
        ),
      ];
}

// // Use Post Pages as a middle layer
// class PostPages extends StatefulWidget {
//   PostPages({Key key, this.contentPage}) : super(key: key);
//   final Widget contentPage;

//   @override
//   _PostPagesState createState() => new _PostPagesState();
// }

// class _PostPagesState extends State<PostPages> {
//   // Pager
//   PageController controller;
//   List pageList;

//   @override
//   void initState() {
//     super.initState();
//     controller = new PageController();
//     pageList = [widget.contentPage, new ChooseTag()];
//   }

//   @override
//   Widget build(BuildContext context) {
//     return new PlatformScaffold(
//       body: new PageView.builder(
//         physics: new NeverScrollableScrollPhysics(),
//         controller: controller,
//         itemCount: pageList.length,
//         itemBuilder: (_, int i) => pageList[i],
//       ),

//       // floatingActionButton: new FloatingActionButton.extended(
//       //   onPressed: () => controller.nextPage(
//       //       curve: Curves.easeOut, duration: Duration(milliseconds: 200)),
//       //   tooltip: '设定标签',
//       //   icon: new Icon(CommunityMaterialIcons.tag_plus),
//       //   label: Text('设标签'),
//       // ), // This trailing comma makes auto-formanew OutlineInputBorder()icer for build methods.
//     );
//   }
// }

// Choose Tags interface
class ChooseTag extends StatefulWidget {
  ChooseTag({
    Key key,
    this.body = '',
    @required this.title,
    this.type,
    // this.stuffCondition,
    this.daigouDepartureDate,
    this.daigouLocation,
    this.stuffQuantity,
    this.images,
    this.originalPrice,
    this.price,
  })  : assert(title != null), // Always requires a title
        super(key: key);

  final PostType type;

  final String title;
  final String body;

  final List<FileData> images;

  /// Book condition for used books or other second-hand stuff
  // final double stuffCondition;

  /// Departure date for daigo
  final DateTime daigouDepartureDate;

  /// Location for daigo
  final String daigouLocation;

  /// Quantity for used books, second-hand stuff, and daigo
  final int stuffQuantity;

  final double originalPrice;
  final double price; // 真正价格

  @override
  _ChooseTagState createState() {
    return new _ChooseTagState(
      content: body,
      title: title,
    );
  }
}

class _ChooseTagState extends State<ChooseTag> {
  final String title;
  final String content;
  Set<TagSearchResultEntry> recommendTags;
  Set<TagSearchResultEntry> allTags;
  List<TagSearchResultEntry> selectedTags;
  var selectedTag = TagSearchResultEntry();

  String keyword;

  int get tagOfThis {
    if (widget.type == PostType.used_book) return 4;
    if (widget.type == PostType.second_hand) return 5;
  }

  TextEditingController controller;

  final _formKey = GlobalKey<FormState>();

  _ChooseTagState({
    Key key,
    this.title,
    this.content,
  });

  Future<void> _initAllTags() async {
    final _allTags = await Repository.get().allTagsOf(widget.type);
    if (this.mounted) {
      setState(() {
        allTags = _allTags;
      });
    }
  }

  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  @override
  void initState() {
    super.initState();
    keyword = title;
    _initAllTags();
  }

  @override
  Widget build(BuildContext context) {
    final List<Widget> allChips =
        allTags?.map<Widget>((TagSearchResultEntry tag) {
      return new ChoiceChip(
        key: new ValueKey<String>(tag.groupTitle),
        label: new Text(tag.groupTitle),
        selected: selectedTag.groupTitle == tag.groupTitle,
        onSelected: (bool value) {
          setState(() {
            selectedTag = value ? tag : null;
          });
        },
      );
    })?.toList();
    // final List<Widget> recommendedChips =
    //     recommendTags?.map<Widget>((TagSearchResultEntry tag) {
    //   return new ChoiceChip(
    //     key: new ValueKey<String>(tag.groupTitle),
    //     label: new Text(tag.groupTitle),
    //     selected: selectedTags[0].groupTitle == tag.groupTitle,
    //     onSelected: (bool value) {
    //       setState(() {
    //         selectedTags[0] = value ? tag : null;
    //       });
    //     },
    //   );
    // })?.toList();

    // final List<Widget> otherChips = allTags == null
    //     ? <Widget>[]
    //     : allTags
    //         .difference(recommendTags)
    //         .map<Widget>((TagSearchResultEntry tag) {
    //         return new ChoiceChip(
    //           key: new ValueKey<String>(tag.groupTitle),
    //           label: new Text(tag.groupTitle),
    //           selected: selectedTags[0].groupTitle == tag.groupTitle,
    //           onSelected: (bool value) {
    //             setState(() {
    //               selectedTags[0] = value ? tag : null;
    //             });
    //           },
    //         );
    //       }).toList();

    // _updateRecommendedTags(String keyword) async {
    //   final _recommendTags =
    //       await Repository.get().recommendedPostTagsOf(keyword);
    //   if (this.mounted) {
    //     setState(() {
    //       recommendTags = _recommendTags;
    //       selectedTags?.clear();
    //     });
    //   }
    // }

    // _buildSearchBar() {
    //   return Container(
    //     padding: EdgeInsets.all(64.0),
    //     child: TextFormField(
    //       controller: controller,
    //       onFieldSubmitted: (String value) {
    //         _updateRecommendedTags(value);
    //         setState(() {
    //           keyword = controller.text;
    //         });
    //       },
    //       autofocus: true,
    //       decoration: InputDecoration(
    //           prefixIcon: Icon(GroovinMaterialIcons.tag),
    //           labelText: '标签',
    //           labelStyle: Theme.of(context)
    //               .textTheme
    //               .title
    //               .copyWith(fontWeight: FontWeight.bold)),
    //     ),
    //   );
    // }

    _buildTags() {
      // // _updateRecommendedTags(keyword);
      // if (recommendTags == null)
      //   return PlatformCircularProgressIndicator(); // Loading
      // if (recommendTags.isEmpty) {
      //   // Loaded
      //   return Container();
      // } else {
      //   return ListView(
      //     children: <Widget>[
      //       Text('推荐'),
      //       Wrap(
      //         children: recommendedChips,
      //       ),
      //       Text('其他'),
      //       // Wrap(children: otherChips),
      //     ],
      //   );
      // }
      return allChips == null
          ? Center(child: PlatformCircularProgressIndicator())
          : Wrap(
              spacing: 12.0,
              children: allChips,
            );
    }

    print('Title: ${widget.title}');
    print('Content: ${widget.body}');

    final _body = ListView(
      padding: EdgeInsets.all(56.0),
      children: <Widget>[
        // _buildSearchBar(),
        _buildTags(),
      ],
    );
    // TODO: implement build
    return new Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        title: Text('设定话题'),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.send),
            onPressed: () async {
              if (selectedTag != null) {
                Repository.get().eventBus.fire(InActionEvent('正在后台提交...'));
                if (widget.type == PostType.ask) {
                  await Repository.get().uploadQA(
                      title: title, tag: selectedTag, content: content);
                } else {
                  await Repository.get().uploadTrade(
                      title: title,
                      content: content,
                      images: widget.images,
                      originalPrice: widget.originalPrice,
                      price: widget.price,
                      tagInfo: selectedTag,
                      tagOfThis: tagOfThis);
                }
                Navigator.of(context)
                    .pushNamedAndRemoveUntil('/', (Route<dynamic> r) => false);
              } else {
                final snackbar = SnackBar(
                  content: Text('必须选择标签噢'),
                  // action: SnackBarAction(
                  //   label: 'Undo',
                  //   onPressed: () {
                  //     // Some code to undo the change!
                  //   },
                  // ),
                );
                Scaffold.of(context).showSnackBar(snackbar);
              }
            },
          ),
        ],
      ),
      body: _body,
      // floatingActionButton: new FloatingActionButton.extended(
      //   label: Text('发布'),
      //   icon: Icon(Icons.send),
      //   onPressed: () {
      //     addMessage();
      //     Navigator.of(context)
      //         .pushNamedAndRemoveUntil('/', (Route<dynamic> r) => false);
      //     // TODO: Consider use a wizard instead
      //     // (or a PageView with circle indicator)
      //   },
      // ),
    );
  }
}

// void chooseTag(BuildContext context) {
//   showDialog<Null>(
//     context: context,
//     barrierDismissible: false,
//     builder: (BuildContext context) {
//       return new AlertDialog(
//         title: new Text('标题'),
//         content: new SingleChildScrollView(
//           child: new ListBody(
//             children: <Widget>[
//               new Text('内容 1'),
//               new Text('内容 2'),
//             ],
//           ),
//         ),
//         actions: <Widget>[
//           new FlatButton(
//             child: new Text('确定'),
//             onPressed: () {
//               Navigator.of(context).pop();
//             },
//           ),
//         ],
//       );
//     },
//   ).then((val) {
//     print(val);
//   });
// }

// DATA
List<NotusDocument> messagesToPost;

void addMessage({NotusDocument content}) {}

// One BackdropPanel is visible at a time. It's stacked on top of the
// the BackdropDemo.
class BackdropPanel extends StatelessWidget {
  const BackdropPanel({
    Key key,
    this.onTap,
    this.onVerticalDragUpdate,
    this.onVerticalDragEnd,
    this.title,
    this.child,
  }) : super(key: key);

  final VoidCallback onTap;
  final GestureDragUpdateCallback onVerticalDragUpdate;
  final GestureDragEndCallback onVerticalDragEnd;
  final Widget title;
  final Widget child;

  @override
  Widget build(BuildContext context) {
    final ThemeData theme = Theme.of(context);
    return new Material(
      elevation: 2.0,
      borderRadius: const BorderRadius.only(
        topLeft: Radius.circular(16.0),
        topRight: Radius.circular(16.0),
      ),
      child: new Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          new GestureDetector(
            behavior: HitTestBehavior.opaque,
            onVerticalDragUpdate: onVerticalDragUpdate,
            onVerticalDragEnd: onVerticalDragEnd,
            onTap: onTap,
            child: new Container(
              height: 48.0,
              padding: const EdgeInsetsDirectional.only(start: 16.0),
              alignment: AlignmentDirectional.centerStart,
              child: new DefaultTextStyle(
                style: theme.textTheme.subhead,
                child: new Tooltip(
                  message: 'Tap to dismiss',
                  child: title,
                ),
              ),
            ),
          ),
          const Divider(height: 1.0),
          new Expanded(child: child),
        ],
      ),
    );
  }
}
