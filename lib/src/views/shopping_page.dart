import 'package:flutter/material.dart';
import 'package:most/src/common/repository.dart';
import 'package:most/src/views/second_hand_page.dart';
import 'package:most/src/widgets/add_post_fab.dart';
import 'package:most/src/widgets/card.dart';
import 'package:most/src/widgets/search_bar.dart';
import '../../src/api/leancloud.dart';
import '../../src/widgets/image_text_widget.dart';
import 'package:fab_menu/fab_menu.dart';
import 'package:flutter_platform_widgets/flutter_platform_widgets.dart';

class ShoppingPage extends StatefulWidget {
  ShoppingPage({Key key}) : super(key: key);
  @override
  State<StatefulWidget> createState() {
    return new ShoppingPageState();
  }
}

class ShoppingPageState extends State<ShoppingPage> {
  List dataT1;
  List dataT2;
  //List dataA1;

  TextStyle subtitleStyle =
      new TextStyle(fontSize: 12.0, color: const Color(0xFFB5BDC0));
  TextStyle contentStyle = new TextStyle(fontSize: 15.0, color: Colors.black);

  ShoppingPageState({Key key});

  //设置字体
  textStyle({int size, Color color, FontWeight fontWeight}) {
    return new TextStyle(
      fontSize: size.toDouble(),
      decoration: TextDecoration.none,
      color: color,
      fontWeight: fontWeight,
    );
  }

  setData() async {
    var newDataT1 = await LeanCloudFunction.callFunction(myData: {
      'father_group': 0,
      'sub_group': 0,
      'start': 0,
      'end': 2,
      'tag': 4
    }, functionName: 'recommend_SH');
    var newDataT2 = await LeanCloudFunction.callFunction(myData: {
      'father_group': 0,
      'sub_group': 0,
      'start': 0,
      'end': 2,
      'tag': 5
    }, functionName: 'recommend_SH');
    if (this.mounted) {
      this.setState(() {
        dataT1 = newDataT1;
        dataT2 = newDataT2;
        PageStorage.of(context)
            .writeState(context, dataT1, identifier: 'dataT1');
        PageStorage.of(context)
            .writeState(context, dataT2, identifier: 'dataT2');
      });
    }
  }

  @override
  initState() {
    super.initState();
    setData();
  }

  @override
  Widget build(BuildContext context) {
    dataT1 = PageStorage.of(context).readState(context, identifier: 'dataT1');
    dataT2 = PageStorage.of(context).readState(context, identifier: 'dataT2');
    var _body = (dataT1 == null && dataT2 == null)
        ? new Material(
          child: Center(
            child: PlatformCircularProgressIndicator(
              android: (_) => MaterialProgressIndicatorData(
                    backgroundColor: Color(0XFFFF2D55),
                  ),
              ios: (_) => CupertinoProgressIndicatorData(),
            ),
          )
        )
        : new Material(
          child: new ListView(
            children: <Widget>[
              new Container(
                height: 64.0,
                margin: EdgeInsets.fromLTRB(0.0, 20.0, 25.0, 0.0),
                child: new Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    // new Container(
                    //   margin: EdgeInsets.fromLTRB(25.0, 0.0, 14.0, 0.0),
                    //   child: Text(
                    //     '分区',
                    //     style: textStyle(
                    //         size: 16,
                    //         color: Color(0XFF545454),
                    //         fontWeight: FontWeight.w600),
                    //   ),
                    // ),
                    // new Container(
                    //   width: 1.0,
                    //   height: 64.0,
                    //   margin: EdgeInsets.all(0.0),
                    //   color: Color(0XFFA5A5A5),
                    // ),
                    new Flexible(
                      fit: FlexFit.tight,
                      flex: 1,
                      child: new Row(
                        children: <Widget>[
                          new Expanded(
                            child: GestureDetector(
                              onTap: () {
                                Navigator.of(context)
                                    .push(new MaterialPageRoute(builder: (ctx) {
                                  return new SecondHandPage(
                                    fatherGroup: 1,
                                    subGroup: 0,
                                    titleName: '学术中心',
                                  );
                                }));
                              },
                              child: ImageTextCard(
                                imageStirng: 'graphics/Trade1.png',
                                isNetImage: false,
                                title: '二手书',
                              ),
                            ),
                          ),
                          new Expanded(
                            child: FlatButton(
                                padding:
                                    EdgeInsets.fromLTRB(8.0, 0.0, 8.0, 0.0),
                                child: Column(
                                  children: <Widget>[
                                    Image.asset(
                                      'graphics/Trade2.png',
                                      // fit: BoxFit.fitWidth,
                                      alignment: Alignment.topCenter,
                                    ),
                                    Container(
                                      margin: EdgeInsets.only(top: 5.0),
                                      child: Text(
                                        '闲置品',
                                        style: TextStyle(
                                          fontSize: 10.0,
                                          color: Colors.black,
                                          fontWeight: FontWeight.w600,
                                        ),
                                      ),
                                    )
                                  ],
                                ),
                                onPressed: () => Navigator.of(context).push(
                                      new MaterialPageRoute(builder: (ctx) {
                                        return new SecondHandPage(
                                          fatherGroup: 2,
                                          subGroup: 0,
                                          titleName: '个人发展',
                                        );
                                      }),
                                    )),
                          ),
                          new Expanded(
                            child: GestureDetector(
                              onTap: () {
                                //Navigator.of(context).push(new MaterialPageRoute(builder: (ctx) {
                                //  return new SHPage(fatherGroup: 3, subGroup: 0, titleName: '校园指南',);
                                //}));
                              },
                              child: ImageTextCard(
                                imageStirng: 'graphics/Trade3.png',
                                isNetImage: false,
                                title: '饮品',
                              ),
                            ),
                          ),
                          new Expanded(
                            child: GestureDetector(
                              onTap: () {
                                //Navigator.of(context).push(new MaterialPageRoute(builder: (ctx) {
                                //  return new QAPage(fatherGroup: 4, subGroup: 0, titleName: '南腔北调',);
                                //}));
                              },
                              child: ImageTextCard(
                                imageStirng: 'graphics/Trade4.png',
                                isNetImage: false,
                                title: '美食',
                              ),
                            ),
                          ),
                          new Expanded(
                            child: GestureDetector(
                              onTap: () {
                                //Navigator.of(context).push(new MaterialPageRoute(builder: (ctx) {
                                //  return new QAPage(fatherGroup: 4, subGroup: 0, titleName: '南腔北调',);
                                //}));
                              },
                              child: ImageTextCard(
                                imageStirng: 'graphics/Trade5.png',
                                isNetImage: false,
                                title: '代购',
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              new MyCard(
                cardData: dataT1,
                cardTitle: '好书推荐',
                isFirstCard: true,
                numberOfLines: 3,
              ),
              new MyCard(
                cardData: dataT2,
                cardTitle: '闲货推荐',
                isFirstCard: false,
                numberOfLines: 3,
              ),
            ],
          ),
          );

    return new Scaffold(
      appBar: new AppBar(
        automaticallyImplyLeading: false,
        // textTheme: Theme.of(context).textTheme.copyWith(
        //       title: Theme.of(context)
        //           .textTheme
        //           .title
        //           .copyWith(color: Theme.of(context).primaryColor),
        //     ),
        // iconTheme: Theme.of(context)
        //     .iconTheme
        //     .copyWith(color: Theme.of(context).primaryColor),
        // backgroundColor: Theme.of(context).primaryTextTheme.title.color,
        title: Image.asset(
          Repository.mostLogoUri,
          height: 24.0,
        ),
        // title: new Text(
        //   'MOST',
        // ),
        actions: <Widget>[new SearchButton()],
      ),
      // appBar: buildAppBar(context),
      body: _body,
      floatingActionButton: AddPostFAB(),
      floatingActionButtonLocation:
          fabMenuLocation, // Remember to set floatingActionButtonLocation to fabMenuLocation')
    );
  }

  String listToJsonString(List list) {
    String result = '[';
    for (var i = 0; i < list.length; i++) {
      result += '"' + list[i] + '"';
      if (i != list.length - 1) {
        result += ',';
      }
    }
    result += ']';
    return result;
  }
}
