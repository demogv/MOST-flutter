import 'package:flutter/material.dart';

class PickColor extends StatefulWidget {
  @override
  _PickColorState createState() => _PickColorState();
}

class _PickColorState extends State<PickColor> {
  TextEditingController brightness;
  TextEditingController primarySwatch;
  TextEditingController primaryColor;
  TextEditingController primaryColorBrightness;
  TextEditingController primaryColorLight;
  TextEditingController primaryColorDark;
  TextEditingController accentColor;
  TextEditingController accentColorBrightness;
  TextEditingController canvasColor;
  TextEditingController scaffoldBackgroundColor;
  TextEditingController bottomAppBarColor;
  TextEditingController cardColor;
  TextEditingController dividerColor;
  TextEditingController highlightColor;
  TextEditingController splashColor;
  TextEditingController selectedRowColor;
  TextEditingController unselectedWidgetColor;
  TextEditingController disabledColor;
  TextEditingController buttonColor;
  TextEditingController secondaryHeaderColor;
  TextEditingController textSelectionColor;
  TextEditingController textSelectionHandleColor;
  TextEditingController backgroundColor;
  TextEditingController dialogBackgroundColor;
  TextEditingController indicatorColor;
  TextEditingController hintColor;
  TextEditingController errorColor;
  TextEditingController toggleableActiveColor;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('主题拣选'),
      ),
      body: ListView(
        children: <Widget>[
          TextField(
            controller: brightness,
            decoration: InputDecoration(labelText: 'brightness'),
          ),
          TextField(
            controller: primarySwatch,
            decoration: InputDecoration(labelText: 'primarySwatch'),
          ),
          TextField(
            controller: primaryColor,
            decoration: InputDecoration(labelText: 'primaryColor'),
          ),
          TextField(
            controller: primaryColorBrightness,
            decoration: InputDecoration(labelText: 'primaryColorBrightness'),
          ),
          TextField(
            controller: primaryColorLight,
            decoration: InputDecoration(labelText: 'primaryColorLight'),
          ),
          TextField(
            controller: primaryColorDark,
            decoration: InputDecoration(labelText: 'primaryColorDark'),
          ),
          TextField(
            controller: accentColor,
            decoration: InputDecoration(labelText: 'accentColor'),
          ),
          TextField(
            controller: accentColorBrightness,
            decoration: InputDecoration(labelText: 'accentColorBrightness'),
          ),
          TextField(
            controller: canvasColor,
            decoration: InputDecoration(labelText: 'canvasColor'),
          ),
          TextField(
            controller: scaffoldBackgroundColor,
            decoration: InputDecoration(labelText: 'scaffoldBackgroundColor'),
          ),
          TextField(
            controller: bottomAppBarColor,
            decoration: InputDecoration(labelText: 'bottomAppBarColor'),
          ),
          TextField(
            controller: cardColor,
            decoration: InputDecoration(labelText: 'cardColor'),
          ),
          TextField(
            controller: dividerColor,
            decoration: InputDecoration(labelText: 'dividerColor'),
          ),
          TextField(
            controller: highlightColor,
            decoration: InputDecoration(labelText: 'highlightColor'),
          ),
          TextField(
            controller: splashColor,
            decoration: InputDecoration(labelText: 'splashColor'),
          ),
          TextField(
            controller: selectedRowColor,
            decoration: InputDecoration(labelText: 'selectedRowColor'),
          ),
          TextField(
            controller: unselectedWidgetColor,
            decoration: InputDecoration(labelText: 'unselectedWidgetColor'),
          ),
          TextField(
            controller: disabledColor,
            decoration: InputDecoration(labelText: 'disabledColor'),
          ),
          TextField(
            controller: buttonColor,
            decoration: InputDecoration(labelText: 'buttonColor'),
          ),
          TextField(
            controller: secondaryHeaderColor,
            decoration: InputDecoration(labelText: 'secondaryHeaderColor'),
          ),
          TextField(
            controller: textSelectionColor,
            decoration: InputDecoration(labelText: 'textSelectionColor'),
          ),
          TextField(
            controller: textSelectionHandleColor,
            decoration: InputDecoration(labelText: 'textSelectionHandleColor'),
          ),
          TextField(
            controller: backgroundColor,
            decoration: InputDecoration(labelText: 'backgroundColor'),
          ),
          TextField(
            controller: dialogBackgroundColor,
            decoration: InputDecoration(labelText: 'dialogBackgroundColor'),
          ),
          TextField(
            controller: indicatorColor,
            decoration: InputDecoration(labelText: 'indicatorColor'),
          ),
          TextField(
            controller: hintColor,
            decoration: InputDecoration(labelText: 'hintColor'),
          ),
          TextField(
            controller: errorColor,
            decoration: InputDecoration(labelText: 'errorColor'),
          ),
          TextField(
            controller: toggleableActiveColor,
            decoration: InputDecoration(labelText: 'toggleableActiveColor'),
          ),
        ],
      ),
    );
  }
}
