import 'dart:async';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:transparent_image/transparent_image.dart';
import 'package:zefyr/zefyr.dart';
// import '../api/leancloud.dart';
import 'package:image_picker/image_picker.dart';
// import 'package:multiple_image_picker/multiple_image_picker.dart';
import '../api/leancloud.dart';
import '../common/repository.dart';
import '../common/events.dart';

class LeanCloudImageDelegate extends ZefyrDefaultImageDelegate {
  // final LeanCloud storage;

  // LeancloudImageDelegate(this.storage);

  Future<File> _cropImage(Uri image) async {
    return await ImageCropper.cropImage(
      sourcePath: image.path,
    );
  }

  @override
  Future<String> pickImage(ImageSource source) async {
    String ref;
    // Default delegate uses standard image_picker plugin to choose images
    final imagePath = await super.pickImage(source);
    final imageUri = Uri.parse(imagePath);

    // Crop image
    final image = await _cropImage(imageUri);

    // Create file and upload to Firestore
    // final File file = new File.fromUri(Uri.parse(imagePath));
    if (image != null) {
      await LeanCloudFile.upload(image.uri)
          .then((fileData) => ref = fileData.uri.toString())
          .catchError((e) => Repository.get().eventBus.fire(ErrorEvent(e)));

      print(ref);
    } else {
      print('File path is not correct.');
      Repository.get().eventBus.fire(ErrorEvent('文件路径不正确'));
    }

    // Return path to the Firestore Storage object. This value will be passed
    // to createImageProvider function below which is responsible for
    // resolving this value into an instance of ImageProvider: FileImage,
    // NetworkImage, etc.
    return ref;
  }

  @override
  Widget buildImage(BuildContext context, String imageSource) {
    return FadeInImage.memoryNetwork(
      placeholder: kTransparentImage,
      image: imageSource,
    );
  }

  // ImageProvider createFirestoreImageProvider(StorageReference ref) {
  //   // We can't use standard NetworkImage provider here because
  //   // ref.getDownloadUrl() is asynchronous.
  //   // TODO: implement
  // }
}
