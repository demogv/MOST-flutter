import 'package:date_format/date_format.dart';

String getDate(DateTime date) {
  return formatDate(date, [mm, '月', dd, '日', hh, ':', nn]).toString();
}

String getDateWithYear(DateTime date) {
  return formatDate(date, [yyyy, '年', mm, '月', dd, '日', hh, ':', nn]).toString();
}

String listToJsonString(List list) {
  String result = '[';
  for (var i = 0; i < list.length; i++) {
    result += '"' + list[i] + '"';
    if (i != list.length - 1) {
      result += ',';
    }
  }
  result += ']';
  return result;
}
