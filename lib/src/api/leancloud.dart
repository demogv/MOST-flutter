library leancloud;

import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:crypto/crypto.dart';
import 'package:dio/dio.dart';
import 'package:flutter/services.dart';
import 'package:meta/meta.dart';
import 'package:most/src/common/repository.dart';
import 'package:most/src/models/search_info.dart';
import 'package:most/src/models/user_info.dart';
import '../models/login_info.dart';

/* Tags defination
------------------
1. 问题
2. 回答
3. QA 评论
4. 二手书
5. 闲置品
10. 订单
*/

/* ----- Data -------*/
/* ID and key */
const appId = 'gv3b8YLk2FJivsHpsEHlf81U-9Nh9j0Va';
const appKey = 'StWz5nlyJxOxeplQn87UyDgR';

// Initialize the HTTP clinet for further use
Future<Dio> get httpClient async => Dio(await leanCloudOptionsWithSessionToken);

/// X-LC-Sign 的值是由 sign,timestamp[,master] 组成的字符串
get appSign {
  final timeStamp = DateTime.now().millisecondsSinceEpoch.toString();
  // Must convert to bytes first
  final bytes = utf8.encode('$timeStamp$appKey');
  final sign = md5.convert(bytes);
  return '$sign,$timeStamp';
}

// Channel for calling native methods
const MethodChannel _kChannel = MethodChannel('morchestra.com/leancloud');

final leanCloud = LeanCloud();

/* Options for operations */
/// Base url for CN servers
// final String baseUrl = // 华北
//     'https://${appId.substring(0, 8).toLowerCase()}.api.lncld.net/1.1';

// 华东
final String baseUrl =
    'https://${appId.substring(0, 8).toLowerCase()}.api.lncldapi.com/1.1';

final String functionBaseUrl =
    'https://${appId.substring(0, 8).toLowerCase()}.engine.lncldapi.com/1.1/call';

// 美国
// final String baseUrl = 'https://us-api.leancloud.cn/1.1';

/// General option
final Options leancloudOptions = new Options(
  baseUrl: baseUrl,
  connectTimeout: 100000,
  receiveTimeout: 100000,
  headers: {
    'X-LC-Id': appId,

    /// Use 'X-LC-Sign' for more security
    'X-LC-Sign': appSign,
    // 'X-LC-Key': appKey,
  },
  contentType: ContentType.json,
  responseType: ResponseType.JSON,
);

Future<Options> get leanCloudFunctionOptionsWithSessionToken async =>
    (await leanCloudOptionsWithSessionToken).merge(baseUrl: functionBaseUrl);
Options get leanCloudFunctionOptions  =>
    leancloudOptions.merge(baseUrl: functionBaseUrl);

Future<Options> get leanCloudOptionsWithSessionToken async {
  final sessionToken = await Repository.get().sessionToken;
  print('sessionToken: $sessionToken');
  if (sessionToken != null) {
    return leancloudOptions.merge(
      headers: {
        'X-LC-Id': appId,

        /// Use 'X-LC-Sign' for more security
        'X-LC-Sign': appSign,
        // 'X-LC-Key': appKey,
        'X-LC-Session': sessionToken
      },
    );
  } else {
    return leancloudOptions;
  }
}

/* ----- Data Structures -------*/

class LeanCloud {
  final LeanCloudObject object = LeanCloudObject();
  final LeanCloudUser user = LeanCloudUser();
  final LeanCloudFile file = LeanCloudFile();
  final LeanCloudFunction function = LeanCloudFunction();
}

class LeanCloudObject {
  /// /1.1/classes/<className> 	POST 	创建对象
  /// /1.1/classes/<className>/<objectId> 	GET 	获取对象
  /// /1.1/classes/<className>/<objectId> 	PUT 	更新对象
  /// /1.1/classes/<className> 	GET 	查询对象
  /// /1.1/classes/<className>/<objectId> 	DELETE 	删除对象
  static const classes = '/classes';

  /// /1.1/cloudQuery 	GET 	使用 CQL 查询对象
  static const cloudQuery = '/cloudQuery';

  /// /1.1/scan/classes/<className> 	GET 	按照特定顺序遍历 Class
  static const scanClasses = '/scan/classes';
}

class LeanCloudFile {
  /// 上传文件到 LeanCloud 通过 POST 请求，注意必须指定文呢t-type，
  /// 例如上传一个文本文件 hello.txt 包含一行字符串：[Cont呢text/plain]；
  /// 图片：["Content-Type: image/png" --data-binary '@tes呢t
  static const files = '/files';

// ---- Start: 用 REST API 上传文件无效 ------
//   Future<String> upload(File file) async {
//     // Process data to upload
//     FormData formData = new FormData.from({
//       "file1": new UploadFileInfo(file, file.uri.toFilePath()),
//     });

//     // Upload
//     Response<Map<String, String>> response = await httpClient
//         .request(files,
//             data: formData,
//             options: Options(method: 'POST', contentType: ContentType.binary))
//         .catchError((onError) {
//       print(onError);
//     });
//     return response?.data['url'];
//   }
// ---- End: 用 REST API 上传文件无效 ------

  /// Upload file by local URI, get the online file URI and the objectID as a Map.
  static Future<FileData> upload(Uri localFileUri, {String filename}) async {
    Uri onlineFileUri;
    String fileObjectId;
    print(localFileUri.toString());

    try {
      await _kChannel.invokeMethod('uploadFile', <String, String>{
        "localFileUri": localFileUri.toFilePath(),
        'filename': filename ?? localFileUri.path.split('/').last // 土方法获取文件名
      }).then((onValue) {
        onlineFileUri = Uri.tryParse(onValue['uri']);
        fileObjectId = onValue['objectId'];
      });
    } on PlatformException catch (error) {
      print(error);
    }

    return FileData(uri: onlineFileUri, objectId: fileObjectId);
  }
}

class FileData {
  final Uri uri;
  final String objectId;

  const FileData({this.uri, this.objectId});

  FileData.fromJson(Map<String, dynamic> json)
      // : objectId = null, // TODO: remove this
      //   uri = null;
      : objectId = json['objectId'],
        uri = Uri.tryParse(json['url']);
  toJson() => {
        'objectId': objectId,
        '__type': 'File',
      };
}

class LeanCloudRoles {
  ///   /1.1/roles 	POST 	创建角色
  /// /1.1/roles/<objectId> 	GET 	获取角色
  /// /1.1/roles/<objectId> 	PUT 	更新角色
  /// /1.1/roles 	GET 	查询角色
  /// /1.1/roles/<objectId> 	DELETE 	删除角色
  static const roles = '/roles';
}

class LeanCloudPush {}

class LeanCloudFunction {
  // Returns a Map<String, dynamic> or a List
  static Future<dynamic> callFunction(
    
      {Map<String, dynamic> myData,
      String functionName,
      String sessionToken}) async {
    dynamic results;
    print('baseUrl: $functionBaseUrl');
    print("processing");

    await (await httpClient)
        .post(
          '/$functionName',
          data: myData,
          options: await leanCloudFunctionOptionsWithSessionToken,
        )
        .catchError((onError) => print('callFunction: $onError'))
        .then((onValue) => results = onValue?.data['result']);
    print('callFunction: $results');
    return results;
  }

  static Future<dynamic> callFunctionWithToken(
      {Map<String, dynamic> myData,
      String functionName,
      String sessionToken}) async {
    Response response;
    print('baseUrl: $baseUrl');
    print("processing");
    response = await (await httpClient)
        .post(
          '/$functionName',
          data: myData,
          options: await leanCloudFunctionOptionsWithSessionToken,

        )
        .catchError((onError) => print(onError));
    print(response?.data['result']);
    return response?.data['result'];
  }

  static Future<SearchResponse> search({
    @required String query,
    int limit, // 返回集合大小，默认 100，最大 1000
    int skip, // 跳过的文档数目，默认为 0
    String sid, // 第一次查询结果中返回的 sid 值，用于分页，对应于 elasticsearch 中的 scoll id。
    String fields, // 逗号隔开的字段列表，查询的字段列表
    String
        highlights, // 高亮字段，可以是通配符 *，也可以是字段列表逗号隔开的字符串。如果加入，返回结果会多出 _highlight 属性，表示高亮的搜索结果内容，关键字用 em 标签括起来。
    String clazz, // 类名，如果没有指定或者为空字符串，则搜索所有启用了应用内搜索的 class。
    String order, // 排序字段，形如 -score,createdAt 逗号隔开的字段，负号表示倒序，可以多个字段组合排序。
    String
        include, // 自动填入内联的 Pointer 字段列表，逗号隔开，形如 user,comment 的字符串。仅支持 include Pointer 类型。
    String sort, // 复杂排序字段，例如地理位置信息排序
    SearchType type = SearchType.post, // 搜索类型
  }) async {
    print('baseUrl: $baseUrl');
    print("processing");
    print('sid: $sid');

    var searchParameters = <String, dynamic>{};

    if (query == null || query.isEmpty) {
      // Should not be null
      return null;
    } else {
      searchParameters['q'] = query;
    }

    if (limit != null) searchParameters['limit'] = limit;
    if (skip != null) searchParameters['skip'] = skip;
    if (sid != null && sid.isNotEmpty) searchParameters['sid'] = sid;
    if (fields != null && fields.isNotEmpty) searchParameters['fields'] = limit;
    if (highlights != null && highlights.isNotEmpty)
      searchParameters['highlights'] = highlights;
    if (clazz != null && clazz.isNotEmpty) searchParameters['clazz'] = clazz;
    if (order != null && order.isNotEmpty) searchParameters['order'] = order;
    if (include != null && include.isNotEmpty)
      searchParameters['include'] = include;
    if (sort != null && sort.isNotEmpty) searchParameters['sort'] = sort;

    print('search paramenters: $searchParameters');
    final Response<Map> response = await (await httpClient)
        .get<Map>("/search/select",
            data: searchParameters,
            options: (await leanCloudOptionsWithSessionToken).merge(
                contentType:
                    ContentType.parse('application/x-www-form-urlencoded')))
        .catchError((onError) => print('Search: $onError'));
    print('data: ${response?.data}');
    print('data.sid: ${response?.data["sid"]}');
    // print('results: ${SearchResponse.fromJson(response?.data)?.results}');
    final formatedResponse = SearchResponse<PostSearchResultEntry>.fromJson(
        response?.data,
        searchType: type);
    formatedResponse.results = formatedResponse.results.map((res) {
      switch (type) {
        case SearchType.post:
          return res as PostSearchResultEntry;
        case SearchType.tag:
          return res as TagSearchResultEntry;
        default:
          return res as PostSearchResultEntry;
      }
    }).toList();

    return formatedResponse;
  }

  //通过查询对象直接获取一堆对象，而不是云函数
  static Future<List> query(
      {Map<String, String> body,
      String className,
      ContentType contentType}) async {
    Response response;
    print('baseUrl: $baseUrl');
    print("processing");
    if (body == null) {
      response = await (await httpClient)
          .get<Map>(
            '/classes/$className',
            options: (await leanCloudOptionsWithSessionToken).merge(
              contentType: contentType ?? ContentType.parse("-urlencode"),
            ),
          )
          .catchError((onError) => print(onError));
    } else {
      response = await (await httpClient)
          .get(
            '/classes/$className',
            data: body,
            options: (await leanCloudOptionsWithSessionToken).merge(
              contentType: ContentType.parse("-urlencode"),
            ),
          )
          .catchError((onError) => print(onError));
    }
    print(response?.data);
    return response?.data['results'];
  }

  // Future<List> postWithMasterKey(
  //     {Map<String, String> body, String urlAddition}) async {
  //   Response response;
  //   String masterKey = await callFunction(functionName: 'get_master_key');
  //   print('baseUrl: $baseUrl');
  //   print("processing");
  //   response = await (await httpClient)
  //       .post(urlAddition,
  //           data: body,
  //           options: new Options(
  //             baseUrl: "https://gv3b8ylk.api.lncldapi.com/1.2",
  //             headers: {
  //               'X-LC-Id': appId,
  //               'X-LC-Key': masterKey + ",master",
  //             },
  //           ))
  //       .catchError((onError) => print(onError));
  //   print(response?.data);
  //   return response?.data['results'];
  // }

  // //post data without master key
  // Future<List> postData({Map<String, String> body, String urlAddition}) async {
  //   Response response;
  //   print('baseUrl: $baseUrl');
  //   print("processing");
  //   response = await httpClient
  //       .request(urlAddition, data: body, options: new Options(method: "POST"))
  //       .catchError((onError) => print(onError));
  //   print(response?.data);
  //   return response?.data;
  // }

  // //delete data without master key
  // Future<List> deleteData(
  //     {Map<String, String> body, String urlAddition}) async {
  //   Response response;
  //   print('baseUrl: $baseUrl');
  //   print("processing");
  //   response = await httpClient
  //       .request(urlAddition,
  //           data: body,
  //           options: new Options(method: "DELETE"))
  //       .catchError((onError) => print(onError));
  //   print(response?.data);
  //   return response?.data;
  // }

  // static Future<List> getWithMasterKey(
  //     {Map<String, String> body, String urlAddition}) async {
  //   Response response;
  //   String masterKey = await callFunction(functionName: 'get_master_key');
  //   print('baseUrl: $baseUrl');
  //   print("processing");
  //   response = await (await httpClient)
  //       .get(urlAddition,
  //           data: body,
  //           options: new Options(
  //               baseUrl: "https://gv3b8ylk.api.lncldapi.com/1.2",
  //               headers: {
  //                 'X-LC-Id': appId,
  //                 'X-LC-Key': masterKey + ",master",
  //               },
  //               contentType: ContentType.parse("-urlencode")))
  //       .catchError((onError) => print(onError));
  //   print(response?.data);
  //   return response?.data;
  // }
}

class LeanCloudFeedback {}

class LeanCloudStatistics {}

class LeanCloudMessaging {}

class LeanCloudUser {
  /// /1.1/users 	POST 	用户注册 用户连接
  /// /1.1/users/<objectId> 	GET 	获取用户
  /// /1.1/users/<objectId> 	DELETE 	删除用户
  /// /1.1/users/<objectId>/refreshSessionToken 	PUT 	重置用户 sessionToken。
  /// /1.1/users/<objectId>/updatePassword 	PUT 	更新密码，要求输入旧密码。
  /// /1.1/users/<objectId> 	PUT 	更新用户 用户连接 验证Email
  /// /1.1/users 	GET 	查询用户
  static const users = '/users';

  /// /1.1/users/me 	GET 	根据 sessionToken 获取用户信息
  static const thisUserInfo = '/users/me';

  /// /1.1/usersByMobilePhone 	POST 	使用手机号码注册或登录
  static const loginByPhone = '/usersByMobilePhone';

  /// /1.1/login 	POST 	用户登录
  static const login = '/login';

  /// /1.1/requestPasswordReset 	POST 	请求密码重设
  static const resetPassword = '/requestPasswordReset';

  /// /1.1/requestEmailVerify 	POST 	请求验证用户邮箱
  static const verifyEmail = '/requestEmailVerify';

  // User(this.uid, this.accessToken);

  // String uid;

  // /// It will be checked in the server side against uid.
  // String accessToken;

  // String sessionToken;
  /// Send ID token to LeanCloud to validate and sign in
  Future<LoginInfo> loginWithIdToken({String idToken}) async {
    // Map<String, dynamic> response;
    print('baseUrl: $baseUrl');
    print("processing");
    LoginInfo loginInfo;

    // Get data
    final Map<String, dynamic> response =
        await LeanCloudFunction.callFunction(functionName: 'login', myData: {
      "id_token": "$idToken" // 注意变量名是下划线法
    }).catchError((onError) => print(onError));
    print('response: $response');

    // Transform into [LoginInfo]
    try {
      loginInfo = LoginInfo.fromJson(response); // May be null
    } catch (e) {
      print(e);
    }

    if (loginInfo != null) {
      print(loginInfo.sessionToken);
      _loginSDKWithSessionToken(loginInfo.sessionToken); // Login SDK
    }
    return loginInfo;
  }

  /// Login SDK. Only use after gained session token. Called by [LeanCloud.loginWithIdToken({String idToken})].
  static Future _loginSDKWithSessionToken(String sessionToken) async {
    try {
      print('_loginSDKWithSessionToken: sessionToken: $sessionToken');
      await _kChannel.invokeMethod('loginWithSessionToken',
          <String, String>{"sessionToken": sessionToken});
    } on PlatformException catch (error) {
      print(error);
    }
  }

  /// Logout SDK. Called by [LeanCloud.({String sessionToken, String objectId})].
  static Future _logoutSDK() async {
    try {
      await _kChannel.invokeMethod('logout');
    } on PlatformException catch (error) {
      print(error);
    }
  }

  static Future logout({String sessionToken, String objectId}) async {
    await (await httpClient).put(
      '$users/$objectId/refreshSessionToken',
      options: await leanCloudOptionsWithSessionToken,
    );
    await _logoutSDK();
  }

  /// Retrieve full user info
  static Future<UserInfo> getUserInfo(String userObjectId) async {
    UserInfo info;
    await (await httpClient)
        .get('$users/$userObjectId')
        .then((response) => info = UserInfo.fromJson(response?.data))
        .catchError((onError) => print('getUserInofo: ${onError.toString()}'));
    return info;
  }

  static Future<UserInfo> getThisUserInfo() async {
    UserInfo info;
    // print('userObjectID: $userObjectId, sessionToken: $sessionToken');
    await (await httpClient).get(thisUserInfo).then((response) {
      print('getThisUserInfo: ${response?.data}');
      info = UserInfo.fromJson(response?.data);
      print('name: ${info.username}');
    }).catchError((onError) => print('getThisUserInfo: ${onError.toString()}'));
    return info;
  }

  /// Update editorable info
  static Future<bool> updateUserInfo(
      String userObjectId, UserEditableInfo info, String sessionToken) async {
    bool success = false;
    print('loginInfo: ${info.toJson()}');
    await (await httpClient)
        .put('$users/$userObjectId',
            data: info.toJson(), // Use enum to get name string
            options: Options(
              headers: {
                'X-LC-Id': appId,

                /// Use 'X-LC-Sign' for more security
                // 'X-LC-Sign': appSign,
                'X-LC-Key': appKey,
                'X-LC-Session': sessionToken
              },
            ))
        .then((response) {
      if (response?.data['updatedAt'] != null) {
        success = false;
      }
    });
    return success;
  }

  /// Returns a list of UserInfo. If error occurs, returns null.
  static Future<List<UserInfo>> followersOf(String userObjectId) async {
    print(
        '${(await leanCloudOptionsWithSessionToken).baseUrl}/users/$userObjectId/followers');
    final Response response = await (await httpClient)
        .get(
          '/users/$userObjectId/followers',
          // data: {'include': 'followee'},
          options: (await leanCloudOptionsWithSessionToken).merge(
              // contentType: ContentType.parse('-urlencoded'),
              ),
        )
        .catchError((onError) => print('followersOf: $onError'));
    if (response != null) {
      final List results = response.data['results'];
      return results.map((follower) => UserInfo.fromJson(follower)).toList();
    }
    return null;
  }

  static Future<List<UserInfo>> followeesOf(String userObjectId) async {
    final Response response = await (await httpClient)
        .get(
          '/users/$userObjectId/followees',
          // data: {'include': 'followee'},
          options: (await leanCloudOptionsWithSessionToken).merge(
              // contentType: ContentType.parse('-urlencoded'),
              ),
        )
        .catchError((onError) => print('followeesOf: $onError'));
    if (response != null) {
      final List results = response.data['results'];
      return results.map((followee) => UserInfo.fromJson(followee)).toList();
    }
    return null;
  }
}

class FollowersAndFollowees {
  final List<UserInfo> followers;
  final List<UserInfo> followees;

  FollowersAndFollowees(this.followers, this.followees);

  FollowersAndFollowees.fromJson(Map<String, dynamic> json)
      : this.followees = (json['followees'] as List)
            .map((followee) => UserInfo.fromJson(followee)),
        this.followers = (json['followers'] as List)
            .map((follower) => UserInfo.fromJson(follower));
}

// TODO: Use JsonSerializable for all JSONs
/// Authentication data to login/register.
///
/// Currently only supports microsoft accounts.
class AuthData {
  // Use uid as the real identifier
  final String openid;

  AuthData(this.openid);

  // To connect more than one platforms, this will be needed

  // final List<Map<String, Map<String, String>>> dataList=[];
  // void add({String platform, String uid, String accessToken}) {
  //   dataList.add({
  //     platform: {'uid': uid, 'access_token': accessToken}
  //   });
  // }

  AuthData.fromJson(Map<String, dynamic> json)
      : this.openid = json['authData']['microsoft']['uid'];

  Map<String, Map<String, Map<String, String>>> toJson() => {
        'authData': {
          'microsoft': {
            'uid': this.openid,
          },
        },
      };
}

/// Infomation that can be edited by user mutually.
class UserEditableInfo {
  final FileData avatar;
  final String username;
  final String alipayAccount;

  const UserEditableInfo({this.avatar, this.username, this.alipayAccount});

  UserEditableInfo.fromJson(Map<String, dynamic> json)
      : this.avatar = FileData.fromJson(json['avatar']),
        this.username = json['username'],
        this.alipayAccount = json['alipay_account'];

  Map<String, dynamic> toJson() => {
        'username': username,
        'avatar': avatar.toJson(),
        'alipay_account': alipayAccount,
      };
}
