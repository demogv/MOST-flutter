import 'dart:async';
import 'dart:io';
// import 'dart:convert';
// import 'dart:io';

// import '../common/secure_storage.dart';
// import 'package:http/http.dart' as http;
import 'package:dio/dio.dart';

import '../common/events.dart';
import 'package:flutter_string_encryption/flutter_string_encryption.dart';

/* 
This page shows the authentication flow, including http server binding:
https://medium.com/@segaud.kevin/facebook-oauth-login-flow-with-flutter-9adb717c9f2e
*/

/* Constants */

const String clientId = 'd676946c-2e2d-4961-856a-0cbb2068995e';
const String clientSecret = 'ubgtVH526![@gndAQTUV26?';

// https://developer.microsoft.com/en-us/graph/docs/concepts/permissions_reference
const List<String> scope = ['openid'];

/// The domain in which the user have to be.
const String tenant = 'link.cuhk.edu.cn';

const int redirectPort = 3000; // Maybe need a strange port
const String redirectUrl =
    'http://localhost:$redirectPort/'; // Must ends with a `/` to have a `?`

const String baseUrl = 'https://login.microsoftonline.com/common/oauth2';

const String authorizationEndpoint =
    'https://login.microsoftonline.com/$tenant/oauth2/authorize'; // Do NOT attatch `v2.0`, otherwise no oid will be given
const String tokenEndpoint =
    "https://login.microsoftonline.com/common/oauth2/token";

const String endSessionEndpoint =
    "https://login.microsoftonline.com/common/oauth2/logout?post_logout_redirect_uri=$redirectUrl";

final cryptor = new PlatformStringCryptor();

/* Login authenticator */

/// Either load an access token from saved tokens or authenticate a new
/// one.
///
/// Use `false` to indicate an error.
Future<String> getIdToken({String user}) async {
  // Generate nonce
  final String nonce = await cryptor.generateRandomKey();

  /// https://docs.microsoft.com/en-us/azure/active-directory/develop/active-directory-v2-protocols-oidc#fetch-the-openid-connect-metadata-document
  final String authorizationUri = Uri.encodeFull('$authorizationEndpoint?'
      'client_id=$clientId'
      // Must include id_token for OpenID Connect sign-in.
      // It might also include other response_types values, such as code.
      '&response_type=id_token'
      '&redirect_uri=$redirectUrl'
      '&scope=${scope.join(' ').toLowerCase()}'
      // Recommend using response_mode=form_post,
      // to ensure the most secure transfer of tokens to your application.
      '&response_mode=fragment'
      '&nonce=$nonce'
      '&login_hint=$user@$tenant');
  print(authorizationUri);
// TODO: add `state` to  prevent cross-site request forgery attacks.

  /* Get code */

  // Setup a server to be redirected to, and show a friendly message.
  // await _server();

  // Open login webview
  if (authorizationUri != null) {
    eventBus.fire(OpenLoginWebViewEvent(authorizationUri));
  } else {
    return null;
  }

  // Waits for idToken, otherwise this function will return too early
  // Returns the id token.
  String idToken = await eventBus
      .on<UserIdTokenRetrieved>()
      .asyncMap((convert) => convert.idToken)
      .first;

  // // Set id token
  // idToken = await onIdToken.first;
  // if (idToken == null) {
  //   eventBus.fire(ErrorEvent('收到来自服务器的错误'));
  //   return null;
  // } else {
  //   // Save ID token and further get access token by code retrived
  //   secureStorage.idToken = idToken;
  //   print(idToken);
  //   // isSuccessfullyGotAccessToken = await getAccessToken(loginResponse.code);
  //   // TODO: check state
  // }

  // TODO: if unsuccessful, raise error

  // if (isSuccessfullyGotAccessToken) {
  //   eventBus.fire(
  //     ValidateUserId(
  //         accessToken: secureStorage.accessToken,
  //         expireIn: secureStorage.accessTokenExpireIn,
  //         openid: secureStorage.idToken),
  //   );
  // }
  // }

  return idToken;
}

Future signOutAzureAD() async {
  Dio httpClient = Dio();
  await httpClient.get(endSessionEndpoint);
}

// Future<String> getAccessToken({String user}) async {
//   String code;
//   final String authorizationUri = Uri.encodeFull('$authorizationEndpoint?'
//       'client_id=$clientId'
//       // Must include id_token for OpenID Connect sign-in.
//       // It might also include other response_types values, such as code.
//       '&response_type=code'
//       '&redirect_uri=$redirectUrl'
//       '&scope=${scope.join(' ').toLowerCase()}'
//       // Recommend using response_mode=form_post,
//       // to ensure the most secure transfer of tokens to your application.
//       // '&response_mode=query'
//       '&nonce=$nonce'
//       '&login_hint=$user@$tenant');

//   if (await secureStorage.accessToken == null) {
//     // If no access token stored, request one
//     // Open login webview
//     if (authorizationUri != null) {
//       // eventBus.fire(OpenLoginWebViewEvent(authorizationUri));
//     } else {
//       return null;
//     }
//     Stream<String> onToken = await _server();
//     code = await onToken.first;
//     if (code == null) {
//       eventBus.fire(ErrorEvent('收到来自服务器的错误'));
//       return null;
//     } else {
//       // Shouldn't store anything here. Only store session token.
//       // Save ID token and further get access token by code retrived
//       // secureStorage.idToken = token.accessToken;
//       // print(token);
//       // isSuccessfullyGotAccessToken = await getAccessToken(loginResponse.code);
//       // TODO: check state
//     }
//     /* Get token */

//     final http.Response response = await http.post(
//         'https://login.microsoftonline.com/common/oauth2/v2.0/token',
//         body: {
//           'grant_type': 'authorization_code',
//           'code': '$code',
//           'redirect_uri': redirectUrl,
//           'scope': '${scope.join(' ').toLowerCase()}',
//           'client_id': '$clientId',
//           'client_secret': '$clientSecret',
//         }).catchError((onError) => eventBus.fire(ErrorEvent(onError)));

//     // accessToken = Token.fromMap(jsonDecode(response.data)).accessToken;
//     // TODO: save token and expire date
//   }
//   // print(accessToken);
//   return code;
// }

// Future<Stream<String>> _idTokenReceiver() async {
//   final StreamController<String> onCode = new StreamController();
//   HttpServer server = await HttpServer.bind(
//     InternetAddress.loopbackIPv4,
//     redirectPort,
//     shared: true,
//   );
//   // Listens POST requests
//   await for (var req in server) {
//     ContentType contentType = req.headers.contentType;
//     HttpResponse response = req.response;

//     if (req.method == 'POST' &&
//         contentType?.mimeType == 'x-www-form-urlencoded' /*1*/) {
//       try {
//         String content = await req.transform(utf8.decoder).join(); /*2*/
//         print(content);
//         var data = jsonDecode(content) as Map; /*3*/
//         onCode.add(data['idToken']);
//         req.response
//           ..statusCode = HttpStatus.ok
//           ..write('Wrote data for ${data['name']}.');
//       } catch (e) {
//         response
//           ..statusCode = HttpStatus.internalServerError
//           ..write("Exception during file I/O: $e.");
//       }
//     } else {
//       response
//         ..statusCode = HttpStatus.methodNotAllowed
//         ..write("Unsupported request: ${req.method}.");
//     }
//     response.close();
//   }

//   // server.listen((HttpRequest request) async {
//   //   LoginResponse response = LoginResponse();
//   //   // response.code = request.uri.queryParameters["code"];

//   //   response.idToken = request.uri.queryParameters["id_token"];
//   //   print('request parameters: ${request.uri.queryParameters}');
//   //   print('response.idToken: ${response.idToken}');
//   //   response.state = request.uri.queryParameters["state"];
//   //   request.response
//   //     ..statusCode = 200
//   //     ..headers.set("Content-Type", ContentType.html.mimeType)
//   //     ..write("<html><h1>You can now close this window</h1></html>");
//   //   await request.response.close();
//   //   await server.close(force: true);
//   //   onCode.add(response);
//   //   await onCode.close();
//   // });
//   await server.close(force: true);
//   onCode.close();
//   return onCode.stream;
// }

// Future<Stream<String>> _server() async {
//   // final StreamController<String> onCode = new StreamController();
//   HttpServer server = await HttpServer.bind(
//       InternetAddress.loopbackIPv4, redirectPort,
//       shared: true);
//   server.listen((HttpRequest request) async {
//     // final String code = request.uri.queryParameters["id_token"];
//     request.response
//       ..statusCode = 200
//       ..headers.set("Content-Type", ContentType.html.mimeType)
//       ..write("<html><h1>You can now close this window</h1></html>");
//     await request.response.close();
//     await server.close(force: true);
//     // onCode.add(code);
//     // await onCode.close();
//   });
//   // return onCode.stream;
// }

/* Data structures */
// class Token {
//   final String accessToken;
//   final String type;
//   final num expiresIn;

//   Token(this.accessToken, this.type, this.expiresIn);

//   Token.fromMap(Map<String, dynamic> json)
//       : accessToken = json['id_token'], // different than facebook
//         type = json['token_type'],
//         expiresIn = json['expires_in'];
// }

// class LoginResponse {
//   String idToken;
//   String code;
//   String state;
// }
