import UIKit
import Flutter

@UIApplicationMain
@objc class AppDelegate: FlutterAppDelegate {
    override func application(
        _ application: UIApplication,
        didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?
        ) -> Bool {
        GeneratedPluginRegistrant.register(with: self)
        let appId: String = "gv3b8YLk2FJivsHpsEHlf81U-9Nh9j0Va"
        let appKey: String = "StWz5nlyJxOxeplQn87UyDgR"
        AVOSCloud.setApplicationId(appId, clientKey: appKey)
        AVOSCloud.setAllLogsEnabled(true)
        let controller: FlutterViewController = window?.rootViewController as! FlutterViewController;
        let myChannel = FlutterMethodChannel.init(name: "morchestra.com/leancloud", binaryMessenger: controller)
        myChannel.setMethodCallHandler { (call, result) in
            print(call.method)
            if call.method == "uploadFile" {
                print("现在uploadFile")
                var file: AVFile
                let localFileUri: String = (call.arguments as! Dictionary<String, Any>)["localFileUri"] as! String
                do {
                    file = try AVFile.init(localPath: localFileUri)
                    file.saveInBackground { (saved:Bool, error:Error?) in
                        if saved {
                            let onlineFileUri: String = file.url()!
                            if onlineFileUri.starts(with: "http") {
                                var onlineFileObject = Dictionary<String, String>()
                                onlineFileObject["uri"] = onlineFileUri
                                onlineFileObject["objectId"] = (file.objectId() as! String)
                                result(onlineFileObject);
                            } else {
                                result(FlutterError.init(code: "Fail", message: "Fail to upload the file.", details: nil))
                            }
                        } else {
                            print(error as Any)
                        }
                    }
                } catch let error {
                    print(error)
                }
            } else if call.method == "loginWithSessionToken" {
                print("直接进去了")
                print(call)
                let sessionToken: String = (call.arguments as! Dictionary<String,AnyObject>)["sessionToken"] as! String
                AVUser.becomeWithSessionToken(inBackground: sessionToken, block: { (user: AVUser?, e: Error?) in
                    //
                })
            } else {
                print("啥都没搞")
                result(FlutterMethodNotImplemented);
            }
        }
        return super.application(application, didFinishLaunchingWithOptions: launchOptions)
    }
    
    override func application(_ application: UIApplication, handleOpen url: URL) -> Bool {
        return FlutterAlipayPlugin.handleOpen(url)
    }
}
