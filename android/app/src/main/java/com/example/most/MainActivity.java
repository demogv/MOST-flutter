
package com.example.most;

import android.os.Bundle;
import android.util.Log;

import com.avos.avoscloud.AVException;
import com.avos.avoscloud.AVFile;
import com.avos.avoscloud.AVOSCloud;
import com.avos.avoscloud.AVUser;
//import com.avos.avoscloud.AVObject;
import com.avos.avoscloud.SaveCallback;
import com.avos.avoscloud.LogInCallback;

import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Map;

import io.flutter.app.FlutterActivity;
import io.flutter.plugins.GeneratedPluginRegistrant;

// Method Channel
import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;
import io.flutter.plugin.common.MethodChannel.MethodCallHandler;
import io.flutter.plugin.common.MethodChannel.Result;

public class MainActivity extends FlutterActivity {
    private static final String CHANNEL = "morchestra.com/leancloud";

    /* ID and key */
    private static final String appId = "gv3b8YLk2FJivsHpsEHlf81U-9Nh9j0Va";
    private static final String appKey = "StWz5nlyJxOxeplQn87UyDgR";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        GeneratedPluginRegistrant.registerWith(this);

        // 初始化参数依次为 this, AppId, AppKey
        AVOSCloud.initialize(this, appId, appKey);

        // // 放在 SDK 初始化语句 AVOSCloud.initialize() 后面，只需要调用一次即可
         AVOSCloud.setDebugLogEnabled(true);
        //
        // // 测试 SDK 是否正常工作的代码
        // AVObject testObject = new AVObject("TestObject");
        // testObject.put("words", "Hello World!");
        // testObject.saveInBackground(new SaveCallback() {
        // @Override
        // public void done(AVException e) {
        // if (e == null) {
        // Log.d("saved", "success!");
        // }
        // }
        // });
        // Register the method channel
        new MethodChannel(getFlutterView(), CHANNEL).setMethodCallHandler(new MethodCallHandler() {
            @Override
            public void onMethodCall(MethodCall call, final Result result) {

                if (call.method.equals("uploadFile")) {
                    final AVFile file;

                    // Retrieve local file URI
                    final String localFileUri = call.argument("localFileUri");
                    final String filename = call.argument("filename");
                    // Try to upload file and get the online file's URI
                    try {
                        // Retrieve local file
                        file = AVFile.withAbsoluteLocalPath(filename, localFileUri);
                        file.addMetaData("owner", AVUser.getCurrentUser().getObjectId());
                        // Save file to the server, get the online URI, and send back the result
                        file.saveInBackground(new SaveCallback() {
                            @Override
                            public void done(AVException e) {
                                Log.d("saved", "done: " + e);
                                final String onlineFileUri = file.getUrl(); // 返回一个唯一的 Url 地址
                                if (onlineFileUri.startsWith("http")) {
                                    final Map<String, String> onlineFileObject = new HashMap<>();
                                    onlineFileObject.put("uri", onlineFileUri);
                                    onlineFileObject.put("objectId", file.getObjectId());
                                    result.success(onlineFileObject);
                                } else {
                                    result.error("Fail", "Fail to upload the file.", null);
                                }

                            }
                        });
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }

                } else if (call.method.equals("loginWithSessionToken")) {
                    String sessionToken = call.argument("sessionToken");
                    AVUser.becomeWithSessionTokenInBackground(sessionToken, new LogInCallback<AVUser>() {
                        @Override
                        public void done(AVUser user, AVException e) {
                            // user.getCurrentUser()
                            // result.success();
                        }
                    });

                } else if (call.method.equals("logout")) {
                    AVUser.logOut();// 清除缓存用户对象
                } else {
                    result.notImplemented();
                }
            }
        });
    }
}
